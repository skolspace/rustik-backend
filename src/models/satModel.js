import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let satModelComponentSchema = new Schema({
        type: String,
        name: String,
        gltfName: String,
        gltfOmtId: String
    });

let satModelSchema = new Schema({
    ver: String,
    uuid: String,
    // uuid: {
    //     type: String,
    //     index: true,
    //     unique: true // Unique index. If you specify `unique: true`
    //     // specifying `index: true` is optional if you do `unique: true`
    //   }
    name: String,
    components: [satModelComponentSchema]
    // components: { type: [{
    //     type: String,
    //     name: String,
    //     gltfName: String
    // }] }
});
export default mongoose.model('SatModel', satModelSchema, "models");