import mongoose from 'mongoose';
const Schema = mongoose.Schema;
let solutionSchema = new Schema({
    ver: String,
    uuid: String,
    name: String
});
export default mongoose.model('Solution', solutionSchema, "solutions");