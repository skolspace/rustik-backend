import mongoose from 'mongoose';
const Schema = mongoose.Schema;
let componentTypesSchema = new Schema({
    uuid: {
        type: String
    },
    componentType: String,
    parentComponentType: String,
    label: String
});
export default mongoose.model('ComponentType', componentTypesSchema, "componentTypes");
// export default mongoose.model('ComponentType', componentTypesSchema, "spoonComponentTypes");
