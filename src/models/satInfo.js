import mongoose from 'mongoose';
const Schema = mongoose.Schema;
let satInfos = new Schema({
    uuid: {
        type: String
    }
    ,
    norad_id: {
        type: Number
    },
    satName: {
        type: String
    },
    tle1: {
        type: String
    },
    tle2: {
        type: String
    }
});
export default mongoose.model('satInfos', satInfos, "satInfos");
