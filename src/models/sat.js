import mongoose from 'mongoose';
const Schema = mongoose.Schema;
let satSchema = new Schema({
    ver: String,
    satName: {
        type: String
    },
    noradId: {
        type: Number
    },
    solId: {
        type: String
    },
    satInfo: {
        tle1: {
            type: String
        },
        tle2: {
            type: String
        }
    }
});
export default mongoose.model('Sat', satSchema, "sats");