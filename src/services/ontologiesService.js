var arangoService = require('./arangoService').service;
var aql = require('./arangoService').aql;
var AT = arangoService.AT;

async function getOntologyTree() {
    var all = await AT.collections.Ontologies.all();

    return all.map(t => t);
}

module.exports.getOntologyTree = getOntologyTree;
// module.exports.create = create;