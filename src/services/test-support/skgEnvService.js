////@ts-check
///Semantic Knowledge Graphs Environment

import uuidv1 from "uuid";

// import arangoService from '../arangoService'
var arangoService = require('../arangoService').service;
var AT = arangoService.AT;
var db = arangoService.db;
// import arangoService from '../arangoService'
import skgModelsService from '../skg/skgModelsService'

//var ArangoDatabase = require('arangojs').Database;
// new ArangoDatabase(global.gConfig.arango.connectionString);
var aql = require('arangojs').aql;

///collection prefix
var cPrefix = "skb"


import generalModels from '../../models/skg/generalModels';

async function init() {
    arangoService.truncateAll();
    // db.useDatabase(global.gConfig.arango.dbName);


}
async function createAll() {
    await init();
    var res1 = await createExtUnits();
    var res2 = await createTechnicalOntology();
    var res3 = await createSatModel1();
    return [res2];
}

async function createExtUnits() {
    try {
        var units = [
            { _key: "any" },
            { _key: "amper", shortLabel: "A" },
            { _key: "watt", shortLabel: "W", desc: "power" },
            { _key: "W-m2", shortLabel: "W/m2", desc: "sm like light power", eg: ["sunlight current", "solar irradiance"] },
            { _key: "enum", shortLabel: "enumerator collection" },
            {
                _key: "gram", shortLabel: "g", desc: "is a metric system unit of mass", ref: "https://en.wikipedia.org/wiki/Gram",
                generalParam: "mass"
            },
            { _key: "sec", shortLabel: "s", longLabel: "second" }, 
            { _key: "metre", shortLabel: "m" },
            { _key: "pascal", shortLabel: "Pa" },
            { _key: "volt", shortLabel: "Volt" },
            { _key: "wattHour", shortLabel: "Wh" },
            { _key: "amperHour", shortLabel: "Ah" },
            { _key: "wattHourPerLiter", shortLabel: "Wh/L" },
            { _key: "liter", shortLabel: "L" },
            { _key: "hertz", shortLabel: "Hz"},
            { _key: "decibel-milliwatt", shortLabel: "dBm", desc: "decibels (dB) with reference to one milliwatt (mW)"},
            { _key: "decibel-watt", shortLabel: "dBW", desc: "signal expressed in decibels relative to one watt"},
            { _key: "celsius-degree", shortLabel: "°C", desc: "Celsius degree"},
            { _key: "angle-degree", shortLabel: "°", desc: "angle degree"},
            { _key: "angle-degree-sec", shortLabel: "°/s", desc: "angle degree per second"}, //deg/s
            { _key: "revolutions-per-minute", shortLabel: "rpm", translationRules: [{text: "1 rpm = 1/60 Hz"}]},
            { _key: "foot", shortLabel: "ft" },
            { _key: "centimetre-cubic", shortLabel: "cm^3"},
            
        ]
        var res1 = await AT.collections.Units.import(units, { onDuplicate: "replace" });

        var unitsExts = [
            { _key: "any" },
            { _key: "max" },
            { _key: "min" },
            { _key: "avg" },
            { _key: "nominal" },
            { _key: "micro", shortlabel: "μ", "calcRule": 0.000001 },
            { _key: "milli", shortlabel: "m", "calcRule": 0.001 },
            { _key: "kilo", shortlabel: "k", "calcRule": 1000 },
            { _key: "paToBar", shortlabel: "", "calcRule": 100000 },
            { _key: "string" }
        ]
        var res2 = await AT.collections.UnitsExt.import(unitsExts, { onDuplicate: "replace" });

        var dataToPut = [
            ["any", "any", "any"],
            ["microAmper", "micro", "amper"],
            ["maxAmper", "max", "amper"],
            ["minAmper", "min", "amper"],
            ["avgAmper", "avg", "amper"],
            ["maxVolt", "max", "volt"],
            ["minVolt", "min", "volt"],
            ["avgVolt", "avg", "volt"],
            ["microWatt", "micro", "watt"],
            ["nominalWatt", "nominal", "watt", { shortLabel: "V", longLabel: "Nominal Watt" }],
            ["kW-m2", "kilo", "W-m2"],
            ["stringEnum", "string", "enum"],
            ["stringAny", "string", "any"],
            ["kg", "kilo", "gram"],
            ["millimetre", "milli", "metre", { shortLabel: "mm" }],//shortlabel: "mm"
            ["kilometre", "kilo", "metre", { shortLabel: "km" }],//shortlabel: "mm"
            ["bar", "paToBar", "pascal"],
            ["avgVolt", "avg", "volt"],
            //energy
            ["milliAmperHour", "milli", "amperHour", { shortLabel: "mAh" }],
            ["kWattHour", "kilo", "wattHour", { shortLabel: "kWh", desc: { ref: "https://en.wikipedia.org/wiki/Kilowatt_hour" } }],
            ["nominalWattHourPerLiter", "nominal", "wattHourPerLiter"],
            ["nominalLiter", "nominal", "liter", { shortLabel: "L" }],
            ["gigaHertz", "giga", "hertz", {shortLabel: "GHz"}],
            ["millisecond", "milli", "second", {shortLabel: "ms", longLabel: "millisecond"}],
        ]
        // var res3 = await  AT.collections.ExtUnits.import(dataToPut, { "type": null, onDuplicate: "replace" });
        await arangoService.saveEdgesOfTwoCollections(AT.collections.ExtUnits, dataToPut,
            AT.collections.UnitsExt, AT.collections.Units, null, true, true);

        return {};
    }
    catch (ex) {
        console.error(ex);
    }
}

async function createTechnicalOntology() {
    try {
        var data1 = [
            { _key: "size", "desc": { "value": "dimensions of things", "ref": "https:wiki.com/size" }, "subparams": "Size can be measured as length, width, height, diameter, perimeter, are" },
            { _key: "interfaceType" },
            { _key: "operatingTemperature", "type": "range", "fromValue": "Kelvins", "toValue": "Kelvins" },
            { _key: "incomeElectricCurrent" },
            { _key: "incomeVoltage" },
            { _key: "outcomeElectricCurrent" },
            { _key: "outcomeVoltage" },
            {
                _key: "electricCapacity", shortLabel: "capacity", synonims: ["energyCapacity"], desc: {
                    text: "Energy capacity goals relate to the amount of energy stored in the batteries and the batteries’ energy density",
                    ref: "https://www.sciencedirect.com/topics/engineering/energy-capacity"
                },
                refs: [
                    { ref: "https://www.sciencedirect.com/topics/engineering/battery-capacity" }
                ]
            },
            { _key: "electricPower", defaultUnit: "amperHour", desc: { text: "electrical energy is transferred by an electric circuit" } },
            { _key: "energyDensity", defaultUnit: "wattHourPerLiter", desc: { text: "electrical energy is transferred by an electric circuit" } },

            { _key: "physicalVolume", units: ["liter", "m^3"] },
            { _key: "sunlightCurrent" },

            { _key: "electrochemicalTechnology", desc: "https://en.wikipedia.org/wiki/Electric_battery line 1" },
            {
                _key: "numberOfRechargeCycles", desc: {
                    text: "how many times it can undergo the process of complete charging and discharging until failure or it starting to lose capacity",
                    ref: "https://en.wikipedia.org/wiki/Charge_cycle"
                }
            },
            { _key: "numberOfPerformedCycles", desc: "cycles already executed" },
            { _key: "motionType", examples: ["https://en.wikipedia.org/wiki/Linear_actuator", "https://en.wikipedia.org/wiki/Rotary_actuator"] },
            { _key: "funcitonalMode", shortLabel: "Mode", measuredIn: "stringEnum" },
            {
                _key: "currentFuncitonalMode", forRealTimeAnalysis: true,
                shortLabel: "cMode", measuredIn: "stringEnum"
            },
            //what is different from size?
            { _key: "geometry", desc: "includes position, geometry form in space area" },
            { _key: "mass", desc: "tatata", wiki: "https://en.wikipedia.org/wiki/Mass" },
            { _key: "outputPressure", synonims: ["outletPressure"] },
            { _key: "inletPressure" },
            { _key: "fuelType" },
            {
                _key: "CRate", shortLabel: "C-rate", def: {
                    text: "is a measure of the rate at which a battery is being charged or discharged. It is defined as the current through the battery divided by the theoretical current draw under which the battery would deliver its nominal rated capacity in one hour",
                    ref: "https://en.wikipedia.org/wiki/Electric_battery#C_rate"
                }
            },
            {
                _key: "capacityLoss", shortLabel: "Capacity Loss", desc: {
                    ref: "https://en.wikipedia.org/wiki/Capacity_loss"
                }
            },
            {
                _key: "capacityLossPerCycle", shortLabel: "Capacity Loss/Cycle", desc: {
                    ref: "https://en.wikipedia.org/wiki/Capacity_loss"
                }
            },
            {
                _key: "stateOfCharge", shortLabel: "SoC", longLabel: "State of charge", desc: {
                    text: `is the level of charge of an electric battery relative to its capacity`,
                    ref: "https://en.wikipedia.org/wiki/State_of_charge"
                },
                rules: [{
                    reverse: "depthOfDischarge",

                    desc: "SoC is normally used when discussing the current state of a battery in use"
                }]
            },
            {
                _key: "depthOfDischarge", shortLabel: "DoD", longLabel: "Depth-aI-discharge", desc: {
                    text: ` is simply the percent oftotal battery capacity removed
                during a discharge period`,
                    ref: "https://en.wikipedia.org/wiki/Depth_of_discharge"
                }, refs: [{ ref: { type: "book", name: "SMAD-2005", page: "420" } }]
            },
            {
                _key: "currentDepthOfDischarge", forRealTimeAnalysis: true,
                shortLabel: "cDoD", longLabel: "Depth-aI-discharge"
            },
            {
                _key: "materialMadeOf", forRealTimeAnalysis: true,
                shortLabel: "Material", longLabel: "construction material from what it physically consists"
            }
            //? { _key: "temperatureData"} ,
        ]
        await AT.collections.GeneralParams.import(data1, { onDuplicate: "replace" });

        var data2 = [
            { _key: "electricCurrent", "measures": [{ "type": "amper", "short": "A" }], desc: { text: "is the flow of electric charge through an object" } },
            { _key: "information" },
            { _key: "temperatureInformation" },
            { _key: "pressureInformation" },
            { _key: "sunlightCurrent" }, //tothink the same name as parameter for orbit???
            { _key: "command" },
            { _key: "rotateCommand", structure: { angle: { measuredIn: "Degree" }, speed: { measuredIn: "DegreePerSec", optional: true } } },
            { _key: "changedGeometry" },
            //todo mb rename to motion? https://en.wikipedia.org/wiki/Linear_actuator
            { _key: "physicalForce" },
            { _key: "linearForce" },
            //torque is physicalForce when generalParams/motionType.value = 'rotary'
            { _key: "torque", synonyms: ["rotaryForce"], desc: { text: "rotational equivalent of linear force", wiki: "https://en.wikipedia.org/wiki/Torque" } },
            //? { _key: "temperatureData"} ,
            { _key: "fluid", wiki: "https://en.wikipedia.org/wiki/Fluid" },
            // { _key: "fluidFlow", baseOntology: "fluidDynamics" },
            { _key: "gas" },
            { _key: "liquid" },
            //{ _key: "fluidized solids" }, //https://en.wikipedia.org/wiki/Valve
            { _key: "slurry" },
            { _key: "fluidFuel" } //todo how is it related to fuel and to fluid
        ]
        await AT.collections.Res.import(data2, { onDuplicate: "replace" });

        var SubclassOf = [
            ["_key", "_from", "_to"],
            ["subcB0", `${AT.collections.Res.name}/temperatureInformation`, `${AT.collections.Res.name}/information`],
            ["subcB1", `${AT.collections.Res.name}/command`, `${AT.collections.Res.name}/information`],
            ["subcB2", `${AT.collections.Res.name}/rotateCommand`, `${AT.collections.Res.name}/command`],
            ["subcB3", `${AT.collections.Res.name}/linearForce`, `${AT.collections.Res.name}/physicalForce`],
            ["subcB4", `${AT.collections.Res.name}/torque`, `${AT.collections.Res.name}/physicalForce`],
            ["subcB5", `${AT.collections.Res.name}/gas`, `${AT.collections.Res.name}/fluid`],
            ["subcB6", `${AT.collections.Res.name}/liquid`, `${AT.collections.Res.name}/fluid`],
            ["subcB7", `${AT.collections.Res.name}/slurry`, `${AT.collections.Res.name}/fluid`],
            ["subcB8", `${AT.collections.Res.name}/pressureInformation`, `${AT.collections.Res.name}/information`],
        ]
        var res2 = await AT.collections.SubclassOf.import(SubclassOf, { "type": null, onDuplicate: "replace" });

        // //posssbile variants for select choose
        // var dataMeasuredIn = [
        //     ["rotaryMotionEnum", "motionType", "stringEnum", { isVariant: true, value: "rotary" }],
        //     ["linearMotionEnum", "motionType", "stringEnum", { isVariant: true, value: "linear" }]
        // ]
        // // await  AT.collections.MeasuredIn.import(dataMeasuredIn, { "type": null, onDuplicate: "replace" });
        // await arangoService.saveEdgesOfTwoCollections(AT.collections.MeasuredIn, dataMeasuredIn,
        //     AT.collections.GeneralParams, AT.collections.ExtUnits, null, true, true);

        // var dataDefinedBy = [
        //     ['defRes00', `linearForce`, `linearMotionEnum`],//, {condition: { value: "rotary"} }]
        //     ['defRes01', `torque`, `rotaryMotionEnum`, { value: "linear" }]
        // ]
        // await arangoService.saveEdgesOfTwoCollections(AT.collections.DefinedBy, dataDefinedBy,
        //     AT.collections.Res, AT.collections.MeasuredIn, null, true, true);
    }
    catch (ex) {
        console.error(ex)
    }

    await createKnowledges();

    await createElectricBatteryOntology();
    await createAerospaceOntology();
    await createOntologiesStructure();

    await createComponentSchemas();
}

async function createComponentSchemas() {
    var shemas = [
        { _key: "componenentShema", shortLabel: "Componenent Shema", longLabel: "" },
        { _key: "satelliteTopLevelShema", shortLabel: "satellite Top Level Schema", longLabel: "" }

    ]
    var res1 = await AT.collections.ComponentTypes.import(shemas, { onDuplicate: "replace" });
    var sublcassOf = [
        ["satelliteTopLevelShema", "componenentShema"]
    ]
    await arangoService.importEdgesOfTwoCollections(AT.collections.SubclassOf, sublcassOf,
        AT.collections.ComponentTypes, AT.collections.ComponentTypes, "subcSchemaA");


    // //spacecraftStructure Ontology
    // await skgModelsService.addComponentTypeRelation("spacecraft", "spacecraftBus", "ssOntConsistsOfSC1");
    // await skgModelsService.addComponentTypeRelation("spacecraft", "spacecraftPayload", "ssOntConsistsOfSC2");
    // await skgModelsService.addComponentTypeRelation("spacecraft", "cableHarness", "ssOntConsistsOfSC3");
    // await skgModelsService.addComponentTypeRelation("spacecraftBus", "AOCS", "ssOntConsistsOfBus1")
    // await skgModelsService.addComponentTypeRelation("spacecraftBus", "ADCS", "ssOntConsistsOfBus2")
    // await skgModelsService.addComponentTypeRelation("spacecraftBus", "GNCsystem", "ssOntConsistsOfBus3")
    // await skgModelsService.addComponentTypeRelation("spacecraftBus", "CDHsubsystem", "ssOntConsistsOfBus4")
    // await skgModelsService.addComponentTypeRelation("spacecraftBus", "EPS", "ssOntConsistsOfBus5")
    // await skgModelsService.addComponentTypeRelation("EPS", "PCDU", "ssOntConsistsOfEPS1")
    // await skgModelsService.addComponentTypeRelation("spacecraftBus", "TCS", "ssOntConsistsOfBus6");
    // await skgModelsService.addComponentTypeRelation("TCS", "PTCS", "ssOntConsistsOfTCS1");
    // await skgModelsService.addComponentTypeRelation("TCS", "ATCS", "ssOntConsistsOfTCS2");
    // await skgModelsService.addComponentTypeRelation("spacecraftBus", "spacecraftPropulsionSubsystem", "ssOntConsistsOfBus7")
    // await skgModelsService.addComponentTypeRelation("spacecraftBus", "TTCsystem", "ssOntConsistsOfBus8")
    var shemasConrete = [
        {
            _key: "testCubesatSchema", shortLabel: "Cubesat schema",
            "@context": "baseLinkedData",
            "@type": "schema",
            shemaStructure: {
                "@context": "baseLinkedData", //"componentsTopStructureApp",
                "@type": "skbComponents/entityList",
                "entityList": [
                    { "@type": "skbConsistsof", _id: "unique123a", _from: "skbComponentTypes/spacecraft", _to: "skbComponentTypes/spacecraftBus" },
                    { "@type": "skbComponentType",  _id: "spacecraftsContext/spacecraftBus", "@context": "physicalComponentsStructure" },
                    { "@type": "skbSubclassOf", _from: "spacecraftsContext/spacecraftBus", _to: "pcs/physicalComponent", "@context": "physicalComponentsStructure" },
                    { "@type": "spacecraftBus", _id: "spacecraftsContext/spacecraftBus123", "@context": "spacecraftsContext" },
                    //is can be equal to this
                    { "@type": "skbComponent", _id: "spacecraftBus123", "@context": "physicalComponentsStructure" },
                    { "@type": "skbRealisationOf", _from: "spacecraftBus123", _to: "spacecraftBus", "@context": "physicalComponentsStructure" },

                    { "@type": "rotatesAround", _from: "spacecraftsContext/marsCube", _to: "spaceContext/mars", "@context": "spaceContext" },
                    //
                    { "@type": "skbConsistsof", _from: "skbSpaceContext/spacecraft", _to: "skbComponentTypes/spacecraftPayload" },
                    { "@type": "skbConsistsof", _from: "skbSpaceContext/spacecraft", _to: "skbComponentTypes/cableHarness" },
                    { "@type": "skbConsistsof", _from: "physicalComponentsStructure/spacecraftBus", _to: "spacecraftStructureContext/AOCS" },
                    { "@type": "skbConsistsof", _from: "physicalComponentsStructure/spacecraftBus", _to: "spacecraftStructureContext/ADCS" },
                    // { "@type": "createdAt", _from: "testCubesatSchema", _to: "skbComponentTypes/createdAtschema1Af12-1234-A" },
                    // { "@type": "skbHasParam", _id: "a1", _from: "testCubesatSchema", _to: "skbGeneralParams/createdAt" },
                    // { "@type": "skbParamValues", _from: "a1", _to: "skbExtUnits/any", value: "123:45" },
                ]
            },
            "createdBy": "asd",
            "createdAt": {
                "@context": "contexts/baseEntityTracking",
                "@type": "createdAtTime",
                value:  "123:45" 
            }
        }
    ]
    var res1 = await AT.collections.baseData.import(shemasConrete, { onDuplicate: "replace" });

    ////entity._id exists
    ////link is entity
    //link._to exists
    //link._from exists
    //skbConsistsof is link

    //skbParamValue is link
    //skbParamValue.value exists 


    var boltAA2materialsStructure = {
        _key: "boltAA2materialsStructure",
        "@type": "schema",
        shemaStructure: {
            "@type": "entityList",
            "entityList": [
                //what is nikel is material or chemistryElement?
                { "@type": "material", _id: "skbMaterials/nickel", "@context": "skbMaterials" },
                { "@type": "chemistryElement", _id: "skbChemistry/nickel", "@context": "skbChemistry" },
                //
                { "@type": "skbConsistsof", "@context": "contexts/materialsStructure", _from: "skbComponents/boltAA2", _to: "skbMaterials/nickel" },
                //Titanal is alloy or is material? skbAlloy/Titanal
                { "@type": "skbConsistsof", "@context": "contexts/materialsStructure", _from: "skbComponents/boltAA2", _to: "skbMaterials/TitanalAluminiumzincAlloy" },
            ]
        }
    }

    var TitanalAluminiumzincAlloy = {
        "_id": "someAlloyId123entityList123",
        "@context": "baseLinkedData",
        "@type": "entityList",
        "entityList": [
            { "@type": "skbConsistsof", "@context": "materialsStructure", _from: "skbComponents/boltAA2", _to: "skbMaterials/Aluminium" },
            { "@type": "skbConsistsof", "@context": "materialsStructure", _from: "skbComponents/boltAA2", _to: "skbMaterials/Zinc" },
            { "@type": "skbConsistsof", "@context": "physicalComponentsStructure", _from: "skbComponentTypes/bolt", _to: "skbComponentTypes/boltHead" },
            { "@type": "skbConsistsof", "@context": "physicalComponentsStructure", _from: "skbComponentTypes/bolt", _to: "skbComponentTypes/boltShank" },
            { "@type": "skbHasParam", "@context": "contexts/params",  _id: "a1", _from: "someAlloyId123", _to: "skbGeneralParams/Density" },
            { "@type": "skbParamValues", "@context": "contexts/params", _from: "a1", _to: "skbExtUnits/kgm3", value: 44 },
        ]
    }

    var boltAA2mechanicalStructure = {
        "@type": "entityList",
        "entityList": [
            {
                "@type": "skbConsistsof", "@context": "materialsStructure",
                _from: "skbComponents/boltAA2", _to: "skbComponents/boltHead123"
            },
            {
                "@type": "skbConsistsof", "@context": "materialsStructure",
                _from: "skbComponents/boltAA2", _to: "skbComponents/boltShank45"
            },
        ]
    }

    var boltMechanicalStructure = {
        "@type": "entityList",
        "entityList": [
            {
                "@type": "skbConsistsof", "@context": "contexts/physicalComponentsStructure",
                _from: "skbComponentTypes/bolt", _to: "skbComponentTypes/boltHead"
            },
            {
                "@type": "skbConsistsof", "@context": "contexts/physicalComponentsStructure",
                _from: "skbComponentTypes/bolt", _to: "skbComponentTypes/boltShank"
            },
        ]
    }


    var dataRealisationOf = [
        [`testCubesatSchema`, "satelliteTopLevelShema"]
    ]
    await arangoService.importEdgesOfTwoCollections(AT.collections.RealisationOf, dataRealisationOf,
        AT.collections.Components, AT.collections.ComponentTypes);


}

async function createSocialExamples(){
    var socialExample1 = [
        {
            _key: "socialExample1", shortLabel: "Social example 1",
            "@context": "baseLinkedData",
            "@type": "skbComponents/schema",
            shemaStructure: {
                "@context": "baseLinkedData", //"componentsTopStructureApp",
                "@type": "skbComponents/entityList",
                "entityList": [
                    { "@type": "componentType",  _id: "event", "@context": "pcs" },
                    { "@type": "subclassOf", _from: "pcs/event", _to: "pcs/rootComponent", "@context": "pcs" },
                    { "@type": "componentType",  _id: "skSocial/socialEvent", "@context": "pcs" },
                    { "@type": "subclassOf", _from: "skSocial/socialEvent", _to: "pcs/event", "@context": "pcs" },
                    { "@type": "componentType",  _id: "skSocial/socialMeeting", "@context": "pcs" },
                    { "@type": "subclassOf", _from: "skSocial/socialMeeting", _to: "skSocial/socialEvent", "@context": "pcs" },
                    { "@type": "componentType",  _id: "skSocial/socialOnlineMeeting", "@context": "pcs" },
                    { "@type": "subclassOf", _from: "skSocial/socialOnlineMeeting", _to: "skSocial/socialMeeting", "@context": "pcs" },
                ]
            },
            "createdBy": "asd"
        }
    ]
    var res1 = await AT.collections.baseData.import(socialExample1, { onDuplicate: "replace" });
}

async function createAerospaceOntology() {
    try {
    }
    catch (ex) {
        console.error(ex)
    }

    await createSatOntology();
    await createSatClementine();
    await createSatHubble();
    await createSatTET1();
}

async function createSatTET1() {
    var sat = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/spacecraft`, {
        _key: "satTET1",
        shortLabel: "TET-1", longLabel: "Technologie Erprobungs Träger-1"
    });

    //create and Nickel-Hydrogen battery component
    var c1 = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/NickelHydrogenBattery`, {
        _key: "satTET1battery",
        desc: [
            {
                text: "after having experienced some problems shortly before launch, the batteries are performing excellently",
                ref: "https://directory.eoportal.org/web/eoportal/satellite-missions/t/tet-1"
            },
            {
                text: "Some cellswithin the battery pack had undergone reversal and the batteries were experiencing an higher than nominaltemperature.  The first priority was to reduce the temperature of the batteries.  This was achieved by loweringthe amount of charge put into the battery; however this method was abandoned as theN iH2batteries areprone to a ”memory effect”.  It was then decided to try and achieve a reduction in temperature with the helpof a different orientation of the satellite.  ",
                ref: "https://www.researchgate.net/publication/282655467_Operational_Experience_with_Nickel_Hydrogen_and_Lithium_Ion_Batteries"
            },
        ]
    });
    await skgModelsService.attachComponentToParentComp(c1._id, sat._id);

}

async function createSatClementine() {
    var sat = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/spacecraft`, { _key: "satClementine" });

    //create and Nickel-Hydrogen battery component
    var c1 = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/NickelHydrogenBattery`, {
        _key: "satClementineBattery",
        desc: {
            book: { name: "Hydrogen as a Future Energy Carrier" }
        }
    });
    await skgModelsService.attachComponentToParentComp(c1._id, sat._id);
}

async function createSatHubble() {
    //create spacecraft
    var satHubble = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/spacecraft`, { _key: "satHubble" });

    //create and Nickel-Hydrogen battery component
    var c1 = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/NickelHydrogenBattery`, {
        _key: "satHubbleBattery",
        desc: {
            ref: "https://www.nasa.gov/mission_pages/hubble/servicing/SM4/main/Battery_FS_HTML.html"
        }
    });
    await skgModelsService.attachComponentToParentComp(c1._id, satHubble._id);
}

async function createSatOntology() {
    try {
        var ComponentTypes = [
            { _key: "rootComponentType" },
            { _key: "physicalComponent" },
            {
                _key: "systemElement", desc: `is a key element aggregate knowledge on
            system hierarchy, life-cycle or domain`, ref: "https://indico.esa.int/event/310/contributions/4571/attachments/3506/4642/Presentation_-_1220_-_Claude_Cazenave__Harald_Eisenmann.pdf",
                standart: "ECSS-E-TM-10-23"
            },
            { _key: "generalSensor", name: "general Sensor", "todoParsing": "https://en.wikipedia.org/wiki/List_of_sensors#Electric_current,_electric_potential,_magnetic,_radio" },
            { _key: "temperatureSensor", "provide": [{ "name": "temperature information", "value": "Kelvins or Celcia" }] },
            { _key: "electricalTemperatureSensor", "consume": ["electricCurrent"], "desc": { "ref": "https://en.wikipedia.org/wiki/List_of_temperature_sensors#Electrical_temperature_sensors" } },
            { _key: "pressureSensor", desc: { text: "is a device for pressure measurement of gases or liquids", wiki: "https://en.wikipedia.org/wiki/Pressure_sensor" } },

            { _key: "spacecraft" },

            { _key: "spaceOrbit" },
            { _key: "geocentricOrbit", wiki: "https://en.wikipedia.org/wiki/Geocentric_orbit" },
            { _key: "electricPowerSource", provide: "electricCurrent" }, //IsDuplicateBad
            { _key: "solarPanel" },
            { _key: "movedSolarPanel" },
            { _key: "actuator", lowLevel: true },
            { _key: "linearActuator", lowLevel: true, wiki: "https://en.wikipedia.org/wiki/Linear_actuator" },
            { _key: "rotaryActuator", lowLevel: true, wiki: "https://en.wikipedia.org/wiki/Rotary_actuator" },
            { _key: "rotatedIn1AxisSolarPanel", specific: true },
            { _key: "randomlyMovedSolarPanel", specific: true }, //hypotetic
            //{ _key: "energyStorage" }, //batteryEnergySotrage is child of energyStorage, not an electricBattery
            { _key: "electricBattery", provide: "electricCurrent" }, //IsDuplicateBad
            { _key: "rechargeableBattery", consume: "electricCurrent", wiki: "https://en.wikipedia.org/wiki/Rechargeable_battery" },

            { _key: "valve", desc: "directs or controls the flow of a fluid" },
            {
                _key: "controlValve", desc: {
                    text: `valve used to control fluid flow by varying the size of the
                        flow passage as directed by a signal from a controller`, ontologies: [
                        { ontology: "Automation", ref: "https://en.wikipedia.org/wiki/Control_valve", text: "In automatic control terminology, a control valve is termed a final control element" }
                    ],
                    wiki: "https://en.wikipedia.org/wiki/Control_valve"
                }
            },
            {
                _key: "pressureRegulator", def: {
                    wiki: "https://en.wikipedia.org/wiki/Pressure_regulator",
                    toLearn: "https://www.beswick.com/resources/the-basics-of-pressure-regulators/"
                }
            },
            {
                _key: "fuelPressureRegulator", specific: true, desc: {
                    refExamples: "https://www.prpracingproducts.com/product/fuel-pressure-regulators/"
                }
            },

            ///spacecraft satellite ONTOLOGY COMPONENTS
            { _key: "spacecraft", shortLabel: "Spacecraft" },
            { _key: "cableHarness", shortLabel: "Harness" },
            { _key: "spacecraftPayload", shortLabel: "Payload" },

            { _key: "spacecraftBus", desc: { text: "is the infrastructure of the spacecraft, usually providing locations for the payload", ref: "https://en.wikipedia.org/wiki/Satellite_bus" } },
            { _key: "spacecraftSubsystem", desc: { desc: "pacecraft subsystems comprise the spacecraft's bus", ref: "https://en.wikipedia.org/wiki/Spacecraft" } },
            { _key: "AOCS", shortLabel: "AOCS", desc: { text: "subsystem that responce for Attitude and Orbit Control system", developer: "GNC Systems Department" } },
            {
                _key: "ADCS", shortLabel: "ADCS", longLabel: "attitude determination and control subsystem", desc: {
                    text: "is used to change the attitude (orientation) of the spacecraft",
                    ref: "https://en.wikipedia.org/wiki/Spacecraft_design"
                }
            },
            {
                _key: "GNCsystem", shortLabel: "GNC", synonyms: ["GN&C"], desc: {
                    text: `includes both the components used for
                position determination and the components used by the Attitude Determination and Control`,
                    ref: "https://www.nasa.gov/sites/default/files/atoms/files/state_of_the_art-aug2016.pdf"
                }
            },
            { _key: "CDHsubsystem", shortLabel: "CDH", synonyms: ["C&DH"] },
            {
                _key: "EPS", shortLabel: "EPS", longLabel: "electrical power system", desc: {
                    text: "encompasses electrical power generation, storage, and distribution",
                    ref: "https://sst-soa.arc.nasa.gov/03-power",
                    refs: [
                        {
                            text: "Fundamentals of Power Electronics Controlled Electric Propulsion",
                            ref: "https://www.sciencedirect.com/science/article/pii/B9780128114070000350"
                        }
                    ]
                }
            },
            {
                _key: "PCDU", shortLabel: "PCDU", longLabel: "Power Conditioning & Distribution Unit", desc: {
                    text: "component of EPS",
                    ref: "https://sst-soa.arc.nasa.gov/03-power"
                }, examples: [{ ref: "https://space-for-space.com/ja/product/12-channel-power-conditioning-and-distribution-unit/" }]
            },

            {
                _key: "TCS", shortLabel: "TCS", longLabel: "thermal control system", desc: {
                    text: "is to keep all the spacecraft's component systems within acceptable temperature ranges during all mission phases",
                    ref: "https://en.wikipedia.org/wiki/Spacecraft_thermal_control"
                }, mayInclude: ["PTCS", "ATCS"]
            },
            { _key: "PTCS", shortLabel: "PTCS", longLabel: "Passive Thermal Control System" },
            { _key: "ATCS", shortLabel: "ATCS", longLabel: "Active Thermal Control System" },
            {
                _key: "spacecraftPropulsionSubsystem", shortLabel: "Propulsion", longLabel: "Spacecraft propulsion", desc: {
                    examples: [{
                        type: "withoutComponent", spacecraft: "Swift Gamma-Ray_Burst_Mission", ref: "https://en.wikipedia.org/wiki/Neil_Gehrels_Swift_Observatory",
                        desc: {
                            text: "The Swift spacecraft is an example of a spacecraft that does not have a propulsion subsystem"
                        }
                    }]
                }
            },
            { _key: "TTCsystem", shortLabel: "TTC", longLabel: "Telemetry, Tracking and Command system" }
        ]
        var res1 = await AT.collections.ComponentTypes.import(ComponentTypes, { onDuplicate: "replace" });

        await skgModelsService.addComponentTypeRelation("rotatedIn1AxisSolarPanel", "solarPanel", "m1consistsOfRSP0");
        await skgModelsService.addComponentTypeRelation("rotatedIn1AxisSolarPanel", "actuator", "m1consistsOfRSP1");

        //spacecraftStructure Ontology
        await skgModelsService.addComponentTypeRelation("spacecraft", "spacecraftBus", "ssOntConsistsOfSC1");
        await skgModelsService.addComponentTypeRelation("spacecraft", "spacecraftPayload", "ssOntConsistsOfSC2");
        await skgModelsService.addComponentTypeRelation("spacecraft", "cableHarness", "ssOntConsistsOfSC3");
        await skgModelsService.addComponentTypeRelation("spacecraftBus", "AOCS", "ssOntConsistsOfBus1")
        await skgModelsService.addComponentTypeRelation("spacecraftBus", "ADCS", "ssOntConsistsOfBus2")
        await skgModelsService.addComponentTypeRelation("spacecraftBus", "GNCsystem", "ssOntConsistsOfBus3")
        await skgModelsService.addComponentTypeRelation("spacecraftBus", "CDHsubsystem", "ssOntConsistsOfBus4")
        await skgModelsService.addComponentTypeRelation("spacecraftBus", "EPS", "ssOntConsistsOfBus5")
        await skgModelsService.addComponentTypeRelation("EPS", "PCDU", "ssOntConsistsOfEPS1")
        await skgModelsService.addComponentTypeRelation("spacecraftBus", "TCS", "ssOntConsistsOfBus6");
        await skgModelsService.addComponentTypeRelation("TCS", "PTCS", "ssOntConsistsOfTCS1");
        await skgModelsService.addComponentTypeRelation("TCS", "ATCS", "ssOntConsistsOfTCS2");
        await skgModelsService.addComponentTypeRelation("spacecraftBus", "spacecraftPropulsionSubsystem", "ssOntConsistsOfBus7")
        await skgModelsService.addComponentTypeRelation("spacecraftBus", "TTCsystem", "ssOntConsistsOfBus8")

        // var SubclassOf = [
        //     ["_key", "_from", "_to"],
        //     ["subcBase0", `${colComponentTypes.name}/generalSensor`, `${colComponentTypes.name}/rootComponentType`],
        //     ["subcBase1", `${colComponentTypes.name}/spaceOrbit`, `${colComponentTypes.name}/rootComponentType`],
        //     ["subcBase2", `${colComponentTypes.name}/electricPowerSource`, `${colComponentTypes.name}/rootComponentType`],

        //     ["subcA0", `${colComponentTypes.name}/temperatureSensor`, `${colComponentTypes.name}/generalSensor`],
        //     ["subcA1", `${colComponentTypes.name}/electricalTemperatureSensor`, `${colComponentTypes.name}/temperatureSensor`],
        //     ["subcA2", `${colComponentTypes.name}/geocentricOrbit`, `${colComponentTypes.name}/spaceOrbit`],
        //     ["subcA3", `${colComponentTypes.name}/solarPanel`, `${colComponentTypes.name}/electricPowerSource`],
        //     ["subcA4", `${colComponentTypes.name}/electricBattery`, `${colComponentTypes.name}/electricPowerSource`],
        //     ["subcA5", `${colComponentTypes.name}/rechargeableBattery`, `${colComponentTypes.name}/electricBattery`]
        // ]
        // var res2 = await  AT.collections.SubclassOf.import(SubclassOf, { "type": null, onDuplicate: "replace" });

        var SubclassOf = [
            ["spaceOrbit", "rootComponentType"],
            ["physicalComponent", "rootComponentType"],

            ["generalSensor", "physicalComponent"],
            ["spacecraft", "physicalComponent"],
            ["electricPowerSource", "physicalComponent"],
            ["actuator", "physicalComponent"],
            ["valve", "physicalComponent"],
            ["spacecraft", "physicalComponent"],
            ["spacecraftSubsystem", "physicalComponent"],
            ["spacecraftPayload", "spacecraftSubsystem"],
            ["cableHarness", "physicalComponent"],

            ["spacecraftBus", "spacecraftSubsystem"],
            ["AOCS", "spacecraftSubsystem"],
            ["ADCS", "spacecraftSubsystem"],
            ["GNCsystem", "spacecraftSubsystem"],
            ["CDHsubsystem", "spacecraftSubsystem"],
            ["EPS", "spacecraftSubsystem"],
            ["EPS", "electricPowerSource"],
            ["PCDU", "electricPowerSource"],
            ["TCS", "spacecraftSubsystem"],
            ["PTCS", "TCS"],
            ["ATCS", "TCS"],
            ["spacecraftPropulsionSubsystem", "spacecraftSubsystem"],
            ["TTCsystem", "spacecraftSubsystem"],

            ["temperatureSensor", "generalSensor"],
            ["electricalTemperatureSensor", "temperatureSensor"],
            ["pressureSensor", "generalSensor"],
            ["geocentricOrbit", "spaceOrbit"],
            ["solarPanel", "electricPowerSource"],
            ["movedSolarPanel", "solarPanel"],
            ["rotatedIn1AxisSolarPanel", "movedSolarPanel"],
            ["randomlyMovedSolarPanel", "movedSolarPanel"],
            ["linearActuator", "actuator"],
            ["rotaryActuator", "actuator"],
            ["electricBattery", "electricPowerSource"],
            ["rechargeableBattery", "electricBattery"],

            ["controlValve", "valve"],
            ["pressureRegulator", "controlValve"],
            ["fuelPressureRegulator", "pressureRegulator"]
        ]
        await arangoService.importEdgesOfTwoCollections(AT.collections.SubclassOf, SubclassOf,
            AT.collections.ComponentTypes, AT.collections.ComponentTypes, "subcA");


        var dataProvide = [
            // ["_key", "_from", "_to"],
            ["prov0", `temperatureSensor`, `temperatureInformation`],
            ["prov1", `electricPowerSource`, `electricCurrent`],
            ["prov2", `movedSolarPanel`, `changedGeometry`],
            //linear actuator or otate actuator
            ["prov3", `actuator`, `physicalForce`],
            ["prov4", `rotaryActuator`, `torque`, { insteadOf: "physicalForce" }],
            ["prov5", `linearActuator`, `linearForce`],
            ["prov6", `valve`, `fluid`],
            ["prov7", `fuelPressureRegulator`, `fluidFuel`],
            ["prov8", `pressureSensor`, `pressureInformation`]
        ]
        await arangoService.saveEdgesOfTwoCollections(AT.collections.Provide, dataProvide,
            AT.collections.ComponentTypes, AT.collections.Res, null, true, true);

        var dataInsteadOf = [
            // ["_key", "_from", "_to"],
            ["insteadOf0", `prov4`, `prov3`],
            ["insteadOf1", `prov5`, `prov3`],
            ["insteadOf2", `prov7`, `prov6`]
        ]
        await arangoService.saveEdgesOfTwoCollections(AT.collections.InsteadOf, dataInsteadOf,
            AT.collections.Provide, AT.collections.Provide, null, true, true);

        var dataConsume = [
            ["_key", "_from", "_to"],
            ["cons0", `${AT.collections.ComponentTypes.name}/electricalTemperatureSensor`, `${AT.collections.Res.name}/electricCurrent`],
            ["cons1", `${AT.collections.ComponentTypes.name}/solarPanel`, `${AT.collections.Res.name}/sunlightCurrent`],
            ["cons2", `${AT.collections.ComponentTypes.name}/actuator`, `${AT.collections.Res.name}/rotateCommand`],
            ["cons3", `${AT.collections.ComponentTypes.name}/valve`, `${AT.collections.Res.name}/fluid`],
            ["cons4", `${AT.collections.ComponentTypes.name}/controlValve`, `${AT.collections.Res.name}/command`],
            ["cons5", `${AT.collections.ComponentTypes.name}/fuelPressureRegulator`, `${AT.collections.Res.name}/fluidFuel`],
            ["cons6", `${AT.collections.ComponentTypes.name}/rechargeableBattery`, `${AT.collections.Res.name}/electricCurrent`]
        ]
        var result = await AT.collections.Consume.import(dataConsume, { "type": null, onDuplicate: "replace" });
        console.log(result)

        var dataInsteadOf = [
            // ["_key", "_from", "_to"],
            ["insteadOfC0", `cons5`, `cons3`]
        ]
        await arangoService.saveEdgesOfTwoCollections(AT.collections.InsteadOf, dataInsteadOf,
            AT.collections.Consume, AT.collections.Consume, null, true, true);

        var dataHasParam = [
            ["temperatureSensor", "interfaceType"],
            ["electricalTemperatureSensor", "incomeElectricCurrent"],
            ["electricPowerSource", "interfaceType"],
            ["electricPowerSource", "outcomeElectricCurrent"],
            ["geocentricOrbit", "sunlightCurrent"],
            ["electricBattery", "electrochemicalTechnology"],//hasParamA5,
            ["rechargeableBattery", "numberOfRechargeCycles"],
            ["movedSolarPanel", "motionType"],
            ["movedSolarPanel", "geometry"],
            ["physicalComponent", "mass"],
            ["pressureRegulator", "outputPressure"], //hasParamA10,
            ["pressureRegulator", "inletPressure"],
            ["fuelPressureRegulator", "fuelType"],
            ["pressureSensor", "interfaceType"],
            ["electricPowerSource", "outcomeVoltage"],
            ["electricBattery", "electricCapacity"], //hasParamA15
            ["electricBattery", "energyDensity"],
            ["electricBattery", "physicalVolume"],
            ["electricBattery", "operatingTemperature", { desc: { ref: "https://www.eaglepicher.com/sites/default/files/Contego%20325%200119.pdf" } }],
            ["electricBattery", "CRate"],
            ["rechargeableBattery", "capacityLossPerCycle", { desc: { ref: "https://en.wikipedia.org/wiki/Capacity_loss" } }], //hasParamA20
            ["electricBattery", "depthOfDischarge", {}],
            ["spacecraftSubsystem", "funcitonalMode"],
            ["rechargeableBattery", "incomeVoltage"],
            ["rechargeableBattery", "interfaceType", { shortLabel: "interfaceType for income"}], //interfacetype for income (mb it different interface)
            ["physicalComponent", "materialMadeOf"] //hasParamA25
            // ["physicalComponent", "size"]
        ]
        // await  AT.collections.HasParam.import(dataHasParam, { "type": null, onDuplicate: "replace" });
        await arangoService.saveEdgesOfTwoCollections(AT.collections.HasParam, dataHasParam,
            AT.collections.ComponentTypes, AT.collections.GeneralParams, "hasParamA", false, true);

        // var dataHasParam2 = [
        //  //   ['hasParam0G', `movedSolarPanel`, `geometry`, { restriction: "oneAxis" }],
        // ]
        // await arangoService.saveEdgesOfTwoCollections(AT.collections.HasParam, dataHasParam2,
        //     AT.collections.ComponentTypes, AT.collections.GeneralParams, null, true, true);

        var dataThrough = [
            ["_from", "_to"],
            [`${AT.collections.Provide.name}/prov0`, `${AT.collections.HasParam.name}/hasParamA0`],
            [`${AT.collections.Consume.name}/cons0`, `${AT.collections.HasParam.name}/hasParamA1`],
            [`${AT.collections.Provide.name}/prov1`, `${AT.collections.HasParam.name}/hasParamA2`],
            [`${AT.collections.Provide.name}/prov1`, `${AT.collections.HasParam.name}/hasParamA3`],
            [`${AT.collections.Provide.name}/prov1`, `${AT.collections.HasParam.name}/hasParamA14`],
            [`${AT.collections.Provide.name}/prov2`, `${AT.collections.HasParam.name}/hasParamA8`],
            [`${AT.collections.Provide.name}/prov8`, `${AT.collections.HasParam.name}/hasParamA13`],
            [`${AT.collections.Provide.name}/prov7`, `${AT.collections.HasParam.name}/hasParamA12`],
            [`${AT.collections.Consume.name}/cons5`, `${AT.collections.HasParam.name}/hasParamA12`],
            [`${AT.collections.Consume.name}/cons6`, `${AT.collections.HasParam.name}/hasParamA23`],
            [`${AT.collections.Consume.name}/cons6`, `${AT.collections.HasParam.name}/hasParamA24`],
            //todo ref IF pressureRegulator from valve.prov6 to pressureRegulator.outputPressure
        ]
        await AT.collections.Through.import(dataThrough, { "type": null, onDuplicate: "replace" });

        // var restrictions = [
        //     { _key: "oneAxis" },
        // ]
        // var res1 = await  AT.collections.Restrictions.import(restrictions, { onDuplicate: "replace" });

        var dataDefinedBy = [
            ["_from", "_to"],
            [`${AT.collections.ComponentTypes.name}/temperatureSensor`, `${AT.collections.Provide.name}/prov0`],
            [`${AT.collections.ComponentTypes.name}/pressureSensor`, `${AT.collections.Provide.name}/prov8`],
            [`${AT.collections.ComponentTypes.name}/electricalTemperatureSensor`, `${AT.collections.Consume.name}/${dataConsume[1][0]}`],
            [`${AT.collections.ComponentTypes.name}/electricPowerSource`, `${AT.collections.Provide.name}/prov1`],
            [`${AT.collections.ComponentTypes.name}/solarPanel`, `${AT.collections.Consume.name}/${dataConsume[2][0]}`],
            [`${AT.collections.ComponentTypes.name}/movedSolarPanel`, `${AT.collections.Provide.name}/prov2`],
            [`${AT.collections.ComponentTypes.name}/electricBattery`, `${AT.collections.HasParam.name}/hasParamA5`],
            [`${AT.collections.ComponentTypes.name}/rotaryActuator`, `${AT.collections.Provide.name}/prov4`],
            [`${AT.collections.ComponentTypes.name}/linearActuator`, `${AT.collections.Provide.name}/prov5`],
            [`${AT.collections.ComponentTypes.name}/valve`, `${AT.collections.Provide.name}/prov6`],
            [`${AT.collections.ComponentTypes.name}/valve`, `${AT.collections.Consume.name}/cons3`],
            [`${AT.collections.ComponentTypes.name}/controlValve`, `${AT.collections.Consume.name}/cons4`],
            //TODO we should say that it change and control pressure
            [`${AT.collections.ComponentTypes.name}/pressureRegulator`, `${AT.collections.HasParam.name}/hasParamA10`],
            [`${AT.collections.ComponentTypes.name}/pressureRegulator`, `${AT.collections.HasParam.name}/hasParamA11`],
            [`${AT.collections.ComponentTypes.name}/fuelPressureRegulator`, `${AT.collections.Provide.name}/prov7`],
            [`${AT.collections.ComponentTypes.name}/fuelPressureRegulator`, `${AT.collections.Consume.name}/cons5`]
        ]
        await AT.collections.DefinedBy.import(dataDefinedBy, { "type": null, onDuplicate: "replace" });

        var dataMeasuredIn = [
            ["hasParamA1", "minAmper"],
            ["hasParamA1", "maxAmper"],
            ["hasParamA3", "minAmper"],
            ["hasParamA3", "avgAmper"],
            ["hasParamA3", "maxAmper"],
            ["hasParamA4", "kW-m2"], //MeasuredIn5
            ["hasParamA7", "stringEnum"],
            ["hasParamA8", "any"],
            ["hasParamA9", "kg"],
            ["hasParamA10", "bar"],
            ["hasParamA11", "bar"], //MeasuredIn10
            ["hasParamA12", "stringEnum"],
            ["hasParamA14", "avgVolt"],
            ["hasParamA15", "milliAmperHour"], //https://www.eaglepicher.com/sites/default/files/Contego%20325%200119.pdf
            ["hasParamA16", "nominalWattHourPerLiter"],
            ["hasParamA17", "nominalLiter"], //MeasuredIn15
            ["hasParamA5", "any"],
            ["hasParamA22", "stringEnum"],
            ["hasParamA14", "minVolt"],
            ["hasParamA14", "maxVolt"],
            ["hasParamA23", "minVolt"],
            ["hasParamA23", "maxVolt"],
            // ["hasParamA25", "any"],
        ]
        // await  AT.collections.MeasuredIn.import(dataMeasuredIn, { "type": null, onDuplicate: "replace" });
        await arangoService.importEdgesOfTwoCollections(AT.collections.MeasuredIn, dataMeasuredIn,
            AT.collections.HasParam, AT.collections.ExtUnits, "MeasuredIn");

        // var dataDefinedBy = [
        //     ['defComp100', `rotatedIn1AxisSolarPanel`, `hasParamA7`, { value: "rotary",  restriction: "oneAxis" }],
        // ]
        // await arangoService.saveEdgesOfTwoCollections(AT.collections.DefinedBy, dataDefinedBy,
        //     AT.collections.ComponentTypes, AT.collections.HasParam, null, true, true);

        var dataDefinedBy = [
            ['defComp100', `rotatedIn1AxisSolarPanel`, `MeasuredIn6`, { value: "rotary", restriction: "oneAxis" }],
            ['defComp100', `EPS`, `MeasuredIn17`, {
                values: [
                    { value: "Off" },
                    { value: "StandBy" },
                    { value: "On" }
                ]
            }],
        ]
        await arangoService.saveEdgesOfTwoCollections(AT.collections.DefinedBy, dataDefinedBy,
            AT.collections.ComponentTypes, AT.collections.MeasuredIn, null, true, true);



        // var generalSensor = await  AT.collections.ComponentTypes.lookupByKeys(["rootComponentType"])
        // var list1 =  AT.collections.ComponentTypes.list();
        // var cursor = await db.query(aql`FOR v,e,path IN 2..5 INBOUND ${generalSensor[0]} skbSubclassOf RETURN {_key: v._id, parent: e._to}`);
        // var data2 = await cursor.all();
        // console.log(data2);
        // return data2;
    }
    catch (ex) {
        throw ex
    }
}

async function createSatModel1() {
    try {
        var Components = [
            { _key: "ab95SolPan" },
            { _key: "ab44Battery" },
            { _key: "abk45termSens" },
            { _key: "orbit34_42" }
        ]
        var res1 = await AT.collections.Components.import(Components, { onDuplicate: "replace" });

        var dataRealisationOf = [
            [`ab95SolPan`, AT.componentTypes.solarPanel],// "solarPanel"],
            [`ab44Battery`, "rechargeableBattery"],
            [`abk45termSens`, "electricalTemperatureSensor"],
            [`orbit34_42`, "geocentricOrbit"]
        ]
        await arangoService.importEdgesOfTwoCollections(AT.collections.RealisationOf, dataRealisationOf, AT.collections.Components, AT.collections.ComponentTypes);

        await skgModelsService.createModel("model1", true)
        await skgModelsService.attachComponentToModel("ab95SolPan", "model1", "m1consistsOf0")

        var c1 = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/fuelPressureRegulator`, { _key: "fuelPressureRegulatorL1" });
        await skgModelsService.attachComponentToModel(c1._id, "model1");
        var c2 = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/pressureSensor`, { _key: "pressureSensorL1_2" });
        await skgModelsService.attachComponentToParentComp(c2._id, c1._id);
        var c3 = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/pressureRegulator`, { _key: "pressureRegulatorL2" });
        await skgModelsService.attachComponentToModel(c3._id, "model1");
        var c3 = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/rechargeableBattery`, { _key: "electricBatteryB1" });
        await skgModelsService.attachComponentToModel(c3._id, "model1");
        var c3 = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/rechargeableBattery`, { _key: "electricBatteryB2" });
        await skgModelsService.attachComponentToModel(c3._id, "model1");
        var c3 = await skgModelsService.createComponent(`${AT.collections.ComponentTypes.name}/solarPanel`, { _key: "solarPan111" });
        await skgModelsService.attachComponentToModel(c3._id, "model1");
        // var  AT.collections.M1ParamValues = db.edgeCollection(`${cPrefix}M1ParamValues`);
        // await arangoService.createCollection(colM1ParamValues);
        // await  AT.collections.M1ParamValues.truncate()
    }
    catch (ex) {
        console.error(ex);
    }
}

async function createElectricBatteryOntology() {
    var ComponentTypes = [
        // { _key: "electricBattery", provide: "electricCurrent" }, //IsDuplicateBad
        // { _key: "rechargeableBattery", consume: "electricCurrent", wiki: "https://en.wikipedia.org/wiki/Rechargeable_battery" },
        //Primary batteries and their characteristics
        {
            _key: "ZincCarbonBattery", longLabel: "Zinc–carbon battery", desc: {
                text: "",
                ref: "https://en.wikipedia.org/wiki/Zinc–carbon_battery"
            }
        },

        ////Secondary (rechargeable) batteries and their characteristics
        {
            _key: "NickelCadmiumBattery", longLabel: "Nickel–cadmium battery", desc: {
                text: "",
                ref: "https://en.wikipedia.org/wiki/Nickel–cadmium_battery"
            }
        },
        {
            _key: "NickelHydrogenBattery", longLabel: "Nickel–hydrogen battery", desc: {
                text: "",
                ref: "https://en.wikipedia.org/wiki/Nickel%E2%80%93hydrogen_battery"
            }
        }
    ]
    var res1 = await AT.collections.ComponentTypes.import(ComponentTypes, { onDuplicate: "replace" });
    //spacecraftStructure Ontology
    // await skgModelsService.addComponentTypeRelation("spacecraft", "spacecraftBus", "ssOntConsistsOfSC1");
    // await skgModelsService.addComponentTypeRelation("spacecraft", "spacecraftPayload", "ssOntConsistsOfSC2");


    var SubclassOf = [
        //Primary batteries and their characteristics
        ["ZincCarbonBattery", "electricBattery"],
        //Secondary (rechargeable) batteries and their characteristics
        ["NickelCadmiumBattery", "rechargeableBattery"],
        ["NickelHydrogenBattery", "rechargeableBattery"]
    ]
    await arangoService.importEdgesOfTwoCollections(AT.collections.SubclassOf, SubclassOf,
        AT.collections.ComponentTypes, AT.collections.ComponentTypes, "ebOntsubcA");

    // var dataProvide = [
    //     // ["_key", "_from", "_to"],
    //     ["prov0", `temperatureSensor`, `temperatureInformation`],
    // ]
    // await arangoService.saveEdgesOfTwoCollections(AT.collections.Provide, dataProvide,
    //     AT.collections.ComponentTypes, AT.collections.Res, null, true, true);

    // var dataInsteadOf = [
    //     // ["_key", "_from", "_to"],
    //     ["insteadOf0", `prov4`, `prov3`]
    // ]
    // await arangoService.saveEdgesOfTwoCollections(AT.collections.InsteadOf, dataInsteadOf,
    //     AT.collections.Provide, AT.collections.Provide, null, true, true);

    // var dataConsume = [
    //     ["_key", "_from", "_to"],
    //     ["cons0", `${AT.collections.ComponentTypes.name}/electricalTemperatureSensor`, `${AT.collections.Res.name}/electricCurrent`]
    // ]
    // var result = await AT.collections.Consume.import(dataConsume, { "type": null, onDuplicate: "replace" });

    // var dataInsteadOf = [
    //     // ["_key", "_from", "_to"],
    //     ["insteadOfC0", `cons5`, `cons3`]
    // ]
    // await arangoService.saveEdgesOfTwoCollections(AT.collections.InsteadOf, dataInsteadOf,
    //     AT.collections.Consume, AT.collections.Consume, null, true, true);

    // var dataHasParam = [
    //     ["temperatureSensor", "interfaceType"],
    //     ["electricalTemperatureSensor", "incomeElectricCurrent"],
    // ]
    // // await  AT.collections.HasParam.import(dataHasParam, { "type": null, onDuplicate: "replace" });
    // await arangoService.saveEdgesOfTwoCollections(AT.collections.HasParam, dataHasParam,
    //     AT.collections.ComponentTypes, AT.collections.GeneralParams, "hasParamA", false, true);

    // var dataThrough = [
    //     ["_from", "_to"],
    //     [`${AT.collections.Provide.name}/prov0`, `${AT.collections.HasParam.name}/hasParamA0`],
    //     [`${AT.collections.Consume.name}/cons0`, `${AT.collections.HasParam.name}/hasParamA1`]
    // ]
    // await AT.collections.Through.import(dataThrough, { "type": null, onDuplicate: "replace" });

    // var dataDefinedBy = [
    //     ["_from", "_to"],
    //     [`${AT.collections.ComponentTypes.name}/temperatureSensor`, `${AT.collections.Provide.name}/prov0`],
    //     [`${AT.collections.ComponentTypes.name}/pressureSensor`, `${AT.collections.Provide.name}/prov8`],

    // ]
    // await AT.collections.DefinedBy.import(dataDefinedBy, { "type": null, onDuplicate: "replace" });

    // var dataMeasuredIn = [
    //     ["hasParamA1", "minAmper"],
    //     ["hasParamA1", "maxAmper"],

    // ]
    // // await  AT.collections.MeasuredIn.import(dataMeasuredIn, { "type": null, onDuplicate: "replace" });
    // await arangoService.importEdgesOfTwoCollections(AT.collections.MeasuredIn, dataMeasuredIn,
    //     AT.collections.HasParam, AT.collections.ExtUnits, "MeasuredIn");


    var dataDefinedBy = [
        ['ebOntDefComp100', `ZincCarbonBattery`, `MeasuredIn16`, { value: "Zinc–carbon", ref: "" }],
        ['ebOntDefComp101', `NickelCadmiumBattery`, `MeasuredIn16`, { value: "Nickel–cadmium", ref: "" }],
        ['ebOntDefComp102', `NickelHydrogenBattery`, `MeasuredIn16`, { value: "Nickel–hydrogen", ref: "" }],
    ]
    await arangoService.saveEdgesOfTwoCollections(AT.collections.DefinedBy, dataDefinedBy,
        AT.collections.ComponentTypes, AT.collections.MeasuredIn, null, true, true);

}

async function createOntologiesStructure() {
    var data2 = [
        // { _key: "space" },
        { _key: "spacecraft" },
        { _key: "spacecraftMission" },
        { _key: "spacecraftStructure" },
        { _key: "smallSpacecraftStructure", desc: { ref: "https://sst-soa.arc.nasa.gov/" } },
        { _key: "electricBattery" },
    ]
    await AT.collections.Ontologies.import(data2, { onDuplicate: "replace" });

    var dataIncludes = [
        ["_from", "_to"],
        [`${AT.collections.Ontologies.name}/spacecraft`, `${AT.collections.Ontologies.name}/spacecraftStructure`],
        [`${AT.collections.Ontologies.name}/spacecraft`, `${AT.collections.Ontologies.name}/spacecraftMission`],
        [`${AT.collections.Ontologies.name}/spacecraft`, `${AT.collections.ComponentTypes.name}/spacecraft`],
        [`${AT.collections.Ontologies.name}/spacecraftStructure`, `${AT.collections.ComponentTypes.name}/spacecraft`],
        [`${AT.collections.Ontologies.name}/spacecraftStructure`, `${AT.collections.ConsistsOf.name}/ssOntConsistsOfSC1`],
        [`${AT.collections.Ontologies.name}/spacecraftStructure`, `${AT.collections.ConsistsOf.name}/ssOntConsistsOfSC2`],
        [`${AT.collections.Ontologies.name}/spacecraftStructure`, `${AT.collections.ConsistsOf.name}/ssOntConsistsOfSC3`],
        [`${AT.collections.Ontologies.name}/spacecraftMission`, `${AT.collections.ComponentTypes.name}/spaceOrbit`],
        // [`${AT.collections.Ontologies.name}/spacecraftStructure`, `${AT.collections.ConsistsOf.name}/hhh`]
        [`${AT.collections.Ontologies.name}/electricBattery`, `${AT.collections.ComponentTypes.name}/electricBattery`]
    ]
    await AT.collections.Includes.import(dataIncludes, { "type": null, onDuplicate: "replace" });

}

async function createKnowledges() {
    var data2 = [
        {
            _key: "batteryCapacityFromNumOfCyclesDependency", type: "rule",
            subject: "electricBattery",
            desc: {
                text: "Then more number of EXECUTED cycles then lower capacity AT THE MOMENT",
                ref: "https://batteryuniversity.com/learn/article/how_to_prolong_lithium_based_batteries",
                imageRef: "https://www.batteryuniversity.com/_img/content/lithium1.jpg"
            },
            rule: {
                basedOn: ["physics rules", "chemistry rules", "capacityLoss"],
                inputs: [{ id: "numberOfPerformedCycles" }, { id: "time" }, { id: "electricCapacity" }],
                ruleText1: "numberOfPerformedCycles INCREASE WITH time => electricCapacity DECREASE WITH time"
            }
        },
        {
            _key: "currentDepthOfDischarge", type: "formula",
            formula: {
                inputs: [
                    { id: "electricCapacity", fName: "C", measuredIn: "amperHour" },
                    { id: "dischargeTime", fName: "t", measuredIn: "hour" },
                    { id: "dischargeElectricCurrentDifference", fName: "d", measuredIn: "amper" }
                ],
                functions: {
                    mathjs: {
                        value: "dod = d * t / C"
                    }
                },
                outputs: [
                    { id: "depthOfDischarge", fName: "dod", measuredIn: "percent" },
                ]
            }, desc: {
                text: "",
                ref: "https://en.wikipedia.org/wiki/Depth_of_discharge",
                refBbl: {}
            }
        },
        {
            _key: "socFromDoD", type: "formula",
            formula: {
                inputs: [
                    { id: "currentDepthOfDischarge", fName: "dod", measuredIn: "percent" }
                ],
                functions: {
                    mathjs: {
                        value: "soc = 100 - DoD"
                    }
                },
                outputs: [
                    { id: "stateOfCharge", fName: "soc", measuredIn: "percent" },
                ]
            }, desc: {
                text: " An alternative form of the same measure is the depth of discharge (DoD), the inverse of SoC (100% = empty; 0% = full)",
                ref: "https://en.wikipedia.org/wiki/State_of_charge",
                refBbl: {}
            }
        }
    ]
    await AT.collections.Knowledges.import(data2, { onDuplicate: "replace" });

    // var dataIncludes = [
    //     ["_from", "_to"],
    //     [`${AT.collections.Ontologies.name}/spacecraft`, `${AT.collections.Ontologies.name}/spacecraftStructure`],

    //     // [`${AT.collections.Ontologies.name}/spacecraftStructure`, `${AT.collections.ConsistsOf.name}/hhh`]
    // ]
    // await AT.collections.Includes.import(dataIncludes, { "type": null, onDuplicate: "replace" });

}

module.exports.createAll = createAll;
// module.exports.createUnits = createUnits;