import Solution from '../models//solution';
import uuidv1 from "uuid";

async function getAvailableSolutions() {
    var result = await Solution.find().limit(20);
    return result;
}

async function create(params){
    console.log("Solutions create", params);
    
    var sols = await Solution.find({ name: params.name });
    if (sols.length > 0) {
        console.error("Solutions already exists", params.name)
        return { error: "Already exists" };
    }
    console.log(uuidv1);
    var solution = new Solution({
        ver: "v2",
        uuid: uuidv1(),
        name: params.name,
    });
    var res = await solution.save();

    return { solution: solution };
}

module.exports.getAvailableSolutions = getAvailableSolutions;
module.exports.create = create;