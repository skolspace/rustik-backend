var arangoService = require('../arangoService').service;
var aql = require('arangojs').aql;


var ruleToEdgeCols = [{ rule: "RealisationOf", cols: ["skbRealisationOf, skbSubclassOf"], depth: 5 },
{ rule: "ConsistsOf", cols: ["ConsistsOf"], depth: 5 }]

async function getItemsFromText(text){
    //RETURN TOKENS("What spacecrafts have Nickel–hydrogen battery?", "text_en") // batteri? mb use special analyzer?
    text  = "What spacecrafts have Nickel–hydrogen battery?";
    //text2  = "What TLE have spaceraftABC12 have at epoch44?";
    //вот вопрос: при посадке кораблю движется по орбите, даже же при входе в атмосферу. А шатл когда он переходит из состояния падения по орбите в состояние траектории полета?
    //Конитекст, Котекст может быть не тольско схема как в JSON-LD но и сама сущность может быть контекстом.
    //например, если поместить Spacecraft и AOCS в схему cubeSatDefaultStructureByNik появится связь Spacecraft-ConsiststOf-AOCS
    //Похоже не сложные контексты poolParty
    ///"What small spacecrafts have Nickel–hydrogen battery with capacity higger than 50 w/h?";
    //invoke sintacsis analyzer, hello yandex
    var res = [
        { index: "xx0", word: "spacecraft", type: "potentialObject" },
        { index: "xx1", word: "have", type: "potentialPredicate" },
        { index: "xx2", word: "Nickel–hydrogen battery", type: "potentialObject" }
    ]

    var res2 = [
        { index: "xx0", word: "spacecraft", foundElement:"skbComponentTypes/spacecraft" },
        { index: "yy",  word: "have", definition: "ConsistsOf(xx0,xx2)" },
        { index: "xx2", word: "Nickel–hydrogen battery", foundElement: "skbComponentTypes/NickelHydrogenBattery"}
    ]

    var result = [
        { index: "xx",  word: "spacecraft", foundElement:"skbComponentTypes/spacecraft" },
        { index: "yy",  word: "have", definition: "ConsistsOf(x,xx2)" },
        { index: "xx2", word: "Nickel–hydrogen battery", foundElement: "skbComponentTypes/NickelHydrogenBattery"}
    ]
    var result2 = [
        { word: "spacecraft", foundElement:"skbComponentTypes/spacecraft" },
        { word: "Nickel–hydrogen battery", foundElement: "skbComponentTypes/battery",
        definitions: ["skbParamValues(skbHasParam(x, electrochemicalTechnology), s).value == 'Nickel–hydrogen'" ] } 
    ]

}
async function SolveQuery(query) {
    var rules = ["skbRealisationOf(x,y) ^ skbSubclassOf(y,z) -> RealisationOf(x,z)",
        "ConsistsOf(x,y) ^ ConsistsOf(y,z) -> ConsistsOf(x,z)"]
    var queries = [
        {
            q: ["What spacecrafts have Nickel–hydrogen battery?"],
            t: ["What component of type spacecraft have subcomponent Nickel–hydrogen battery?"],
            t2: ["What component of ComponentType spacecraft have child component of ComponentType electricBattery with Nickel–hydrogen electrochemicalTechnology?"],
            t3: [`RealisationOf(x,skbComponentTypes/spacecraft) ^ 
            ConsistsOf(x,ch) ^ 
            RealisationOf(ch, electricBattery) ^
            skbParamValues(skbHasParam(ch, electrochemicalTechnology), s).value == "Nickel–hydrogen" `],
            d: {
                a0: 'skbComponentTypes/spacecraft',
                b0: 'electricBattery',
                b1: 'skbGeneralParams/electrochemicalTechnology',
                b2: "Nickel–hydrogen"
            },
            r: [aql`
            FOR v,e, path IN 1..5 INBOUND $$a0
        skbRealisationOf, skbSubclassOf
        let foundValidComp = ( for childComp, ee in 1..5 OUTBOUND v skbConsistsOf 
            let subTypes = ( for v3, e3 in 1..5 OUTBOUND childComp skbRealisationOf, skbSubclassOf
                PRUNE v3._key == $$b0
                FILTER v3._key == $$b0
                return v3)
            FILTER LENGTH(subTypes) > 0
            
            let withValidParam  = (for v3, e3 in 1..5 OUTBOUND childComp skbHasParam
                filter e3._to == $$b1
                let foundValues  = (for v4, e4 in 1..5 OUTBOUND e3 skbParamValues
                    FILTER e4.value == $$b2
                    return e4 )
                FILTER LENGTH(foundValues) > 0
                return foundValues )
            FILTER LENGTH(withValidParam) > 0
             
        return LENGTH(childComp) > 0  )
        
        FILTER foundValidComp[0] == true
        
RETURN {v, e, foundValidComp}
`]
        }
    ]

}

function fromSKBLtoAQL() {
    var q = `RealisationOf(x,$$a00) ^ 
    ConsistsOf(x,ch) ^ 
    RealisationOf(ch, $$a01) ^
    skbParamValues(skbHasParam(ch, $$a02), s).value == $$a03`;

    var hasNext = true;
    while (hasNext) {
        hasNext = false;
    }
}