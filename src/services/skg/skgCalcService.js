
// import arangoService from '../arangoService'
var arangoService = require('../arangoService').service;
//var aql = require('../arangoService').aql;
var AT = arangoService.AT;

const evaluatex = require("evaluatex");

async function calculateParamValuesForModel(modelId) {
    //get param values of graph
    //with additional relationships conditions fixed values etc
    //make graph

    var cursor = await arangoService.dbQuery(`FOR v,e,path IN 2..6 OUTBOUND 
    '${AT.collections.Models.name}/${modelId}'  
     skbConsistsOf, skbHasParam 
     FILTER IS_SAME_COLLECTION('skbHasParam', e)
     LET vals = (FOR vv, ee IN 1..3 OUTBOUND e skbParamValues
        LET inputs = (FOR v3, e3 IN 1..1 INBOUND ee skbInputs
            return e3)
        LET conditions = (FOR v3, e3 IN 1..1 INBOUND ee skbConditions
                return e3)
    
        return MERGE(ee, {_inputs: inputs, _conditions: conditions }) )
        FILTER LENGTH(vals) > 0
        RETURN MERGE(e, {comp: DOCUMENT(e._from), param: DOCUMENT(e._to),
        paramValues:  vals[*], unit: DOCUMENT(vals[*]._to) })`);
    var data2 = await cursor.all();
    console.log(data2);
    var allParamValues = _.reduce(data2, (r, c) => {
        return [].concat(r, c.paramValues)
    }, []);

    var items = allParamValues.map(v => {

        var el = { _id: v._id, valueType: v.valueType, entity: v }
        //find ids of input links
        //find ids of conditions link
        return el;
    })
    return await calculateParamValues(items);
}

function getMiltiplerCalc(extUnitId, extData, multipler = 1){
    var extUnit = extData.extUnits.find(e => e._id == extUnitId);

    if (extUnit.calcRule){
        multipler *= extUnit.calcRule
    }
    var ext = extData.exts.find(e => e._id == extUnit._from);
    if (ext._id.includes("extUnit")){
        multipler *= getMiltiplerCalc(ext._id, extData, multipler);
    }
    return multipler;
}

async function calculateParamValues(baseDependencyItems) {

    var extData = {};
    var extsC = await arangoService.dbQuery(aql`FOR v IN ${arangoService.AT.collections.UnitsExt} RETURN v`)
    extData.exts = await extsC.all();
    var extUnitsC = await arangoService.dbQuery(aql`FOR v IN skbExtUnits RETURN v`)
    extData.extUnits = await extUnitsC.all();
    // var exts = await  arangoService.AT.collections.UnitsExt.list();
    var unitsC = await arangoService.dbQuery(aql`FOR v IN skbUnits RETURN v`)
    extData.units = await unitsC.all();

    //TODO mage real dependency graph algorithm and them sort by dependencies if no cycles
    var unorderedDependencyItems = baseDependencyItems.filter(v => v.valueType == "latex")
    var dependencyItems = await orderDependencyItems(unorderedDependencyItems);
    for (var v of dependencyItems) {
        if (v.valueType == "latex") {
            var inputsFromIds = v.entity._inputs.map(i => i._from);
            var latex = v.entity.value;
            //substitute our variables by value;
            for (var inputIdFrom of inputsFromIds) {
                var re = new RegExp(`\\\\customEmbed{myVar}\\[${inputIdFrom}]`, 'g');
                var input = baseDependencyItems.find(d => d._id == inputIdFrom);
                var newValue = input.valueType == "latex" ? input.entity.viewValue : input.entity.value;
                //latex = 'sqrt{\customEmbed{myVar}[skbParamValues/749588]\customEmbed{myVar}[skbParamValues/749588]\customEmbed{myVar}[skbParamValues/749795]ds\customEmbed{myVar}[skbParamValues/749785]\customEmbed{myVar}[skbParamValues/749795]\customEmbed{myVar}[skbParamValues/749793]9}';
                var multipliyer = getMiltiplerCalc(input.entity._to, extData);
                var lValue = `(${multipliyer}*${newValue})`
                latex = latex.replace(re, lValue)
            }

            //\frac is working but \cdot no
            latex = latex.replace("\\cdot", "*");

            const fn = evaluatex(latex, { latex: true });
            const result = fn();//fn({aa: 27}, { latex: true });
            var newDep = await updateViewValue(v._id, result);
            //remove old from local and add new
            var baseDependencyItem = baseDependencyItems.find(item => item._id === v._id);
            baseDependencyItem.entity = newDep;
        }
    }
}

async function orderDependencyItems(dependencyItems) {
    var resolved = [];
    var unresolved = [];

    for (var item of dependencyItems) {
        dep_resolve(item, resolved, unresolved, dependencyItems);
    }

    if (resolved.length != dependencyItems.length)
        throw new Error("simething was wrong w")
    var res = [];
    for (var r of resolved) {
        //var item = dependencyItems.find(d => d._id == id);
        res.push(r);
    }

    return res;
}


function dep_resolve(node, resolved, unresolved, data) {
    unresolved.push(node)
    var inputsFromIds = node.entity._inputs.map(i => i._from);
    for (var fromId of inputsFromIds) {
        var from = data.find(d => d._id == fromId);
        if (from) {
            if (!resolved.includes(fromId)) {
                if (unresolved.includes(fromId)) {
                    throw new Error('Circular reference detected: %s -> %s' % (node, fromId))
                }
                if (from) {
                    dep_resolve(from, resolved, unresolved, data);
                }
            }
        }
    }
    if (!resolved.includes(node)) {
        resolved.push(node);
    }
    unresolved = unresolved.filter(item => item._id !== node._id)
}

async function updateViewValue(paramValId, newCalculatedValue) {
    try {
        var collection = await arangoService.collection(paramValId.split('/')[0]);
        var ent = await arangoService.getEntity(paramValId);
        ent.viewValue = newCalculatedValue;
        ent._rev = null;
        var res = await collection.save(ent, { overwrite: true, returnNew: true });
        return res.new;
    }
    catch (e) {
        console.log(e);
        return null;
    }
}

// let graphElement = {
//     _id: String,
//     dependsOnIds: string[]
// };

module.exports.calculateParamValues = calculateParamValues;
module.exports.calculateParamValuesForModel = calculateParamValuesForModel;