
// import arangoService from '../arangoService'
var arangoService = require('../arangoService').service;
//var aql = require('../arangoService').aql;
var AT = arangoService.AT;
// var AT = arangoService.AT;
// var db = arangoService.db;
//var db = arangoService.db;
var cPrefix = "skb"

async function createComponentByExisting(componentTypeId, baseComponentId, componentBody) {
    //get snapshot of existing component
    //with list of related entities
    var cIndex = arangoService.getWMidToCollection();
    var key = baseComponentId.split("/")[1];
    var idToCols = await cIndex.lookupByKeys([key]);
    console.log("createComponentByExisting", baseComponentId, idToCols[0])
    var idToCol = idToCols[0];
    var compColName = idToCol.value;
    var compCol = arangoService.collection(compColName);

    var originalComps = await compCol.lookupByKeys([key]);
    var originalComp = originalComps[0];
    delete originalComp._id;
    delete originalComp._key;
    delete originalComp._rev;
    var newComp = await compCol.save(originalComp);
    var newComponentId = newComp._id;

    var compTypesCol = arangoService.collection(componentTypeId.split("/")[0])
    var realisationOfCol = arangoService.collection("WM333RealisationOf-pcs");//AT.collections.RealisationOf;

    var types = await compTypesCol.lookupByKeys([componentTypeId]);
    var realization = await realisationOfCol.save({
        _from: newComponentId,
        _to: types[0]._id,
    });
    var realization2 = await realisationOfCol.save({
        _from: newComponentId,
        _to: baseComponentId,
    });


    for (var edgeId of (idToCol.isFromFor || [])) {
        if (edgeId) {
            var idToCols1 = await cIndex.lookupByKeys([edgeId]);
            if (idToCols1[0]) {
                var edgeColName = idToCols1[0].value;
                var edgeCol = arangoService.collection(edgeColName);

                var isItRequiredToMakeEdgeCopy = edgeColName.includes("HasParam") || edgeColName.includes("ParamValue");

                if (isItRequiredToMakeEdgeCopy) {
                    var edgeElements = await edgeCol.lookupByKeys([edgeId]);
                    var edgeElement = edgeElements[0]
                    if (!edgeElement) {
                        console.error("edgeElement not found", edgeId, edgeCol)
                    }
                    else {
                        var toId = edgeElement._to;

                        var colToName = toId.split("/")[0]
                        var colTo = arangoService.collection(colToName);

                        var toMakeCopyOfTo = colToName.includes("ParamValue");
                        var toElement = (await colTo.lookupByKeys([toId]))[0];
                        var newToElementId = null;
                        if (toMakeCopyOfTo) {
                            toColTo = await colTo.lookupByKeys([edgeId]);
                            if (toColTo[0]) {
                                // var toMakeCopyOfTo = 
                                //TODO make reqursievely for to exlude this path
                            }
                            else {
                                console.error("idToCol1 not found in edgeCol", pId, edgeColName)
                            }
                            newToElementId = "TODONEWID"
                        }
                        else {
                            //use old _to of edge for new edge
                            newToElementId = toElement._id;
                        }

                        delete edgeElement._id;
                        delete edgeElement._key;
                        delete edgeElement._rev;
                        edgeElement._from = newComponentId;
                        edgeElement._to = newToElementId;
                        var res2 = await edgeCol.save(edgeElement); //
                        console.log("created edgeElement", edgeElement, edgeCol)

                        //TODO make reqursievely
                    }
                }
            }
            else {
                console.error("idToCol1 not found in cIndex", pId)
            }
        }
    }

    for (var p of (idToCol.isToFor || [])) {

    }

    return newComp;
}

async function createComponent(componentTypeId, componentBody, baseComponentId = null) {
    if (baseComponentId) {
        var c = await createComponentByExisting(componentTypeId, baseComponentId, componentBody)
        return c;
    }
    var newComponent = await AT.collections.Components.save(componentBody);
    var newComponentId = newComponent._id;
    var types = await AT.collections.ComponentTypes.lookupByKeys([componentTypeId]);

    var oldNewIds = [];
    var realization = await AT.collections.RealisationOf.save({
        _from: newComponentId,
        _to: types[0]._id,
    });

    //1 get list of all provide,consume, hasParam, MeasuredIn of this type and thie parent types
    //2 create component
    //3 create same edges provide, consume, hasParam
    //4 clone MeasuredIn from type to component ParamValues)

    //db._query("With skbProvide FOR v,e,path IN 0..5 OUTBOUND 'skbComponentTypes/electricalTemperatureSensor' 
    //OUTBOUND skbSubclassOf, skbProvide, skbConsume FILTER IS_SAME_COLLECTION('skbComponentTypes', e._from) 
    //AND !IS_SAME_COLLECTION('skbComponentTypes', e._to) RETURN {_key: v}").toArray()
    //db._query("With skbProvide FOR v,e,path IN 0..5 OUTBOUND 'skbComponentTypes/electricalTemperatureSensor' OUTBOUND skbSubclassOf, skbProvide, skbConsume, skbHasParam FILTER IS_SAME_COLLECTION('skbComponentTypes', e._from) AND !IS_SAME_COLLECTION('skbComponentTypes', e._to) RETURN {_key: v, col: PARSE_IDENTIFIER(e).collection, path: e}").toArray()


    //measuredin
    //  db._query("FOR v,e IN 1..3 OUTBOUND 'skbComponentTypes/electricalTemperatureSensor' skbHasParam, skbSubclassOf FOR vv, ee IN 1 OUTBOUND e skbMeasuredIn RETURN {_key: vv, e: e, ee: ee}").toArray()
    var cursor2 = await arangoService.dbQuery(aql`FOR v,e, path IN 1..5 OUTBOUND ${types[0]}
        skbHasParam, skbSubclassOf 
        FILTER IS_SAME_COLLECTION('skbHasParam', e._id) 
        LET measArr = (FOR vv, ee IN 1 OUTBOUND e skbMeasuredIn LET measDefinitions = (FOR v3, e3 IN 1 INBOUND ee skbDefinedBy
                    FILTER POSITION(path.vertices[*]._id, e3._from) 
                    return e3)
        return { measure: ee, measureDefs: measDefinitions } )
        LET definedParams = (FOR vv, ee IN 1 INBOUND e skbDefinedBy return ee )
        LET definedMeasures = (FOR vv, ee IN 0..1 OUTBOUND e._to skbMeasuredIn return vv )
        RETURN DISTINCT { edge: e, edgeCol: PARSE_IDENTIFIER(e).collection, 
           measuredInArr: measArr, 
           definedMeasures: definedMeasures, definedParams: definedParams }`);
    var paramsAndMeasuredIn = await cursor2.all();
    console.log(paramsAndMeasuredIn);
    var res22 = _.map(paramsAndMeasuredIn, async d => {
        d.edge.baseId = d.edge._id;
        delete d.edge._id;
        delete d.edge._key;
        delete d.edge._rev;
        d.edge._from = newComponentId;

        var collection = arangoService.collection(d.edgeCol);
        var res2 = await collection.save(d.edge); //
        oldNewIds[d.edge.baseId] = res2._id;

        // var predefinedValues = [];
        // if (d.definedParams){
        //     for (var definition of d.definedParams) {
        //         predefinedValues[definition._to]  = { value: definition.value };
        //     }
        // }
        for (var measuredIndata of d.measuredInArr) {
            var measuredIn = measuredIndata.measure;
            var measureDefs = measuredIndata.measureDefs;
            measuredIn.baseId = measuredIn._id;
            delete measuredIn._id;
            delete measuredIn._key;
            delete measuredIn._rev;
            measuredIn._from = res2._id;

            var values = [];
            if (measureDefs && measureDefs.length > 0) {
                var values = [];
                for (var def of measureDefs) {
                    if (Array.isArray(def.values)) {
                        values = [].concat(values, def.values)
                    }
                    else {
                        const filtered = Object.keys(def)
                            .filter(key => !key.startsWith("_"))
                            .reduce((obj, key) => {
                                obj[key] = def[key];
                                return obj;
                            }, {});;
                        values.push(filtered);
                    }
                }
                // measuredIn.values = values;
                // measuredIn.valueType = "array";
            }
            else {
                values.push(44);
                // measuredIn.value = 44;
                // measuredIn.valueType = "fixed";
            }
            for (var v of values) {
                measuredIn.value = v.value ? v.value : v;
                measuredIn.valueType = "fixed";
                var collection44 = AT.collections.ParamValues;
                var res4 = await collection44.save(measuredIn);
                oldNewIds[measuredIn.baseId] = res4._id;
            }
        }
        return {};
    });


    var cursor = await arangoService.dbQuery(aql`FOR v,e,path IN 0..5 OUTBOUND ${types[0]} OUTBOUND 
        skbSubclassOf, skbProvide, skbConsume
        FILTER IS_SAME_COLLECTION('skbComponentTypes', e._from) 
        AND !IS_SAME_COLLECTION('skbComponentTypes', e._to) 
        let throughs = ( for vv, ee in 1 OUTBOUND e skbThrough return ee)
        let insteads = ( for vv, ee in 1 OUTBOUND e skbInsteadOf return ee)
        RETURN {targ: v, col: PARSE_IDENTIFIER(e).collection, edge: e, throughs: throughs,
            insteads: insteads}`);
    var data2 = await cursor.all();
    console.log(data2);
    var allInsteads = _.reduce(data2, (result, value, key) => {
        var t = [].concat(result, value.insteads)
        return t;
    }, []);
    console.log("allInsteads", allInsteads);
    data2 = data2.filter(d => {
        return !allInsteads.find(i => i._to == d.edge._id);
    })

    var toInsert = _.map(data2, async d => {
        d.edge.baseId = d.edge._id
        delete d.edge._id;
        delete d.edge._key;
        delete d.edge._rev;
        d.edge._from = newComponentId;

        var collection = arangoService.collection(d.col);
        var res2 = await collection.save(d.edge);
        oldNewIds[d.edge.baseId] = res2._id;

        for (var through of d.throughs) {
            d.baseId = d._id
            delete through._id;
            delete through._key;
            delete through._rev;
            through._from = oldNewIds[through._from];
            through._to = oldNewIds[through._to];

            // var collection3 = arangoService.collection(measuredInCol);
            // var res3 = await collection3.save(measuredIn);
            var res2 = await AT.collections.Through.save(through);
            oldNewIds[d.baseId] = res2._id;
        }

        return res2;
    })

    return newComponent;//[newComponent, res, res22];

}

async function createModel(name, overwrite = false) {
    var model = { _key: name }
    return await AT.collections.Models.save(model, { overwrite: overwrite });
}

async function getModel(key) {
    var cursor = await AT.collections.Models.lookupByKeys([key]);
    if (cursor[0]) {
        return cursor[0];
    }
    else {
        console.log(`${key} not found`);
        return null;
    }
}

async function getComponent(key) {
    var cursor = null;
    if (key.split("/").length > 1) {
        var col = arangoService.collection(key.split("/")[0])
        cursor = await col.lookupByKeys([key]);
    }
    cursor = await AT.collections.Components.lookupByKeys([key]);
    if (cursor[0]) {
        return cursor[0];
    }
    else {
        console.log(`${key} not found`);
        return null;
    }
}

async function getComponentType(key) {
    var cursor = await AT.collections.ComponentTypes.lookupByKeys([key]);
    if (cursor[0]) {
        return cursor[0];
    }
    else {
        console.log(`${key} not found`);
        return null;
    }
}


async function attachComponentToModel(componentKey, modelKey, key = null) {
    var component = await getComponent(componentKey);
    var model = await getModel(modelKey);
    if (!model || !component) {
        var er = new Error("model or component not found");
        er.data = [componentKey, modelKey]
        throw er;
    }
    await AT.collections.ConsistsOf.save(
        key ? { _key: key } : {},
        `${model._id}`,
        `${component._id}`
    );
}

async function attachComponentToParentComp(componentKey, parentCompId, key = null) {
    var component = await getComponent(componentKey);
    var paremtComp = await getComponent(parentCompId);
    if (!paremtComp || !component)
        throw new Exception("model or component not found", componentKey, parentCompId);
    await AT.collections.ConsistsOf.save(
        key ? { _key: key } : {},
        `${paremtComp._id}`,
        `${component._id}`
    );
}

async function getSubcomponents(parentCompId) {
    var cursor = await arangoService.dbQuery(aql`FOR v,e,path IN 1..5 OUTBOUND ${parentCompId}
     ${AT.collections.ConsistsOf}
       RETURN {_id: v._id, name: PARSE_IDENTIFIER(v).key, obj: v, parentId: e._from}`);
    var data2 = await cursor.all();
    return data2;
}

async function getCompFamily(compId, request) {
    var cursor = await arangoService.dbQuery(aql`
    LET parents = (FOR v,e,path IN 1..1 INBOUND  ${compId}
    skbConsistsOf 
           
        return v)
    LET neighbors = (FOR vv, ee IN 2..2 ANY ${compId}
        skbConsistsOf
      FILTER POSITION(parents[*]._id, ee._from)
      return   vv)
      
      LET childs = (FOR v,e,path IN 1..1 OUTBOUND ${compId}
        skbConsistsOf
      return v)
   
      RETURN { parents: parents, neighbors: neighbors, childs: childs}`);
    var data2 = await cursor.all();
    return data2[0];
}
async function addParam(compId, request) {
    var component = await getComponent(compId);
    var generalParam = request.generalParam;
    var paramData = request.paramData;
    var generalParamId;
    if (generalParam._id) {
        //add param to existed id
        var cursor = await AT.collections.GeneralParams.lookupByKeys([generalParam._id]);
        if (cursor[0]) {
            generalParamId = cursor[0]._id;
        }
        else {
            throw new Error(`GeneralParams ${generalParam._id} not found`)
        }
    }
    else {
        if (generalParam.shortLabel) {
            //add new generalparam and add link
            var newGeneralParam = {
                shortLabel: generalParam.shortLabel
            }
            var res = await AT.collections.GeneralParams.save(newGeneralParam, { returnNew: true });
            generalParamId = res.new._id;
        }
        else {
            throw new Error("New param should have shortLabel property")
        }
    }
    await AT.collections.HasParam.save(
        paramData,
        `${component._id}`,
        `${generalParamId}`
    );

}

async function addFunction(compId, functionType, resourceId) {
    var cursor = await AT.collections.Components.lookupByKeys([compId]);
    var cursorRes = await AT.collections.Res.lookupByKeys([resourceId]);
    if (cursor[0] && cursorRes[0]) {
        var col = functionType == "provide" ? AT.collections.Provide :
            functionType == "consume" ? AT.collections.Consume : null;
        if (!col) {
            new Error(`type ${functionType} not valid`);
        }

        var res = await col.save({}, cursor[0]._id, cursorRes[0]._id);
        return res;
    }
    else {
        throw new Error(`Element not found`)
    }
}

async function removeFunction(funcId) {
    var func = await arangoService.getEntity(funcId);
    if (!func) {
        new Error(`functionId ${funcId} not found`);
    }

    //remove all connections from and to this function
    var cursor = await arangoService.dbQuery(aql`
    FOR v, e IN 1..1 ANY  ${funcId} skbConnectedTo
    REMOVE e IN skbConnectedTo 
    LET removed = OLD 
    RETURN removed._key`);
    var removedConnIds = await cursor.all();
    //remove all throughs from this function
    var cursor = await arangoService.dbQuery(aql`
    FOR v, e IN 1..1 ANY  ${funcId} skbThrough
    REMOVE e IN skbThrough 
    LET removed = OLD 
    RETURN removed._key`);
    var removedThroughIds = await cursor.all();

    var res = await arangoService.removeEntity(funcId);

    return { res, removedConnIds, removedThroughIds }
}

async function addThrough(functionId, toId) {
    var func = await arangoService.getEntity(functionId);
    if (!func) {
        new Error(`functionId ${functionId} not valid`);
    }
    var to = await arangoService.getEntity(toId);
    if (!to) {
        new Error(`fromId ${toId} not valid`);
    }
    var res = await AT.collections.Through.save({}, func._id, to);
    return res;
}

async function deleteThrough(throughId) {
    var item = await arangoService.getEntity(throughId);
    if (!item) {
        new Error(`throughId ${throughId} not valid`);
    }

    var res = await AT.collections.Through.remove(item);
    return res;
}

async function addParamValue(hasParamId, request) {
    var cursor = await AT.collections.HasParam.lookupByKeys([hasParamId]);
    var cursor2 = await AT.collections.ExtUnits.lookupByKeys(['any']);
    if (cursor[0]) {
        var hasParamId = cursor[0]._id;
        var extUnitId = cursor2[0]._id;
        var res = await AT.collections.ParamValues.save({}, hasParamId, extUnitId);
        return res;
    }
    else {
        throw new Error(`HasParam ${hasParamId} not found`)
    }
}

/**
 * Conistst of
 * @param {String} parentKey 
 * @param {child} childKey 
 * @param {*} key 
 */
async function addComponentTypeRelation(parentKey, childKey, key = null) {
    var parent = await getComponentType(parentKey);
    var component = await getComponentType(childKey);
    if (!parent || !component)
        throw "parent component or component not found";
    await AT.collections.ConsistsOf.save(
        key ? { _key: key } : {},
        `${parent._id}`,
        `${component._id}`
    );
}

async function getInfo(modelId, infoReq) {
    var data = {
        components: [],
        params: [],
        resourcesSchema: [],
    };
    if (infoReq.components.toplevel) {
        var cursor = await arangoService.dbQuery(`FOR v,e,path IN 1..5 OUTBOUND 
        '${AT.collections.Models.name}/${modelId}' OUTBOUND 
         skbConsistsOf
         LET isRealisation = (FOR vv, ee IN 1 OUTBOUND v skbRealisationOf RETURN {val: vv, link: ee})
         LET consistsOf = (FOR vv, ee IN 1 OUTBOUND v skbConsistsOf RETURN {val: vv, link: ee})
         let depth = LENGTH((FOR v3 IN OUTBOUND
      SHORTEST_PATH '${AT.collections.Models.name}/${modelId}' TO v skbConsistsOf
        RETURN v3))
         RETURN MERGE(v, {is: isRealisation, consistsOf: consistsOf, depth: depth}) `);
        var data2 = await cursor.all();
        console.log(data2);
        data.components = data2;
    }
    if (infoReq.components.params.values) {
        var cursor = await arangoService.dbQuery(`FOR v,e,path IN 2..6 OUTBOUND 
        '${AT.collections.Models.name}/${modelId}'  
         skbConsistsOf, skbHasParam 
         FILTER IS_SAME_COLLECTION('skbHasParam', e)
         LET vals = (FOR vv, ee IN 1..3 OUTBOUND e skbParamValues
            return ee)
            RETURN MERGE(e, {comp: DOCUMENT(e._from), param: DOCUMENT(e._to),
            paramValues:  vals[*], unit: DOCUMENT(vals[*]._to) })`);
        var data2 = await cursor.all();
        console.log(data2);
        data.params = data2;
    }

    if (infoReq.components.resourcesSchema) {
        var cursor = await arangoService.dbQuery(`FOR comp,e,path IN 1..5 OUTBOUND 
        '${AT.collections.Models.name}/${modelId}'  
        skbConsistsOf
         LET vals = (FOR vv, ee IN 1..3 OUTBOUND comp skbConsume, skbProvide
                        LET vals2 = (FOR v3, e3 IN 1..3 OUTBOUND ee skbThrough
                            return {param: v3, th: e3 })
                        LET connections = (FOR v3, e3 IN 1..1 ANY ee skbConnectedTo
                            return e3)
                RETURN { action: ee, resource: DOCUMENT(ee._to), throughs: vals2, 
                    connections: connections} )  
        RETURN {comp: comp,
        consPros:  vals[*] }`);
        var data2 = await cursor.all();
        console.log(data2);
        data.resourcesSchema = data2;
    }

    return data;
}

async function createConnection(fromId, toId, additionalProps = null) {
    var obj = {
        _from: fromId,
        _to: toId,
    };
    if (typeof additionalProps === 'object') {
        for (var pKey in additionalProps) {
            obj[pKey] = additionalProps[pKey];
        }
    }

    var res = await AT.collections.ConnectedTo.save(obj);
    return res;
}

async function searchPossibleRisksByRules(modelId, rules) {
    var rules = [
        {
            _id: "rule4",
            "@type": "rule",
            "@context": "skRules",
            "rule": {
                textDesc: "rule for risk trigeer of corrosion of  physical connection of material1 and material2",
                ruleType: "riskTrigger",
                predefinedVars: [
                    { var: "matVal1", data: ["materialA1"] },
                    { var: "matVal2", data: ["materialA2"] }
                ],
                //pseudo rule language check that ifconnection have throughs with params it value should be uequal, for example usb == usb
                ruleLang: //`var conn = ConnectedTo(var f1 = Provide($c1, $res), var f2 = Consume($c2, $res)) ^
                    `var conn = ConnectedTo(Provide($c1, skbResources/physicalForce), Consume($c2, skbResources/physicalForce)) ^
                var f1 = xFroms($conn) ^
                var f2 = xTos($conn) ^
                var th1 = Through($f1,  HasParam($c1, skbGeneralParams/materialMadeOf) ) ^
                var th2 = Through($f2,  HasParam($c2, skbGeneralParams/materialMadeOf) ) ^
                var hp1 = xTos($th1) ^
                var hp2 = xTos($th2) ->
                 xEqualToAll(ParamValues($hp1, $eUnit).xParamValueValue, $matVal1) ^
                 xEqualToAll(ParamValues($hp2, $eUnit).xParamValueValue, $matVal2) ^`
                //  ParamValue(hp1, $eUnit).value == ParamValue(hp2, $eUnit).value`
                //var hp1 = Through($f1, HasParam($c1, skbGeneralParams/interfaceType) )._to
                //swrlb:equal(?age, 17)
            },
            "@entityList": [
            ]
        }
    ]

    return await executeRules(modelId, rules);
}

async function searchRequestByRules(modelId, rules) {
    var rules = [
        {
            _id: "rule0",
            "@type": "rule",
            "@context": "skRules",
            "rule": {
                textDesc: "It's not rule but return as c2 all rechargeableBattery that outcomeVoltage better (narrower) and lowerOrEqualMass",
                ruleType: "request", // check request
                predefinedVars: [{
                    var: "c1", data: ["skbComponents/electricBatteryB1"]
                }
                ],
                varsToResult: ["c2"],
                //pseudo rule language check that ifconnection have throughs with params it value should be uequal, for example usb == usb
                ruleLang: //`var conn = ConnectedTo(var f1 = Provide($c1, $res), var f2 = Consume($c2, $res)) ^
                    // `RealisationOf(c2,  componentTypes/electricBattery) ^
                    // ParamValue(HasParam(c2, generalParam/incomeVoltage), extUnit/minAmper).value <= ParamValue(HasParam(c1, generalParam/incomeVoltage), extUnit/minAmper).value`
                    `RealisationOf($c2,  skbComponentTypes/rechargeableBattery) ->
                    xLowerOrEqualThanAll(ParamValue(HasParam($c2, skbGeneralParams/outcomeVoltage), skbExtUnits/maxVolt).xParamValueValue, ParamValue(HasParam($c1, skbGeneralParams/outcomeVoltage), skbExtUnits/maxVolt).xParamValueValue) ^
                    xGreaterOrEqualThanAll(ParamValue(HasParam($c2, skbGeneralParams/outcomeVoltage), skbExtUnits/minVolt).xParamValueValue, ParamValue(HasParam($c1, skbGeneralParams/outcomeVoltage), skbExtUnits/minVolt).xParamValueValue) ^
                    xLowerOrEqualThanAll(ParamValue(HasParam($c2, skbGeneralParams/mass), skbExtUnits/kg).xParamValueValue, ParamValue(HasParam($c1, skbGeneralParams/mass), skbExtUnits/kg).xParamValueValue)`
                //  ParamValue(hp1, $eUnit).xParamValueValue == ParamValue(hp2, $eUnit).xParamValueValue`
                //var hp1 = Through($f1, HasParam($c1, skbGeneralParams/interfaceType) )._to
                //swrlb:equal(?age, 17)
            },
            "@entityList": [
                { "@type": "Consume", _id: `a`, "@context": "pcs" },
                { "@type": "Provide", _id: `b`, "@context": "pcs" },

            ]
        }
    ]

    return await executeRules(modelId, rules);
}

async function executeCheckRules(modelId, rules) {
    //RULE 1 connection checks
    //check parameters of through params of consume and provide that connections
    //for example interfaceType should ===
    //outputVoltage of provide should be inside range of inputVoltage
    var rules = [
        {
            _id: "rule1",
            "@type": "rule",
            "@context": "skRules",
            "rule": {
                textDesc: "interfaceType should paired with interfaceType of connection",
                ruleType: "check",
                //pseudo rule language check that ifconnection have throughs with params it value should be uequal, for example usb == usb
                ruleLang: //`var conn = ConnectedTo(var f1 = Provide($c1, $res), var f2 = Consume($c2, $res)) ^
                    `var conn = ConnectedTo(Provide($c1, $res), Consume($c2, $res)) ^
                var f1 = xFroms($conn) ^
                var f2 = xTos($conn) ^
                var th1 = Through($f1,  HasParam($c1, skbGeneralParams/interfaceType) ) ^
                var th2 = Through($f2,  HasParam($c2, skbGeneralParams/interfaceType) ) ^
                var hp1 = xTos($th1) ^
                var hp2 = xTos($th2) ->
                 xEqual1to1Pair(ParamValues($hp1, $eUnit).xParamValueValue, ParamValues($hp2, $eUnit).xParamValueValue)`
                //  ParamValue(hp1, $eUnit).value == ParamValue(hp2, $eUnit).value`
                //var hp1 = Through($f1, HasParam($c1, skbGeneralParams/interfaceType) )._to
                //swrlb:equal(?age, 17)
            },
            "@entityList": [
                { "@type": "Consume", _id: `a`, "@context": "pcs" },
                { "@type": "Provide", _id: `b`, "@context": "pcs" },

            ]
        }
        ,
        {
            _id: "rule2",
            "@type": "rule",
            "@context": "skRules",
            "rule": {
                textDesc: "Alowable income voltage should be bounded by (narrower) outcome voltage of connection",
                //pseudo rule language
                ruleLang: `var conn = ConnectedTo(Provide($c1, $res), Consume($c2, $res)) ^
                var f1 = xFroms($conn) ^
                var f2 = xTos($conn) ^
                var th1 = Through($f1,  HasParam($c1, skbGeneralParams/outcomeVoltage) ) ^
                var th2 = Through($f2,  HasParam($c2, skbGeneralParams/incomeVoltage) ) ^
                var outputVol = xTos($th1) ^
                var inputVol = xTos($th2) ->
                xGreaterOrEqual1to1Pair(ParamValue($outputVol, skbExtUnits/minVolt).xParamValueValue, ParamValue($inputVol, skbExtUnits/minVolt).xParamValueValue) ^
                xLowerOrEqual1to1Pair(ParamValue($outputVol, skbExtUnits/maxVolt).xParamValueValue, ParamValue($inputVol, skbExtUnits/maxVolt).xParamValueValue)`
                //swrlb:greaterThan(?age, 17)
            },
            "@entityList": [

            ]
        }
        // ,
        // {
        //     _id: "rule3",
        //     "@type": "rule",
        //     "@context": "skRules",
        //     "rule": {
        //         textDesc: "Li-ion battery restricted max 150 watt-hours of electricity in 1 kilogram",
        //         //pseudo rule language
        //         ruleLang: `RealisationOf($c1,  skbComponentTypes/rechargeableBattery) ^
        //         var cap = ParamValue(HasParam($c1, skbGeneralParams/electricCapacity), skbExtUnits/milliAmperHour).xParamValueValue ^
        //         var mass = ParamValue(HasParam($c1, skbGeneralParams/mass), skbExtUnits/kg).xParamValueValue).xParamValueValue ->
        //         xLowerOrEqual1to1Pair(xDiv(cap, mass), 150)`
        //         //swrlb:greaterThan(?age, 17)
        //     },
        //     "@entityList": [

        //     ]
        // }
    ]

    return await executeRules(modelId, rules);
}
async function executeRules(modelId, rules) {
    var result = [];

    var allDataEntitues = await getAllDataEntities();


    for (var ruleblock of rules) {
        var lines = ruleblock.rule.ruleLang.split(/\r?\n/);
        var combinedNewVars = {
            newVars: ruleblock.rule.predefinedVars || [],
            newVarsRestrictedIdTuples: []
        };
        var nextLineIsResult = false;
        var lineResults = [];
        var allLinesValid = true;
        for (var l of lines) {
            var currentLineResult = [];
            var r = parseEntity(allDataEntitues, combinedNewVars, l)
            combinedNewVars.newVars = combinedNewVars.newVars.concat(r.newVars);
            for (var varName in r.newVarsRestrictedIdTuples) {
                if (combinedNewVars.newVarsRestrictedIdTuples[varName]) {
                    console.error("varName already defined, need to intersect", varName)
                }
                else {
                    combinedNewVars.newVarsRestrictedIdTuples[varName] = r.newVarsRestrictedIdTuples[varName]
                }
            }
            // combinedNewVars.newVarsRestrictedIdTuples = combinedNewVars.newVarsRestrictedIdTuples.concat(r.newVarsRestrictedIdTuples)
            if (r.nextLineIsResult) {
                nextLineIsResult = true;
            }
            else if (nextLineIsResult) {
                if (ruleblock.rule.ruleType == "request") {
                    for (var rV of r.result) {
                        if (rV.valid) {
                            if (ruleblock.rule.varsToResult) {
                                var rvVar = {}
                                for (var varName in rV.resultCombination) {
                                    if (ruleblock.rule.varsToResult.includes(varName)) {
                                        rvVar[varName] = rV.resultCombination[varName];
                                    }
                                }
                                currentLineResult.push(rvVar);
                            }
                            else {
                                //currentLineResult.push(rV.resultCombination);
                                currentLineResult.push(rV);
                            }
                        }
                    }
                    lineResults.push(currentLineResult);
                }
                else if (ruleblock.rule.ruleType == "riskTrigger") {
                    // for (var rV of r.result) {
                    //if (rV.valid) {
                    //var varCombinationThatTriggerthisEvent = rV.resultCombination
                    //}
                    lineResults.push(r);
                    //}
                }
                else {
                    for (var rV of r.result) {
                        lineResults.push(rV);
                        allLinesValid &= rV.valid;
                        if (!rV.valid) {
                            console.error("not valid", rV)
                        }
                    }
                }
            }
            console.log("parseEntity", l, r);
        }

        if (ruleblock.rule.ruleType == "request"
            && lineResults.length > 0) {
            //intersect all lineresults 
            var intersectedLineResult = lineResults[0];
            for (var lineResult of lineResults) {
                //search result in intersectedLineResult
                intersectedLineResult = intersectedLineResult.filter(intersectedCombination => {
                    //if intersectedResult not found in lineResult, remove intersectedResult from intersectedLineResult
                    var foundSameResult = lineResult.some(combination => {
                        var notFoundVars = Object.keys(intersectedCombination).filter(vName => {
                            return !combination[vName] || combination[vName] != intersectedCombination[vName];
                        });
                        return notFoundVars.length == 0;
                    });
                    return foundSameResult;
                })
            }

            result.push({ valid: allLinesValid, results: intersectedLineResult, rule: ruleblock.rule, resultsPerLine: lineResults })
        }
        else if (ruleblock.rule.ruleType == "riskTrigger") {
            var funcReturnValidPairs = function (res, lineIndex) {
                //for (var r of lineResults) {
                //find all intersections of valie lines where resultCombination NOT CONTRADICT
                //ar intersectedLineResult = lineResults[0];
                var resCombs = [];
                for (var i in res) {
                    for (var j = 0; j < lineResults[lineIndex].result.length; j++) {
                        var combinationsContrdict = false;
                        var resultCombination = {};
                        var firstCombination = res[i].combination;
                        Object.assign(resultCombination, firstCombination);
                        var potentiaSecCombination2 = lineResults[lineIndex].result[j].resultCombination;
                        for (var vName in potentiaSecCombination2) {
                            if (firstCombination[vName] &&
                                firstCombination[vName] != potentiaSecCombination2[vName]) {
                                combinationsContrdict = true;
                                break;


                                //resultCombination[vName] = firstCombination[vName];

                            }
                            else {
                                resultCombination[vName] = potentiaSecCombination2[vName];
                            }
                        }
                        if (!combinationsContrdict) {
                            var newrs = [...res[i].rs, lineResults[lineIndex].result[j]]
                            // res[i].rs.push(lineResults[lineIndex].result[j]);
                            var valid = res[i].valid && lineResults[lineIndex].result[j].valid;
                            resCombs.push({ rs: newrs, combination: resultCombination, valid: valid })
                        }
                    }
                }
                //}
                lineIndex++;
                if (lineIndex < lineResults.length) {
                    funcReturnValidPairs(resCombs, lineIndex);
                }
                return resCombs;
            }
            var resComb = lineResults[0].result.map(r => { return { rs: [r], combination: r.resultCombination, valid: r.valid } });
            resComb = funcReturnValidPairs(resComb, 1);
            var triggered = resComb.filter(r => r.valid)

            result.push({ valid: allLinesValid, results: triggered, rule: ruleblock.rule, resultsPerLine: lineResults })


        }
        else {
            result.push({ valid: allLinesValid, results: lineResults, rule: ruleblock.rule, resultsPerLine: lineResults })
        }
    }

    ///find better components of same componenttype with same parameters but with less mass
    //eg for radio transmitter if have 3 params (frequency range, power consumption, and mass)
    //we need to find all with freqrang >= fre1rango0 && powerCons <= powerCons0  and mass < mass0
    //it s possible to store params comparision for each component type
    //because, fro one type Voltage less is better, but for another type the same generalParam better is bigger.
    var tt = [
        { componentType: "physicalComponent", generalParam: "mass", extUnit: "kg", action: "LowerIsBetter" },
        //чем шире диапазон допустимого входного напряжения тем лучше
        { componentType: "electricBattery", generalParam: "inputVoltage", extUnit: "minAmper", action: "LowerIsBetter" },
        { componentType: "electricBattery", generalParam: "inputVoltage", extUnit: "maxAmper", action: "BiggerIsBetter" },
        //Чем уже диапазон возможного выходного напряжения тем лучше?
        { componentType: "electricBattery", generalParam: "outcomeVoltage", extUnit: "maxAmper", action: "BiggerIsBetter" },
        { componentType: "electricBattery", generalParam: "outcomeVoltage", extUnit: "maxAmper", action: "LowerIsBetter" },

    ]
    //search component c1 which componentType is tt[i].componentType 
    //find another components c2 with same type where
    //RealisationOf(c2,  componentTypes/electricBattery) ^
    //ParamValue(HasParam(c2, generalParam/inputVoltage), extUnit/minAmper).value <=
    //ParamValue(HasParam(c1, generalParam/inputVoltage), extUnit/minAmper).value

    var test1231245 = `var p = RealisationOf(comp, $basis)
    ComponentType($basis)
    var compTypes = xTos(p) ^
    //only parents with type componentType if it was realisation of another component ignore, search only ComponentType
    //mb to write function to search component type of item in different ways
    //var p2 = ComponentType(compTypes)
    //var compTypes2 = xTos(p2);

                var f1 = xFroms($conn) ^
                var f2 = xTos($conn) `

    return result;
}

async function getAllDataEntities() {
    var result = [];
    var cIndex = arangoService.getWMidToCollection();
    // var extsC = await arangoService.dbQuery(aql`FOR doc IN ${cIndex}
    // RETURN DISTINCT doc.value`)
    var allWMcollections = []// await extsC.all();
    allWMcollections.push("skbComponents");
    allWMcollections.push("skbComponentTypes");
    allWMcollections.push("skbConsume");
    allWMcollections.push("skbProvide");
    allWMcollections.push("skbResources");
    allWMcollections.push("skbThrough");
    allWMcollections.push("skbConnectedTo");
    allWMcollections.push("skbSubclassOf");
    allWMcollections.push("skbSubclassOf");
    allWMcollections.push("skbConsistsof");
    allWMcollections.push("skbRealisationOf");
    allWMcollections.push("skbHasParam");
    allWMcollections.push("skbGeneralParams");
    allWMcollections.push("skbParamValues");
    allWMcollections.push("skbExtUnits");

    for (var colname of allWMcollections) {
        try {
            var collection = arangoService.collection(colname);
            var dataR = await collection.all({ limit: 1000 });
            var data = await dataR.all();
            result = result.concat(data);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    return result;
}

function parseEntity(allDataEntitues, combinedNewVars, line) {
    var res = {
        result: [],
        resultIds: [],
        pairsToResult: [],
        newVars: [],
        //id valid for var con1 if and only if ids in tuples { "con1": { //key names from newVars
        // "skbConnectedTo/123543": { "c1": ["pskbProvide/111"], "c2": [ "skbConsume/222" ] }
        // } }
        newVarsRestrictedIdTuples: combinedNewVars.newVarsRestrictedIdTuples, //{}, //var a = FuncA(c1, c2)
        //{ //get it's from result
        //"skbConnectedTo/123543": { "c1": ["pskbProvide/111"], "c2": [ "skbConsume/222" ] }
        //} 
        resultRestrictedPairs: {}, //FuncA(c1, c2)
    }
    if (line.startsWith("$")) {
        //TODO check variables comparision in total expression
        var existedVarData = combinedNewVars.newVars.find(v => v && (v.var == line || ("$" + v.var) == line));

        if (existedVarData) {
            res.result = existedVarData.data;
            if (existedVarData.pairsToResult) {
                res.pairsToResult = existedVarData.pairsToResult
            }
        }
        else {
            res.result = allDataEntitues.map(v => v._id);
        }

        var existedRestrictionVariants = combinedNewVars.newVarsRestrictedIdTuples.find((v, i) => i && (i == line || ("$" + i) == line))
        // if (existedRestrictionVariants) {
        //     res.resultRestrictedPairs[line.replace("$", "")] = 
        // }

        res.isVarOnly = true;
        return res;
    }
    //if it's concrete entity Id
    var reIIsId = new RegExp(/^\w+\/\w+$/, 'i')
    var isId = reIIsId.exec(line)
    if (isId) {
        res.result = allDataEntitues.filter(e => e._id == line).map(v => v._id)
        if (res.result.length != 1) {
            console.error("not found e with id ", line)
        }
        return res;
    }

    //eg var conn = Connection(Provide($c1, $res), Consume($c2, $res)) ^
    //eg  Through(conn, outputVol = HasParam($c1, outputVoltage) ) ^
    var bracketCount = 0;
    var currentLine = "";
    var currentFunc = null;
    var currentVarName = null;
    var newVars = [];
    for (var cIndex = 0; cIndex < line.length; cIndex++) {
        //console.log("cIndex", cIndex, line)
        var c = line[cIndex];
        currentLine += c;
        if (c == "(") {
            if (bracketCount == 0) {
                currentFunc = currentLine.slice(0, -1).trim();
                currentLine = "";
            }
            bracketCount++;
        }
        if (c == ")") {
            bracketCount--;
            if (bracketCount == 0) {
                //is end of inner exe;
                //var res2 = new RegExp(/,(?=[^\)]*(?:\(|$))/, 'i')
                //var str2 = "Provide($c1, $res), Consume($c2, $res)"

                if (currentFunc.startsWith("x")) {
                    var funcName = currentFunc.substr(1);

                    if (funcName == "Froms" || funcName == "Tos") {
                        var innerVar = currentLine.slice(0, -1).trim();
                        var r0 = parseEntity(allDataEntitues, combinedNewVars, innerVar);
                        if (r0.pairsToResult) {
                            var r = r0.pairsToResult.map(p => funcName == "Froms" ? p._from : p._to);

                            //var resultRestrictedPairs = [];
                            for (var lId in r0.result) {
                                var restrictedPairsVariants = funcAddrestPairs(r0, r0.result[lId], innerVar);

                                for (var restrictedPairs of restrictedPairsVariants) {
                                    // var restrictedPairs = []
                                    // for (var restVarName in restrictedPairsA) {
                                    //     restrictedPairs[restVarName] = restrictedPairs[restVarName] || []
                                    //     // restrictedPairs[restVarName] = restrictedPairs[restVarName].concat(restrictedPairsA[restVarName]);
                                    //     restrictedPairs[restVarName].push(restrictedPairsA[restVarName])
                                    // }

                                    res.resultRestrictedPairs[r[lId]] = res.resultRestrictedPairs[r[lId]] || [];
                                    //add variant of vars combination restrictedPairs that lead to value r[lId]
                                    res.resultRestrictedPairs[r[lId]].push(restrictedPairs);
                                }
                            }
                            res.newVarsRestrictedIdTuples[currentVarName] = res.resultRestrictedPairs;

                            newVars.push({
                                var: currentVarName, data: r,
                                pairsToResult: r0.result.map(id => { return { "_id": id } }), currentFunc: currentFunc, line: currentLine.slice(0, -1)
                            })
                        }
                        else {
                            console.error("r0.pairsToResult is null", r0, innerVar)
                        }
                    }
                    // else if (funcName == "Div"
                    //     || funcName == "Multiply"){

                    // }
                    else if (funcName == "Equal1to1Pair"
                        || funcName == "GreaterOrEqual1to1Pair"
                        || funcName == "LowerOrEqual1to1Pair"
                        || funcName == "EqualToAll"
                        || funcName == "GreaterOrEqualThanAll"
                        || funcName == "LowerOrEqualThanAll") {
                        var splitOnTwoEx = new RegExp(/,(?=[^\)]*(?:\(|$))/, 'i')
                        var currentLineTwoObjects = currentLine.slice(0, -1).trim().split(splitOnTwoEx)
                        var r0 = parseEntity(allDataEntitues, combinedNewVars, currentLineTwoObjects[0].trim());
                        var r1 = parseEntity(allDataEntitues, combinedNewVars, currentLineTwoObjects[1].trim());

                        //  var restrictedPairs = funcAddrestPairs(r0, r0.resultIds[i], currentLineTwoObjects[0].trim());
                        //  var restrictedPairs = funcAddrestPairs(r1, r1.resultIds[i], currentLineTwoObjects[1].trim(), restrictedPairs);

                        // var allVarsIsValid = true;
                        // for (var varName in restrictedPairs) {
                        //     var thisVarNameRestrValid = true;
                        //     if (restrictedPairs[varName].length > 0) {
                        //         var conditionVarValue = restrictedPairs[varName][0]
                        //         //if any value in list differ it's bad
                        //         //all varVals should be equal for each variable, it's valid vars tuple for current e._id
                        //         for (var varVal of restrictedPairs[varName]) {
                        //             if (conditionVarValue != varVal) {
                        //                 thisVarNameRestrValid = false;
                        //                 console.log("var not valid", varName, restrictedPairs, varVal, conditionVarValue)
                        //             }
                        //         }
                        //     }
                        //     allVarsIsValid &= thisVarNameRestrValid
                        // }

                        //try to pair all entities
                        //pairedR0Is[pairedI] = pairedTo
                        var lastPairedR0Is = {};
                        var lastPairedR1Is = {};
                        var pairsR0R1 = [];
                        var pairedRestrcitedPairsByR0 = [];

                        var flatR1resultRestrictedPairs = [];

                        var flatR0resultRestrictedPairs = getFlattenResultRestrictedpair(r0)
                        var flatR1resultRestrictedPairs = getFlattenResultRestrictedpair(r1)
                        for (var i in r0.result) {
                            var r0v = r0.result[i];
                            var foundValidR1Value = false;
                            // var validr1Index = null;
                            //find all values in second param array that equal to this
                            for (var j in r1.result) {
                                var r1v = r1.result[j];
                                var valid = (funcName == "Equal1to1Pair" || funcName == "EqualToAll") ? r0v == r1v :
                                    (funcName == "GreaterOrEqual1to1Pair" || funcName == "GreaterOrEqualThanAll") ? r0v >= r1v :
                                        (funcName == "LowerOrEqual1to1Pair" || funcName == "LowerOrEqualThanAll") ? r0v <= r1v : null;
                                //if valid and j no already paired before with another i
                                if (valid &&
                                    //if is 1to1pair chck that both elements not paired yet;
                                    (!funcName.endsWith("1to1Pair") || lastPairedR0Is[i] == undefined && lastPairedR1Is[j] == undefined)) { // because if pairedR1Is[j]=0  is ok but 0 return false

                                    //check that vars combinations is valid 
                                    var restrictedPairsVariantsA = funcAddrestPairs(r0, r0.resultIds[i], currentLineTwoObjects[0].trim());
                                    var restrictedPairsVariantsB = funcAddrestPairs(r1, r1.resultIds[j], currentLineTwoObjects[1].trim());

                                    for (var restrictedPairsA of restrictedPairsVariantsA) {
                                        for (var restrictedPairsB of restrictedPairsVariantsB) {
                                            var restrictedPairs = []
                                            for (var restVarName in restrictedPairsA) {
                                                restrictedPairs[restVarName] = restrictedPairs[restVarName] || []
                                                // restrictedPairs[restVarName] = restrictedPairs[restVarName].concat(restrictedPairsA[restVarName]);
                                                restrictedPairs[restVarName].push(restrictedPairsA[restVarName])
                                            }

                                            var allVarsIsValid = true;
                                            for (var varName in restrictedPairs) {
                                                var thisVarNameRestrValid = true;
                                                if (restrictedPairs[varName].length > 0) {
                                                    var conditionVarValue = restrictedPairs[varName][0]
                                                    //if any value in list differ it's bad
                                                    //all varVals should be equal for each variable, it's valid vars tuple for current e._id
                                                    for (var varVal of restrictedPairs[varName]) {
                                                        if (conditionVarValue != varVal) {
                                                            thisVarNameRestrValid = false;
                                                            console.log("var not valid", varName, restrictedPairs, varVal, conditionVarValue)
                                                        }
                                                    }
                                                }
                                                allVarsIsValid &= thisVarNameRestrValid
                                            }

                                            if (allVarsIsValid) {
                                                lastPairedR0Is[i] = j;
                                                lastPairedR1Is[j] = i;
                                                pairsR0R1.push({ r0Index: i, r1Index: j })
                                                foundValidR1Value = true;

                                                for (var rIndex in restrictedPairs) {
                                                    restrictedPairs[rIndex] = restrictedPairs[rIndex].filter(function (elem, pos) {
                                                        return restrictedPairs[rIndex].indexOf(elem) == pos;
                                                    })[0]
                                                }
                                                pairedRestrcitedPairsByR0[i] = restrictedPairs;
                                                // validr1Index = j;
                                                //break;
                                            }

                                        }
                                    }
                                }
                            }
                            // if (foundValidR1Value) {

                            // }
                        }

                        //if all Is was paiered and all J s was paired (because r0.result can be shorter that r1.result)
                        // if (r0.result.length == pairedR0Is.length && r1.result.length == pairedR1Is.length) {

                        // }
                        var notPairedR0Is = r0.result.map((v, i) => i).filter(i => lastPairedR0Is[i] == undefined);
                        var notPairedR1Is = r1.result.map((v, i) => i).filter(i => lastPairedR1Is[i] == undefined);
                        //console.log("notPairedR0s", notPairedR0s, notPairedR1s)

                        // if (foundValidR1Value) {
                        for (var i in pairsR0R1) {
                            // var r0i = pairedR0Is[i];
                            // var r1i = pairedR1Is[i];
                            //var r1i = lastPairedR1Is[r0i];
                            var r0i = pairsR0R1[i].r0Index;
                            var r1i = pairsR0R1[i].r1Index;

                            //var problemVarsAll = [];
                            var restrictionPathA = flatR0resultRestrictedPairs[r0i];
                            var restrictionPathB = flatR1resultRestrictedPairs[r1i];
                            var problemVarsAll0 = getProblemVars(r0.pairsToResult[r0i], currentLineTwoObjects[0].trim(),
                                combinedNewVars, restrictionPathA);
                            var problemVarsAll1 = getProblemVars(r1.pairsToResult[r1i], currentLineTwoObjects[1].trim(),
                                combinedNewVars, restrictionPathB);
                            // problemVarsAll0 = problemVarsAll0.filter(r => r.badRefs)
                            // problemVarsAll1 = problemVarsAll1.filter(r => r.badRefs)
                            //build common path
                            var commonPath = [];
                            for (var v1I = problemVarsAll0.length - 1; v1I >= 0; v1I--) {
                                var v1 = problemVarsAll0[v1I];
                                var foundinSecondI = problemVarsAll1.findIndex(v2 => v2.badInputId == v1.badInputId)
                                if (foundinSecondI != -1) {
                                    //found common intersection;
                                    commonPath.push(r0.resultIds[r0i]);
                                    commonPath = commonPath.concat(problemVarsAll0.slice(v1I, problemVarsAll0.length).reverse())
                                    commonPath = commonPath.concat(problemVarsAll1.slice(foundinSecondI, problemVarsAll1.length))
                                    commonPath.push(r1.resultIds[r1i]);
                                    break;
                                }
                                else {
                                    console.log("foundinSecondI not found", v1.badInputId, v1, problemVarsAll1)
                                }
                            }
                            if (commonPath.length == 0) {
                                commonPath.push(r0.resultIds[r0i]);
                                commonPath = commonPath.concat(problemVarsAll0.reverse())
                                commonPath = commonPath.concat(problemVarsAll1)
                                commonPath.push(r1.resultIds[r1i]);

                            }

                            var restrictPairToResult = {}
                            for (var vName in restrictionPathA) { restrictPairToResult[vName] = restrictionPathA[vName] }
                            for (var vName in restrictionPathB) { restrictPairToResult[vName] = restrictionPathB[vName] }

                            res.result.push({
                                valid: true, currentFunc: funcName,
                                commonPath: commonPath,
                                resultCombination: restrictPairToResult,
                                resultCombinationLeft: restrictionPathA,
                                resultCombinationRight: restrictionPathB,
                                problemVars: problemVarsAll0.concat(problemVarsAll1)
                            })
                            res.pairsToResult.push([r0.result[r0i], r1.result[r1i]]);
                            res.pairsIdsToResult = res.pairsIdsToResult || []
                            res.pairsIdsToResult.push([r0.resultIds[r0i], r1.resultIds[r1i]]);
                        }

                        var funcAddNotPairedResults = function (rObj, notPairedIds, indexInPair) {
                            for (var rI of notPairedIds) {
                                var restrictionPath = indexInPair ? flatR1resultRestrictedPairs[rI] : flatR0resultRestrictedPairs[rI]
                                var problemVarsAll0 = getProblemVars(rObj.pairsToResult[rI], currentLineTwoObjects[indexInPair].trim(),
                                    combinedNewVars, restrictionPath);
                                var commonPath = [];
                                commonPath.push(rObj.resultIds[rI]);
                                commonPath = commonPath.concat(problemVarsAll0.reverse())
                                res.result.push({
                                    valid: false, currentFunc: funcName,
                                    //commonPath: commonPath,
                                    commonPath: commonPath,
                                    resultCombination: restrictionPath,
                                    problemVars: problemVarsAll0///.concat(problemVarsAll1)
                                })
                                indexInPair ? res.result.resultCombinationRight = restrictionPath : res.result.resultCombinationLeft = restrictionPath
                                res.pairsToResult.push(indexInPair ?
                                    [null, rObj.result[rI]] : [rObj.result[rI], null]);
                            }
                        }
                        funcAddNotPairedResults(r0, notPairedR0Is, 0);
                        funcAddNotPairedResults(r1, notPairedR1Is, 1);

                    }
                }
                else {
                    //https://stackoverflow.com/questions/732029/how-to-split-string-by-unless-is-within-brackets-using-regex
                    var splitOnTwoEx = new RegExp(/,(?=[^\)]*(?:\(|$))/, 'i')
                    var currentLineTwoObjects = currentLine.slice(0, -1).trim().split(splitOnTwoEx)
                    //var varsForFunc = { newVars: combinedNewVars.newVars.concat(newVars) };
                    combinedNewVars.newVars = combinedNewVars.newVars.concat(newVars)
                    var r0 = parseEntity(allDataEntitues, combinedNewVars, currentLineTwoObjects[0].trim());
                    var r1 = parseEntity(allDataEntitues, combinedNewVars, currentLineTwoObjects[1].trim());
                    // console.log("r0, r1 parsed", r0, r1, currentLineTwoObjects)
                    newVars = newVars.concat(r0.newVars);
                    newVars = newVars.concat(r1.newVars);
                    combinedNewVars.newVars = combinedNewVars.newVars.concat(r0.newVars)
                    combinedNewVars.newVars = combinedNewVars.newVars.concat(r1.newVars)
                    //get result for currentFunc with r0 and r1;
                    var r = {
                        result: [], pairsToResult: [],
                        newVarsRestrictedIdTuples: [],
                        resultRestrictedPairs: []
                    };
                    for (var e of allDataEntitues) {
                        if (e._id.includes(currentFunc)) {
                            var fromValid = r0.result.some(r => r == e._from);
                            var toValid = r1.result.some(r => r == e._to);
                            if (fromValid && toValid) {
                                // var isNotViolateTuples = true;
                                // var restrictedPairs = {};
                                // var funcAddrestPairs = function (robj, objId, locLine) {
                                //     var localRestParis;
                                //     if (robj.isVarOnly) {
                                //         //if is only var check this var in all restricted tuples combined with res
                                //         //restrictedPairs[currentLineTwoObjects[0].trim()] = [objId]
                                //         var varName = locLine.replace("$", "").trim();
                                //         // if (robj.newVarsRestrictedIdTuples[varName]) {
                                //         //     localRestParis = robj.newVarsRestrictedIdTuples[varName];
                                //         // }
                                //         localRestParis = robj.result;// robj.resultIds

                                //         restrictedPairs[varName] = restrictedPairs[varName] || []
                                //         restrictedPairs[varName].push(objId);
                                //     }
                                //     else { //if (robj.resultRestrictedPairs){
                                //         localRestParis = robj.resultRestrictedPairs;

                                //         //eg currentFunct(FuncA(c1, c2), c3)
                                //         var restr = localRestParis && localRestParis[objId];
                                //         if (restr) {
                                //             for (var restVarName in restr) {
                                //                 restrictedPairs[restVarName] = restrictedPairs[restVarName] || []
                                //                 restrictedPairs[restVarName] = restrictedPairs[restVarName].concat(restr[restVarName]);
                                //                 ///restrictedPairs[restVarName] = restr[restVarName];
                                //             }
                                //         }
                                //         else {
                                //             console.log("not found", localRestParis, objId)
                                //         }
                                //     }


                                // }
                                var restrictedPairsVariantsA = funcAddrestPairs(r0, e._from, currentLineTwoObjects[0].trim());
                                var restrictedPairsVariantsB = funcAddrestPairs(r1, e._to, currentLineTwoObjects[1].trim());

                                // var allCombinations

                                for (var restrictedPairsA of restrictedPairsVariantsA) {
                                    for (var restrictedPairsB of restrictedPairsVariantsB) {
                                        var restrictedPairs = []
                                        for (var restVarName in restrictedPairsA) {
                                            restrictedPairs[restVarName] = restrictedPairs[restVarName] || []
                                            // restrictedPairs[restVarName] = restrictedPairs[restVarName].concat(restrictedPairsA[restVarName]);
                                            restrictedPairs[restVarName].push(restrictedPairsA[restVarName])
                                        }

                                        for (var restVarName in restrictedPairsB) {
                                            restrictedPairs[restVarName] = restrictedPairs[restVarName] || []
                                            // restrictedPairs[restVarName] = restrictedPairs[restVarName].concat(restrictedPairsB[restVarName]);
                                            restrictedPairs[restVarName].push(restrictedPairsB[restVarName])
                                        }

                                        var allVarsIsValid = true;
                                        for (var varName in restrictedPairs) {
                                            var thisVarNameRestrValid = true;
                                            if (restrictedPairs[varName].length > 0) {
                                                var conditionVarValue = restrictedPairs[varName][0]
                                                //if any value in list differ it's bad
                                                //all varVals should be equal for each variable, it's valid vars tuple for current e._id
                                                for (var varVal of restrictedPairs[varName]) {
                                                    if (conditionVarValue != varVal) {
                                                        thisVarNameRestrValid = false;
                                                        console.log("var not valid", varName, restrictedPairs, varVal, conditionVarValue)
                                                    }
                                                }
                                            }
                                            allVarsIsValid &= thisVarNameRestrValid
                                        }

                                        if (allVarsIsValid) {
                                            r.result.push(e._id);
                                            r.pairsToResult.push({ _from: e._from, _to: e._to });

                                            //reduce restricted pairs
                                            for (var i in restrictedPairs) {
                                                restrictedPairs[i] = restrictedPairs[i].filter(function (elem, pos) {
                                                    return restrictedPairs[i].indexOf(elem) == pos;
                                                })[0]
                                            }
                                            r.resultRestrictedPairs[e._id] = r.resultRestrictedPairs[e._id] || [];
                                            r.resultRestrictedPairs[e._id].push(restrictedPairs);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    console.log("r parsed", r)

                    //make back propogation of new filtered data.
                    var fromVar = currentLineTwoObjects[0].trim();
                    var updatedFromVals = r.pairsToResult.map(p => p._from);
                    var fromVarLine = combinedNewVars.newVars.find(v => v.var == fromVar || ("$" + v.var) == fromVar);
                    console.log("SDD", updatedFromVals, fromVarLine);
                    combinedNewVars = progateDataBack(allDataEntitues, combinedNewVars, fromVar, updatedFromVals);
                    //setFroms(fromVar, updatedFromVals)

                    //check is it access to json structure after this ex
                    if (cIndex < line.length && line[cIndex + 1] == ".") {
                        const jsonPathReg = new RegExp(/[\w.]+/, 'i')
                        var jsonpathStr = jsonPathReg.exec(line.substring(cIndex + 2))[0]
                        var jsonpath = jsonpathStr.split(".");
                        // var jsonObj = r.result;
                        // for (var subFieldName of jsonpath) {
                        //     jsonObj = jsonObj[subFieldName];
                        // }
                        // r.result = jsonObj;
                        cIndex += jsonpathStr.length;
                        for (var i = 0; i < r.result.length; i++) {
                            var jsonObjId = r.result[i];
                            var jsonObj = allDataEntitues.find(a => a._id == jsonObjId);
                            for (var subFieldName of jsonpath) {
                                if (subFieldName.startsWith("x")) {
                                    jsonObj = getByCustomJsonField(jsonObj, subFieldName);
                                }
                                else {
                                    jsonObj = jsonObj[subFieldName];
                                }
                            }
                            r.resultIds = r.resultIds || [];
                            r.resultIds[i] = r.result[i];
                            r.result[i] = jsonObj;
                        }
                        console.log("new r from json", r, jsonpath)
                    }
                    if (currentVarName) {
                        //newVars[currentVarName] = r.result;
                        //if (r.result) {
                        newVars.push({
                            var: currentVarName, data: r.result,
                            pairsToResult: r.pairsToResult, currentFunc: currentFunc, line: currentLine.slice(0, -1)
                        })
                        r.newVarsRestrictedIdTuples[currentVarName] == r.newVarsRestrictedIdTuples[currentVarName] || {};
                        // r.newVarsRestrictedIdTuples[currentVarName] = r.newVarsRestrictedIdTuples[currentVarName]
                        //     .concat(r.resultRestrictedPairs);
                        r.newVarsRestrictedIdTuples[currentVarName] = r.resultRestrictedPairs
                        // res.result = r.result;
                        // }
                        currentVarName = null;

                    }
                    else {
                        // res.result = r.result;
                        // res.pairsToResult = r.pairsToResult;
                    }
                    res.result = r.result;
                    res.resultIds = r.resultIds;
                    res.functionInputs = [r0, r1]
                    res.pairsToResult = r.pairsToResult;
                    res.newVarsRestrictedIdTuples = r.newVarsRestrictedIdTuples;
                    res.resultRestrictedPairs = r.resultRestrictedPairs;
                    // newVars = newVars.concat(r.newVars);
                }

                currentLine = "";
            }
        }

        // //check is it access to json structure after this ex
        if (bracketCount == 0 && c == ".") {
            const jsonPathReg = new RegExp(/[\w.]+/, 'i')
            var jsonpathStr = jsonPathReg.exec(line.substring(cIndex + 1))[0]
            var jsonpath = jsonpathStr.split(".");
            cIndex += jsonpathStr.length;
            for (var i = 0; i < r.result.length; i++) {
                var jsonObjId = r.result[i];
                var jsonObj = allDataEntitues.find(a => a._id == jsonObjId)[0];
                for (var subFieldName of jsonpath) {
                    if (subFieldName.startsWith("x")) {
                        jsonObj = getByCustomJsonField(jsonObj, subFieldName);
                    }
                    else {
                        jsonObj = jsonObj[subFieldName];
                    }
                }
                r.result[i] = jsonObj;
            }
            // r.result = jsonObj;
            console.log("new r from json", r, jsonpath)
        }
        //check set is variable 
        //var re = new RegExp(/var\s(.+)\s*=/, 'i')
        //console.log(re.exec("var aabb = "))
        var re = new RegExp(/var\s(.+)\s*=/, 'i')
        var isVar = re.exec(currentLine)
        if (isVar && bracketCount == 0) { //to ognore inner variables Through($conn, var hp1 = HasParam($c1, interfaceType) )
            currentVarName = isVar[1].trim();
            currentLine = "";
        }
        if (currentLine.trim() == "^") {

        }
        if (currentLine.trim() == "->") {
            res.nextLineIsResult = true;
        }
    }

    res.newVars = newVars;
    return res;
    //  return {
    //     result: []
    //     newVars: [
    //         conn: [],
    //         hp1: []
    //     ]
    // }
}

function getFlattenResultRestrictedpair(r) {
    var result = [];
    var lastTakedIndex = [];
    if (r.resultIds) {
        for (var resultId0 of r.resultIds.filter((v, k) => k != undefined)) { //TODO hack ) {
            var pairs = r.resultRestrictedPairs[resultId0];
            if (lastTakedIndex[resultId0] == undefined) {
                lastTakedIndex[resultId0] = 0;
            }
            else {
                lastTakedIndex[resultId0] = lastTakedIndex[resultId0] + 1;
            }
            result.push(pairs[lastTakedIndex[resultId0]]);
        }
    }
    return result;
}

function getProblemVars(badInputIds, line, combinedNewVars, restrictCombination) {
    var problemVars = [];
    if (!badInputIds) {
        console.log("getProblemVars return, no badInputId", badInputIds)
        return problemVars;
    }
    var depVars0 = getAllVarsInString(line).filter(vname => combinedNewVars.newVars.some(v => v.var == vname));
    // var depVars1 = getAllVarsInString(r1).filter(vname => combinedNewVars.newVars.some(v => v.var == vname));
    depVars0.map((v0, paramIndex) => {
        var varInfo = combinedNewVars.newVars.find(v => v.var == v0);
        var badInputId = paramIndex == 1 ? badInputIds._to : badInputIds._from || badInputIds._id;
        var indexesInList = varInfo.data.reduce((c, v, i) => v == badInputId ? c.concat(i) : c, []); //varInfo.data.findIndex(dId => dId == badInputId);
        // var newBadInputId = varInfo.data[indexInList];
        var dependLine = varInfo.line;
        //if at least any dependent value exists
        if (indexesInList.length > 0) {
            //TODO if its not "$c1" but more complex like "Provide($c1, $res), Consume($c2, $res)" or "$c1, Consume($c2, $res)"
            //TOOD split to all variables and check
            var restrictedVariableName = dependLine ? dependLine.replace("$", "") : null;
            if (dependLine && restrictCombination && restrictCombination[restrictedVariableName]) {

                var restrictedValue = restrictCombination[restrictedVariableName];
                var restrictedIndex = null;
                for (var indexInList of indexesInList) {
                    var badRefs = varInfo.pairsToResult[indexInList];
                    if (badRefs._id == restrictedValue ||
                        badRefs._from == restrictedValue ||
                        badRefs._to == restrictedValue) {
                        //TODO potential bag if two contains;
                        //show if this case exists
                        if (restrictedIndex) {
                            console.error("potential bag. potentially two valid paths", varInfo.pairsToResult, indexInList)
                        }
                        //else{
                        restrictedIndex = indexInList
                        // }
                    }
                }

                if (restrictedIndex == null) {
                    console.error("potential error. restrictedIndex not found", varInfo.pairsToResult, indexInList, restrictedValue);
                    restrictedIndex = indexesInList[0];
                }
                var badRefs = varInfo.pairsToResult[restrictedIndex];

                var innerProblemVars = getProblemVars(badRefs, dependLine, combinedNewVars, restrictCombination);
                problemVars = problemVars.concat(innerProblemVars)
            }

            else {
                var badRefs = varInfo.pairsToResult[indexesInList[0]];
                //get params from this line and repeat Recoursively;
                //var newBadInputId = 0;//badRefs..
                var innerProblemVars = getProblemVars(badRefs, dependLine, combinedNewVars, restrictCombination);
                problemVars = problemVars.concat(innerProblemVars)
            }
        }
        else {

            var restrictedVariableName = varInfo.var;
            if (restrictCombination && restrictCombination[restrictedVariableName]) {

                var restrictedValue = restrictCombination[restrictedVariableName];
                // var restrictedIndex = null;
                var arrToFindVal = varInfo.data;
                var foundBadId = null;
                for (var val of arrToFindVal) {
                    if (val == restrictedValue) {
                        foundBadId = val;
                    }
                }

                problemVars.push({ badInputId: foundBadId, var: v0, dependLine: null, badRefs: null });
                //var innerProblemVars = getProblemVars(badRefs, dependLine, combinedNewVars, restrictCombination);
                //problemVars = problemVars.concat(innerProblemVars)
            }
            else {
                console.throw("ERRORRRR not ofund", varInfo)
                //var badRefs = varInfo.pairsToResult[indexesInList[0]];
                problemVars.push({ badInputId: varInfo.data[0], var: v0, dependLine: null, badRefs: null });
                //get params from this line and repeat Recoursively;
                //var newBadInputId = 0;//badRefs..
                //var innerProblemVars = getProblemVars(badRefs, dependLine, combinedNewVars, restrictCombination);
                //problemVars = problemVars.concat(innerProblemVars)
            }
        }
        // // var badRefs = varInfo.pairsToResult[indexInList];
        // if (dependLine) {
        //     //get params from this line and repeat Recoursively;
        //     //var newBadInputId = 0;//badRefs..
        //     var innerProblemVars = getProblemVars(badRefs, dependLine, combinedNewVars, restrictCombination);
        //     problemVars = problemVars.concat(innerProblemVars)
        // }
        //}
        // else {
        //     console.error("badInputId was removed in var store", v0, indexInList)
        // }
        // problemVars.push({ badInputId: badInputId, var: v0, dependLine: dependLine, badRefs: badRefs })
        problemVars.push({ badInputId: badInputId, var: v0, dependLine: dependLine, badRefs: badRefs })
    })
    // problemVars.push({ badInputId: badInputId, var: v0, dependLine: line, badRefs: null })
    return problemVars;
}

function funcAddrestPairs(robj, objId, locLine, initialRestrictedPairs = null) {
    var restrictedPairs = [];// initialRestrictedPairs || {};
    var localRestParis;
    if (robj.isVarOnly) {
        //if is only var check this var in all restricted tuples combined with res
        //restrictedPairs[currentLineTwoObjects[0].trim()] = [objId]
        var varName = locLine.replace("$", "").trim();

        localRestParis = robj.newVarsRestrictedIdTuples[varName];
        if (localRestParis) {
            var variantsToThisVar = localRestParis[objId] || []; // [{ varname: val, varName2: val2 }]
            // if (variantsToThisVar.length > 1) {
            for (var variant of variantsToThisVar) {
                //variant that drive to varName { varname: val, varName2: val2 }
                var newVariant = {};
                for (var prevVarName in variant) {
                    newVariant[prevVarName] = variant[prevVarName];
                }
                newVariant[varName] = objId;
                restrictedPairs.push(newVariant);
                // newVariant[varName] = newVariant[varName] || []
            }
            // }
            // else {

            // }
        }
        else {
            restrictedPairs[0] = restrictedPairs[0] || []
            restrictedPairs[0][varName] = objId;
        }

        ///localRestParis = robj.result;// robj.resultIds


    }
    else { //if (robj.resultRestrictedPairs){
        localRestParis = robj.resultRestrictedPairs;

        try {
            //eg currentFunct(FuncA(c1, c2), c3)
            var restrVariants = localRestParis && localRestParis[objId];
            if (restrVariants) {
                for (var variantToGetThisobjId of restrVariants) {
                    // if (variantToGetThisobjId) {
                    var newVariant = {};
                    for (var restVarName in variantToGetThisobjId) {
                        // newVariant[restVarName] = newVariant[restVarName] || []
                        // newVariant[restVarName] = newVariant[restVarName].concat(variantToGetThisobjId[restVarName]);
                        newVariant[restVarName] = variantToGetThisobjId[restVarName];
                    }
                    restrictedPairs.push(newVariant);
                }
            }
            else {
                restrictedPairs.push([])// add empty for variant objId means that it exists without restrictions
                console.log("not found. no restrictions", localRestParis, objId, robj)
            }

        }
        catch (ex) {
            console.log("EXEXE1", robj, localRestParis[objId])
        }
    }

    return restrictedPairs;
}

function progateDataBack(allDataEntitues, combinedNewVars, lineToUpdate, newVals) {
    // var res = {
    //     result: [],
    //     newVars: []
    // }

    //var fromVarLine = combinedNewVars.newVars.find(v => v.var == fromVar || ("$" + v.var) == fromVar);

    if (lineToUpdate.startsWith("$")) {
        //TODO check variables comparision in total expression
        var existedVarIndex = combinedNewVars.newVars.findIndex(v => v && (v.var == lineToUpdate || ("$" + v.var) == lineToUpdate))
        // existedVarData.data = newVals;
        if (existedVarIndex == -1) {
            combinedNewVars.newVars.push({
                var: lineToUpdate.replace("$", ""), data: newVals,
                pairsToResult: null, currentFunc: null, line: null
            })
        }
        else {
            var i = 0;
            while (i < combinedNewVars.newVars[existedVarIndex].data.length) {
                var valExistsInNew = newVals.find(v => v == combinedNewVars.newVars[existedVarIndex].data[i]);
                if (!valExistsInNew) {
                    combinedNewVars.newVars[existedVarIndex].data.splice(i, 1);
                    if (combinedNewVars.newVars[existedVarIndex].pairsToResult) {
                        combinedNewVars.newVars[existedVarIndex].pairsToResult.splice(i, 1);
                    }
                    i--;
                }
                i++;
            }
        }

        // combinedNewVars[existedVarIndex].data = newVals;
        // if (existedVarData) {
        //     res.result = existedVarData.data;
        // }
        // else {
        //     res.result = allDataEntitues;
        // }
        // return res;
    }

    return combinedNewVars;
    // return res;
}

function getAllVarsInString(input) {
    var re3 = new RegExp(/\$[\w]+/, 'gi')
    // var input = " ParamValues($hp2, $eUnit).value"
    var vars = [];
    var match = null;
    while ((match = re3.exec(input)) != null) {
        vars.push(match[0].substr(1))
        // console.log(match);
    }
    return vars;
}

function getByCustomJsonField(jsonObj, customJsonFieldRequestName) {
    if (!customJsonFieldRequestName.startsWith("x")) {
        console.error("getByCustomJsonField field name not starts with x", customJsonFieldRequestName, jsonObj)
    }

    var name = customJsonFieldRequestName.substring(1);

    if (name == "ParamValueValue") {
        if (jsonObj.valueType == "latex" && jsonObj.viewValue) {
            return jsonObj.viewValue;
        }
        return jsonObj.value;
    }

    return null;
}

// function searchByJsonPath(path, obj, target) {
//     //https://stackoverflow.com/questions/8790607/javascript-json-get-path-to-given-subnode
//     for (var k in obj) {
//         if (obj.hasOwnProperty(k))
//             if (obj[k] === target)
//                 return path + "['" + k + "']"
//             else if (typeof obj[k] === "object") {
//                 var result = search(path + "['" + k + "']", obj[k], target);
//                 if (result)
//                     return result;
//             }
//     }
//     return false;
// }

module.exports.createComponent = createComponent;
module.exports.createModel = createModel;
module.exports.getModel = getModel;
module.exports.getComponent = getComponent;
module.exports.attachComponentToModel = attachComponentToModel;
module.exports.attachComponentToParentComp = attachComponentToParentComp;
module.exports.addComponentTypeRelation = addComponentTypeRelation;
module.exports.getSubcomponents = getSubcomponents;
module.exports.getCompFamily = getCompFamily;
module.exports.addParam = addParam;
module.exports.addFunction = addFunction;
module.exports.removeFunction = removeFunction;
module.exports.addThrough = addThrough;
module.exports.deleteThrough = deleteThrough;
module.exports.addParamValue = addParamValue;
module.exports.getInfo = getInfo;
module.exports.createConnection = createConnection;
module.exports.searchPossibleRisksByRules = searchPossibleRisksByRules;
module.exports.searchRequestByRules = searchRequestByRules;
module.exports.executeCheckRules = executeCheckRules;
