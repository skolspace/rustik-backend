var satInfos = require( '../models//satInfo');
var Sat = require('../models//sat');

var skgModelsService  = require('./skg/skgModelsService');

async function satsBySolId(solId) {
    //get basesat tles

    var sats = await Sat.find({ solId: solId });
    return sats;
}

async function generateSats(solId, genSatsParams) {
    //prepare params if string
    genSatsParams.inclinationStep = parseInt(genSatsParams.inclinationStep);
    genSatsParams.ascensionStep = parseInt(genSatsParams.ascensionStep);

    var baseSat = await satInfos.findById(genSatsParams.baseSat._id);

    //remove allprevious 
    await Sat.deleteMany({ solId: solId });
    //
    var currentInc = genSatsParams.inclinationFrom;
    var currentAscension = genSatsParams.ascensionFrom;
    var inclinationsCnt = (genSatsParams.inclinationTo - genSatsParams.inclinationFrom) / genSatsParams.inclinationStep;
    var ascensionCnt = (genSatsParams.ascensionTo - genSatsParams.ascensionFrom) / genSatsParams.ascensionStep;
    var index = 1;
    for (var incI = 0; incI < inclinationsCnt; incI++) {
        currentInc = incI * genSatsParams.inclinationStep;
        for (var lngI = 0; lngI < ascensionCnt; lngI++) {
            currentAscension = lngI * genSatsParams.ascensionStep;
            for (var circleI = 0; circleI < genSatsParams.satsPerOrbit; circleI++) {
                var currentPerigee = circleI * (360 / genSatsParams.satsPerOrbit);

                var tle1 = baseSat.tle1;
                var tle2 = baseSat.tle2;
                // currentInc = 98.4291;
                // currentAscension = 16.0436;
                var inclinationStr = currentInc.toFixed(4).padStart(8);
                var ascentionStr = currentAscension.toFixed(4).padStart(8);
                var perigeeStr = currentPerigee.toFixed(4).padStart(8);
                tle2 = baseSat.tle2.substr(0, 8)
                    + inclinationStr + ' ' + ascentionStr
                    + baseSat.tle2.substr(8 + 8 + 1 + 8, 1 + 7) + ' '
                    + perigeeStr
                    + baseSat.tle2.substr(8 + 8 + 1 + 8 + 1 + 7 + 1 + 8, 70 - 8 - 8 - 1 - 8 - 1 - 7 - 1 - 8);
                // + baseSat.tle2.substr(8+8+1+8, 70-8-8-1-8);
                var name = "sat-Inc" + currentInc + "--" + "Asc" + currentAscension + '--' + circleI;
                var sat = new Sat({
                    satName: name,
                    noradId: baseSat.norad_id, //index++,
                    satInfo: {
                        tle1: tle1,
                        tle2: tle2
                    },
                    solId: solId,
                    ver: "v3"
                });
                console.log(name, sat);
                var insRes = await sat.save();
                console.log("insRes", insRes);
            }
        }
    }

    return { generated: index };
};

async function getInfo(modelId, infoReq) {
    return await skgModelsService.getInfo(modelId, infoReq);
}

var GenSatsParams = function () { };

module.exports.satsBySolId = satsBySolId;
module.exports.GenSatsParams = GenSatsParams;
module.exports.generateSats = generateSats;
module.exports.satsBySolId = satsBySolId;
module.exports.getInfo = getInfo;