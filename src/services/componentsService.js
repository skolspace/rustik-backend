var arangoService = require('./arangoService').service;
var aql = require('./arangoService').aql;
var AT = arangoService.AT;

// import skgModelsService from './skg/skgModelsService'
var skgModelsService = require('./skg/skgModelsService');
/**
 * tatat
 * @param {*} filterOntologyIds 
 */
async function getComponentTypes(filterOntologyIds = null) {
    // var result = await ComponentType.find().limit(100);
    var data2 = [];
   // filterOntologyIds = ["skbOntologies/spacecraft"]
    if (filterOntologyIds && filterOntologyIds.length && filterOntologyIds.length > 0) {
        var comps = [];
        for (var ontologyId of filterOntologyIds) {
            var cursor = await arangoService.dbQuery(aql`
            FOR v,e,path IN 1..5 OUTBOUND ${ontologyId} skbIncludes,
            skbConsistsOf 
            FILTER IS_SAME_COLLECTION(${AT.collections.ComponentTypes.name}, v)
            let parents = (FOR vv,ee IN 1..1 OUTBOUND v skbSubclassOf RETURN vv )
            let descendants = (FOR vv,ee IN 1..5 INBOUND v skbSubclassOf
                        COLLECT kk = vv._id INTO pps
                        RETURN {_key: kk, 
                        label: PARSE_IDENTIFIER(kk).key, parents: pps[*].ee._to} )
            RETURN DISTINCT {_key: v._id, 
                        label: PARSE_IDENTIFIER(v).key, parents: parents[*]._id, descendants: descendants} `);
            var te = await cursor.all();
            comps = [].concat(comps, te);
            te.forEach(c => {
                comps = [].concat(comps, c.descendants);
            })
            comps.forEach(v => v.ontology = ontologyId);
        }
        comps = comps.filter((c, i) => comps.findIndex(cc => cc._key == c._key) === i);
        data2 = comps;
    }
    else {
        var root = await arangoService.AT.collections.ComponentTypes.lookupByKeys(["rootComponentType"])

        var list1 = arangoService.AT.collections.ComponentTypes.list();
        var cursor = await arangoService.dbQuery(aql`FOR v,e,path IN 1..8 INBOUND ${root[0]} skbSubclassOf
        COLLECT kk = v._id INTO pps
        RETURN {_key: kk, 
            label: PARSE_IDENTIFIER(kk).key, parents: pps[*].e._to}`);
        data2 = await cursor.all();

        var el123= "WM333ComponentType-pcs/pcs-rootComponent"
        var col123 = "WM333SubclassOf-pcs"
        var cursor = await arangoService.dbQuery(aql`FOR v,e,path IN 1..8 INBOUND ${el123} ${col123}
        COLLECT kk = v._id INTO pps
        RETURN {_key: kk, 
            label: PARSE_IDENTIFIER(kk).key, parents: pps[*].e._to}`);
        var dataWM = await cursor.all();
        data2 = data2.concat(dataWM);
    }
    console.log(data2);
    var res = _.map(data2, d => {
        return {
            componentType: d._key,
            parentComponentTypes: d.parents,
            label: d.label
        }
    });

    return res;
}

async function getComponentsByType(compTypeId) {
    var tt123 = "WM333RealisationOf-pcs"
    var aa2123 = "WM333SubclassOf-pcs"
    var cc2123 = "WM333Component-pcs"
    var cursor = await arangoService.dbQuery(aql`FOR v,e,path IN 0..5 INBOUND ${compTypeId}
     ${AT.collections.RealisationOf},  INBOUND ${AT.collections.SubclassOf}, 
     ${tt123}, INBOUND ${aa2123}
      FILTER IS_SAME_COLLECTION(${AT.collections.Components}, v) OR IS_SAME_COLLECTION(${cc2123}, v)
       RETURN {_id: v._id, name: PARSE_IDENTIFIER(v).key, obj: v}`);
    var data2 = await cursor.all();
    return data2;
}


async function createComponent(componentTypeId, componentBody, baseComponentId) {
    return await skgModelsService.createComponent(componentTypeId, componentBody, baseComponentId);
}

async function attachComponentToModel(componentKey, modelKey) {
    return await skgModelsService.attachComponentToModel(componentKey, modelKey);
}

async function attachComponentToParentComp(componentKey, parentCompId) {
    return await skgModelsService.attachComponentToParentComp(componentKey, parentCompId);
}

async function getSubcomponents(parentCompId) {
    return await skgModelsService.getSubcomponents(parentCompId);
}
async function getCompFamily(compId, request) {
    return await skgModelsService.getCompFamily(compId, request);
}

async function addParam(compId, request) {
    return await skgModelsService.addParam(compId, request);
}

async function addFunction(compId, functionType, resourceId) {
    return await skgModelsService.addFunction(compId, functionType, resourceId);
}

async function addThrough(functionId, toId) {
    return await skgModelsService.addThrough(functionId, toId);
}

async function addParamValue(hasParamId, request) {
    return await skgModelsService.addParamValue(hasParamId, request);
}

//FOR v,e IN 1 OUTBOUND 'skbComponentTypes/electricalTemperatureSensor' skbConsume, skbProvide, skbHasParam FOR vv, ee IN 1 ANY e skbDefinedBy RETURN {_key: vv, e: e, ee: ee}
async function getComponentTypeDefinition(key) {
    var cursor = await arangoService.dbQuery(aql`FOR v,e IN 1 OUTBOUND ${key} 
    skbDefinedBy
    RETURN { col: PARSE_IDENTIFIER(v).collection, value: v, from: DOCUMENT(v._from), to: DOCUMENT(v._to), v: v, definition: e}`);
    var data2 = await cursor.all()
    var definedBy = _.map(data2, d => {
        d.from.label = d.from._key;
        d.to.label = d.to._key;
        d.definition.label = d.definition._key;
        return d;
    })

    var cursor2 = await arangoService.dbQuery(aql`FOR v,e IN 1 OUTBOUND ${key} 
     skbConsistsOf RETURN v`);
    var data2 = await cursor2.all();
    var consistsOf = _.map(data2, d => {
        d.label = d._key;
        return d;
    })

    var cursor2 = await arangoService.dbQuery(aql`FOR v,e IN 1 OUTBOUND ${key} 
     skbSubclassOf RETURN {val: v}`);
    var data2 = await cursor2.all();
    var subclassOf = data2[0].val;

    return { subclassOf: subclassOf, consistsOf: consistsOf, definedBy: definedBy };
}

async function createConnection(fromId, toId, additionalProps = null) {
    //TOOD check is it allowable

    return await skgModelsService.createConnection(fromId, toId, additionalProps);
}

async function getExtUnits(){
    var extsC = await arangoService.dbQuery(aql`FOR v IN ${arangoService.AT.collections.UnitsExt} RETURN v`)
    var exts = await extsC.all();
    var extUnitsC = await arangoService.dbQuery(aql`FOR v IN skbExtUnits RETURN v`)
    var extUnits = await extUnitsC.all();
    // var exts = await  arangoService.AT.collections.UnitsExt.list();
    var unitsC = await arangoService.dbQuery(aql`FOR v IN skbUnits RETURN v`)
    var units = await unitsC.all();
    return {
        extUnits: extUnits,
        exts: exts,
        units: units
    };
}

async function getResources(){
    var extUnitsC = await arangoService.dbQuery(aql`FOR v IN skbResources RETURN v`)
    var res = await extUnitsC.all();
    return res;
}

async function getGeneralParams(){
    var extUnitsC = await arangoService.dbQuery(aql`FOR v IN skbGeneralParams RETURN v`)
    var res = await extUnitsC.all();
    return res;
}

async function checkRules(modeId, rules){
    var r = {}
    r.executeCheckRules = await skgModelsService.executeCheckRules(modeId, rules);
    r.searchRequestByRules = await skgModelsService.searchRequestByRules(modeId, rules);
    r.searchPossibleRisksByRules = await skgModelsService.searchPossibleRisksByRules(modeId, rules);
    return r;
}


module.exports.getComponentTypes = getComponentTypes;
module.exports.getComponentsByType = getComponentsByType;
module.exports.createComponent = createComponent;
module.exports.attachComponentToModel = attachComponentToModel;
module.exports.attachComponentToParentComp = attachComponentToParentComp;
module.exports.getSubcomponents = getSubcomponents;
module.exports.getCompFamily = getCompFamily;
module.exports.addParam = addParam;
module.exports.addFunction = addFunction;
module.exports.removeFunction = skgModelsService.removeFunction;
module.exports.addThrough = addThrough;
module.exports.deleteThrough = skgModelsService.deleteThrough;
module.exports.addParamValue = addParamValue;
module.exports.getComponentTypeDefinition = getComponentTypeDefinition;
module.exports.createConnection = createConnection;
module.exports.getExtUnits = getExtUnits;
module.exports.getResources = getResources;
module.exports.getGeneralParams = getGeneralParams;
module.exports.checkRules = checkRules;