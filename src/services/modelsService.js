import SatModel from '../models//satModel';
//var SatModel = require('../models//satModel');
var arangoService = require('./arangoService').service;
var skgCalcService = require('./skg/skgCalcService');

async function get(modelId, solId) {
    //todo replace to uuid
    //TOOD temp hack
    var result = await SatModel.findOne({ name: "model1" }, (err, cust) => {
        if (err) return console.error(err)
    });
    result.name = modelId;
    return result;
}

async function getShort(solId) {
    //todo replace to uuid
    var result = await SatModel.find({}, { components: 0 }, (err, cust) => {
        if (err) return console.error(err)
    }).limit(40);
    return result;
}

async function updateComponents(modelId, components) {
    //var result = await  SatModel.update(
    //     { name: modelId },
    //     {$set: {components: components}}
    //     );
}

async function modifyValue(paramValId, props) {
    if (props.valueType == "latex") {
        //check formula for all inputs12
        //is any modifications of inputs
        const existedInputs = await arangoService.AT.collections.Inputs.inEdges(paramValId);
        var existedFroms = existedInputs.map(v => v._from);
        var newInputFroms = [];
        var re = /customEmbed{myVar}\[([A-Za-z0-9\/]*)]/g;
        //var s = 'sqrt{\customEmbed{myVar}[skbParamValues/749588]\customEmbed{myVar}[skbParamValues/749588]\customEmbed{myVar}[skbParamValues/749795]ds\customEmbed{myVar}[skbParamValues/749785]\customEmbed{myVar}[skbParamValues/749795]\customEmbed{myVar}[skbParamValues/749793]9}';
        var s = props.value;
        var m;
        do {
            m = re.exec(s);
            if (m) {
                var match = m[0];
                newInputFroms.push(m[1]);
            }
        } while (m);
        //get only unique
        newInputFroms = newInputFroms.filter((id, i) => newInputFroms.indexOf(id) == i);
        var fromsToAdd = newInputFroms.filter(id => !existedFroms.includes(id));
        var fromsToRemove = existedFroms.filter(id => !newInputFroms.includes(id));
        fromsToAdd.forEach(fromId => {
            var el = {
                _from: fromId,
                _to: paramValId,
            }
            arangoService.AT.collections.Inputs.save(el)
        });
        fromsToRemove.forEach(fromId => {
            var id = existedInputs.find(v => v._from == fromId)._id;
            arangoService.AT.collections.Inputs.remove(id)
        });
    }
    if (props.unit) {
        //TODO find extUnitId by name;
        var newExtUnitId = "skbExtUnits/" + props.unit;
        await arangoService.modifyValueToUnit(paramValId, newExtUnitId, true);
    }
   // else {
        return await arangoService.modifyValue(paramValId, props, true);
    //}
}

async function calculateParamValues(modelId) {
    return await skgCalcService.calculateParamValuesForModel(modelId);
}

module.exports.get = get;
module.exports.getShort = getShort;
module.exports.modifyValue = modifyValue;
module.exports.calculateParamValues = calculateParamValues;
// module.exports.create = create;