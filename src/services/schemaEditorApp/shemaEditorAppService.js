

var appUsedSchemas = ["skbContexts/base"]

var arangoService = require('../arangoService').service;
var AT = arangoService.AT;
var db = arangoService.db;

var appWMid = arangoService.appWMid;//"WM333";
var contexts = ["pcs", "baseLinkedData"] //physical components strcuture

function pcsContextRules() {
    //eg
    //{ "@type": "skbComponentType",  _id: "pcs/physicalComponent", "@context": "pcs" },
    //{ "@type": "skbSubclassOf", _from: "spacecraftsContext/spacecraftBus", _to: "pcs/physicalComponent", "@context": "pcs" },
    //{ "@type": "spacecraftBus", _id: "spacecraftsContext/spacecraftBus123", "@context": "spacecraftsContext" },
    "pcs/skbSubclassOf(x, y) ^ pcs/componenType(y) -> pcs/componenType(x)",
        //result is  { "@type": "skbComponentType",  _id: "spacecraftsContext/spacecraftBus", "@context": "pcs" },

        //{ "@type": "spacecraftBus", _id: "spacecraftsContext/spacecraftBus123", "@context": "spacecraftsContext" },
        // spacecraftsContext/spacecraftBus(spacecraftBus123) = >
        // replace because spacecraftBus is pcs/componentType
        //  spacecraftsContext/pcs/componentType(spacecraftBus)(spacecraftBus123) => pcs/isRealisationOf(spacecraftBus123, spacecraftBus)
        "x(y) ^ pcs/componentType(x) -> pcs/component(y) ^ pcs/isRealisationOf(y, x)"
    //result is  { "@type": "component",  _id: "spacecraftsContext/spacecraftBus123", "@context": "pcs" },
    //result is  { "@type": "isRealisationOf",  _from: "spacecraftsContext/spacecraftBus123" _to: "spacecraftsContext/spacecraftBus" , "@context": "pcs" },
}

// async function downloadData(dataContexts = []) {
//     var res = [];
//     for (var d in dataContexts) {
//         if (d.type == "arangoDb") {
//             var col = await arangoService.collection("baseData");
//             const cursor = await col.lookupByKeys([d.arangoDbName]);
//             if (cursor[0]) {
//                 res.push(cursor[0]);
//             }
//         }
//     }

//     return res;
// }

// async function getSchemas(dataContexts = []) {

//     var datas = downloadData(dataContexts);

//     //find all where type "@type": "skbComponents/schema"
//     datas.filter(e => e["@type"] == "skbComponents/schema")

// }

function getdata1() {
    var data1 = [
        { "@type": "ComponentType", _id: "pcs/rootComponent", "@context": "pcs" },
        { "@type": "ComponentType", _id: "pcs/physicalComponent", "@context": "pcs" },
        //Example that physicalComponent has default context pcs, if not mentionaed because pcs is context of this line
        { "@type": "SubclassOf", _from: "physicalComponent", _to: "pcs/rootComponent", "@context": "pcs" }
        // { "@type": "Consistsof", _from: "skbComponentTypes/bolt", _to: "skbComponentTypes/boltHead", "@context": "pcs" }
    ];

    var data2SpacecraftComponents = [
        { "@type": "ComponentType", _id: "spacecraftsContext/spacecraft", shortLabel: "Spacecraft", "@context": "pcs" },
        { "@type": "ComponentType", _id: "spacecraftsContext/spacecraftBus", shortLabel: "Spacecraft Bus", "@context": "pcs" },
        { "@type": "ComponentType", _id: "spacecraftsContext/payload", shortLabel: "Payload", "@context": "pcs" },
        { "@type": "ComponentType", _id: "spacecraftsContext/cableHarness", shortLabel: "Harness", "@context": "pcs" },
        { "@type": "ComponentType", _id: "spacecraftsContext/ADCS", shortLabel: "ADCS", "@context": "pcs" },
        { "@type": "SubclassOf", _from: "spacecraftsContext/spacecraft", _to: "pcs/physicalComponent", "@context": "pcs" },
        { "@type": "SubclassOf", _from: "spacecraftsContext/spacecraftBus", _to: "pcs/physicalComponent", "@context": "pcs" },
        { "@type": "SubclassOf", _from: "spacecraftsContext/payload", _to: "pcs/physicalComponent", "@context": "pcs" },
        { "@type": "SubclassOf", _from: "spacecraftsContext/cableHarness", _to: "pcs/physicalComponent", "@context": "pcs" },
        { "@type": "SubclassOf", _from: "spacecraftsContext/ADCS", _to: "pcs/physicalComponent", "@context": "pcs" },

        { "@type": "spacecraftBus", _id: "spacecraftsContext/spacecraftBus123", "@context": "spacecraftsContext" },
    ]

    var data3ComponentSchemas = [
        {
            _id: "testCubesatSchema", shortLabel: "Cubesat schema",
            "@context": "baseLinkedData", //"componentsTopStructureApp",
            "@type": "schema",
            "@entityList": [
                { "@type": "Consistsof", "@context": "pcs", _from: "spacecraftsContext/spacecraft", _to: "spacecraftsContext/payload" },
                { "@type": "Consistsof", "@context": "pcs", _from: "spacecraftsContext/spacecraft", _to: "spacecraftsContext/cableHarness" },
                { "@type": "Consistsof", "@context": "pcs", _from: "spacecraftsContext/spacecraft", _to: "spacecraftsContext/ADCS" },
            ],
            "createdBy": "asd",
            "createdAt": {
                "@context": "baseEntityTrackingContext",
                "@type": "createdAtTime",
                value: "13:45"
            }
        },
        {
            _id: "test22CubesatSchema", shortLabel: "Cubesat22 schema",
            "@context": "baseLinkedData", //"componentsTopStructureApp",
            "@type": "schema",
            "@entityList": [
                { "@type": "Consistsof", "@context": "pcs", _from: "spacecraftsContext/spacecraft", _to: "spacecraftsContext/spacecraftBus123" },
                { "@type": "Consistsof", "@context": "pcs", _from: "spacecraftsContext/spacecraft", _to: "spacecraftsContext/payload" },
                { "@type": "Consistsof", "@context": "pcs", _from: "spacecraftsContext/spacecraft", _to: "spacecraftsContext/cableHarness" },
                { "@type": "Consistsof", "@context": "pcs", _from: "spacecraftsContext/spacecraftBus", _to: "spacecraftsContext/ADCS" },
            ],
            "createdBy": "asd",
            "createdAt": {
                "@context": "baseEntityTrackingContext",
                "@type": "createdAtTime",
                value: "23:45"
            }
        }
    ];

    var data4research = [
        {
            _id: "nik1researchActivitySchema", shortLabel: "nik1 research actvity schema",
            "@context": "baseLinkedData", //"componentsTopStructureApp",
            "@type": "schema",
            "@entityList": [
                {
                    shortLabel: "Activity to define problem for Thesis", "@type": "skActivity", "@context": "skSocial",
                    "@entityList": [
                        {
                            "@type": "skWebActvity", "@context": "skSocial",
                            "_action": [
                                {
                                    "@type": "skTextNotice",
                                    text: {
                                        "textref": "To accomplish the objective of creating smart factories, three dimensions of data integration must be ensured...",
                                    },
                                    //pseudo semantic structure
                                    "@entityList": [
                                        { "@type": "objective", "@context": null, _id: "a1", shortlabel: "creating smart factories" },
                                        { "@type": null, "@context": null, _id: "a2", shortlabel: "data integration" },
                                        { _from: "a1", to: "a2" },
                                        { _id: "b1", shortLabel: "vertical integration" },
                                        { "@type": "dimensionOf", "@context": null, _id: "b1-1",  _from: "b1", _to: "a2" },
                                        { "@type": "conditionFor", "@context": null, _id: "c1-1",  _from: "b1-1", _to: "a1" },
                                        { _id: "b2", shortLabel: "horizontal integration" },
                                        { "@type": "dimensionOf", "@context": null, _id: "b2-1", _from: "b2", _to: "a2" },
                                        { "@type": "conditionFor", "@context": null, _id: "c2-1",  _from: "b2-1", _to: "a1" },
                                        { _id: "b3", shortLabel: "end-to-end integration" },
                                        { "@type": "dimensionOf", "@context": null, _id: "b3-1", _from: "b3", _to: "a2" },
                                        { "@type": "conditionFor", "@context": null, _id: "c3-1",  _from: "b3-1", _to: "a1" },
                                        
                                    ],

                                }
                            ],
                            "_subject": ["nikita1"],
                            "_object": [
                                // { _id: "someUrllllll" }
                                {
                                    _id: {
                                        "@type": "complexIdByFieldValues",
                                        "@context": "base",
                                        //"pageUrl": "urn:x-pdf:4b9ac5bda167f9d67afd83f8bcbfb28b",
                                        "pageName": "A Knowledge Graph Based Integration Approach for Industry 4.0"
                                        // "version: 
                                    }
                                }
                            ]
                        }
                    ],
                    "_subject": ["nikita1"],
                    "_object": ["Thesis1Id"],
                },
                {
                    
                }
            ],
            "createdBy": "asd",
            "createdAt": {
                "@context": "baseEntityTrackingContext",
                "@type": "createdAtTime",
                value: "13:45"
            }
        }]

    var data = data1.concat(data2SpacecraftComponents, data3ComponentSchemas, data4research);
    return data;
}

function getWMcollectionName(entityType, lineContext) {
    return `${appWMid}${entityType}-${lineContext}`;
}

// function getWMidToCollection() {
//     var colname = `${appWMid}idToCol`;
//     return arangoService.collection(colname);
// }

async function AddKnownContextLine(v) {
    var type = v["@type"];
    var lineContext = v["@context"];
    var WMcollectionName = getWMcollectionName(type, lineContext);
    var collection = (v._from || v._to) ?
        arangoService.edgeCollection(WMcollectionName) :
        arangoService.collection(WMcollectionName);
    await arangoService.createCollection(collection);

    //prepare _id _from _to fields
    var fieldsToEdit = ["_id", "_from", "_to"];
    for (var key of fieldsToEdit) {
        if (v[key]) {
            if (v[key].split('/').length > 1) { //if context already in id
                v[key] = v[key].replace("/", "-");
            }
            else {
                v[key] = lineContext + "-" + v[key];
            }
        }
    }
    //find in whast collections we store _from _to entities to make valid links in arangoDB
    //for this each WM store key-value _idOriginal->AM-arango-collection

    //save 
    var cIndex = arangoService.getWMidToCollection();
    if (v["_id"]) {
        v._key = v["_id"];
        delete v["_id"];
        var res = await cIndex.save({ _key: v._key, value: WMcollectionName }, { overwrite: true });
    }
    try {
        if (v["_from"]) {
            var cindexFrom = await cIndex.document(v["_from"]);
            v["_from"] = `${cindexFrom.value}/${v["_from"]}`
            cindexFrom.isFromFor = cindexFrom.isFromFor || [];
            cindexFrom.isFromFor.push(v._key);
            cIndex.save(cindexFrom, { overwrite: true });
        }
        if (v["_to"]) {
            var cindexTo = await cIndex.document(v["_to"]);
            v["_to"] = `${cindexTo.value}/${v["_to"]}`
            cindexTo.isToFor = cindexFrom.isToFor || [];
            cindexTo.isToFor.push(v._key);
            cIndex.save(cindexFrom, { overwrite: true });
        }
    }
    catch (ex) {
        console.error("cannot find collection for edge verticies", v)
        console.error(ex)

    }
    try {
        console.log("save", v)
        // var res2 = await collection.save(v);//, { waitForSync: true, overwrite: true });
        var res2;
        if (v["_from"]) {
            res2 = await collection.save(v);
        }
        else {
            res2 = await collection.save(v, { waitForSync: true, overwrite: true, graceful: true });
        }
        return res2;
    }
    catch (ex) {
        console.error(ex);
    }
}

async function getAvailableDataChunks() {
    var extsC = await arangoService.dbQuery(aql`FOR doc IN ${AT.collections.baseData}
    RETURN DISTINCT { _id: doc._id }`)
    var res = await extsC.all();

    return res;
}

async function uploadDataToWM(dataId = null) {
    //preparation
    var cIndex = arangoService.getWMidToCollection();


    var componentsData1 = null;
    if (!dataId || dataId == "default") {
        await arangoService.createCollection(cIndex);
        await cIndex.truncate();
        //get fake data
        componentsData1 = getdata1();
        // var componentsData2 = getdata2();
    }
    else {
        var col = await arangoService.collection("baseData");
        const cursor = await col.lookupByKeys([dataId]);
        // var data = await cursor.all();
        if (cursor[0]) {
            if (cursor[0]["@type"] == "schema") {
                componentsData1 = cursor[0]["@entityList"]
            }
        }
    }

    //create WM collections by this data.

    for (var v of componentsData1) {
        if (v["@context"] == "pcs" ||
            v["@context"] == "baseLinkedData") {
            await AddKnownContextLine(v);
        }
        else {
            //if our app dont know context of this line 
            //we can try to infer this line to our context
            //eg { "@type": "spacecraftBus", _id: "spacecraftsContext/spacecraftBus123", "@context": "spacecraftsContext" },
            var typeElementKey = v["@context"] + "-" + v["@type"];
            // var typeExistsAsElement = await cIndex.documentExists(typeElementKey);
            var typeExistsAsElement = false;
            try {
                await cIndex.document(typeElementKey);
                typeExistsAsElement = true;
            }
            catch (ex) {
                console.error(ex);
            }
            if (typeExistsAsElement) {
                var document = await cIndex.document(typeElementKey);
                var typeCollectionName = document.value;
                // var typeCollection = db.collection(typeCollectionName);
                var typeElement = await arangoService.getEntity(`${typeCollectionName}/${typeElementKey}`)
                // v["_to"] = `${collectionName.value}/${v["_to"]}`
                //hmm Where we have spacecraftsContext/spacecraftBus in pcs context already
                //yes we have 2
                //1 { "@type": "ComponentType", _id: "spacecraftsContext/spacecraftBus", shortLabel: "Spacecraft Bus", "@context": "pcs" },
                //2 { "@type": "SubclassOf", _from: "spacecraftsContext/spacecraftBus", _to: "pcs/physicalComponent", "@context": "pcs" },
                //Hm what rules do we have where pcs/ComponentType or pcs/SubclassOf in left side?
                //yes we have "x(y) ^ pcs/ComponentType(x) -> pcs/Component(y) ^ pcs/isRealisationOf(y, x)"
                if (typeElement._key == typeElementKey && typeElement["@context"] == "pcs" && typeElement["@type"] == "ComponentType") {
                    //let check left side
                    //spacecraftBus(spacecraftBus123) ^ pcs/ComponentType(spacecraftBus) ->
                    //true ^ true -> ok
                    //means pcs/Component(spacecraftBus123) ^ pcs/isRealisationOf(spacecraftBus123, spacecraftBus)
                    //result is  { "@type": "component",  _id: "spacecraftsContext/spacecraftBus123", "@context": "pcs", .... },
                    var element = Object.assign({}, v);
                    element["@context"] = "pcs";
                    element["@type"] = "Component";
                    //result is  { "@type": "isRealisationOf",  _from: "spacecraftsContext/spacecraftBus123" _to: "spacecraftsContext/spacecraftBus" , "@context": "pcs" },
                    var link2 = {
                        "@context": "pcs", "@type": "RealisationOf",
                        _from: element._id,
                        _to: `${v["@context"]}/${v["@type"]}`
                    };
                    await AddKnownContextLine(element)
                    await AddKnownContextLine(link2)
                }
            }
        }
    }
}

async function uploadSchemaToWM(shemaId) {

}

async function disclosureSchemaToWM(objectId) {

}

async function getComponentsShema() {

}

async function getWMasGraph() {
    var result = [];
    var cIndex = arangoService.getWMidToCollection();
    var extsC = await arangoService.dbQuery(aql`FOR doc IN ${cIndex}
    RETURN DISTINCT doc.value`)
    var allWMcollections = await extsC.all();
    allWMcollections.push(getWMcollectionName("SubclassOf", "pcs"));
    allWMcollections.push(getWMcollectionName("Consistsof", "pcs"));
    allWMcollections.push(getWMcollectionName("RealisationOf", "pcs"));
    allWMcollections.push(getWMcollectionName("HasParam", "pcs"));
    allWMcollections.push(getWMcollectionName("ParamValue", "pcs"));
    allWMcollections.push(`${appWMid}DisclosedFrom`);

    for (var colname of allWMcollections) {
        try {
            var collection = arangoService.collection(colname);
            var dataR = await collection.all({ limit: 1000 });
            var data = await dataR.all();
            result = result.concat(data);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    return result;
}

async function discloseElementToWm(elementId) {
    var entity = await arangoService.getEntity(elementId);
    var entityList = entity["@entityList"];

    // entityList.sort((a, b) => (a._id && b._id) ? 0 : a._id ? -1 : 1)

    for (var v of entityList) {
        try {
            var res2 = await AddKnownContextLine(v);
            console.log("var res2", res2)
            await AddDisclosedFromLink(res2._id, elementId);
        }
        catch (ex) {
            console.error(v);
            console.error(ex);
        }
    }
}

async function closeElementFromWm(elementId) {
    var disclosedFrom = `${appWMid}DisclosedFrom`;
    var discloseCol = await arangoService.edgeCollection(disclosedFrom);

    var cIndex = arangoService.getWMidToCollection();
    const edges = await discloseCol.inEdges(elementId);
    for (var edge of edges) {
        await discloseCol.remove(edge._id);

        try {
            var col = await arangoService.collection(edge._from.split('/')[0])
            await col.remove(edge._from);
        }
        catch (ex) {
            console.error(edge._from, col)
            console.error(ex)
        }

        try {
            await cIndex.remove({ _key: edge._from.split('/')[1] });
        }
        catch (ex) {
            console.error(edge._from.split('/')[1], edge._from, "cIndex")
            console.error(ex)
        }
    }


    //     var extsC = await arangoService.dbQuery(aql`FOR v,e,path IN 1..1 INBOUND ${elementId}  
    //     ${disclosedFrom}
    //     REMOVE e._from IN PARSE_IDENTIFIER(e._from).collection`)
    //     var res = await extsC.all();
    //     return res;
}

async function AddDisclosedFromLink(fromId, toId) {
    var disclosedFrom = `${appWMid}DisclosedFrom`;
    var col = await arangoService.edgeCollection(disclosedFrom);

    await arangoService.createCollection(col);
    await col.save({ _from: fromId, _to: toId })
}

async function saveComponentsShema() {

}


module.exports.getAvailableDataChunks = getAvailableDataChunks;
module.exports.uploadDataToWM = uploadDataToWM;
module.exports.getComponentsShema = getComponentsShema;
module.exports.saveComponentsShema = saveComponentsShema;
module.exports.getWMasGraph = getWMasGraph;
module.exports.discloseElementToWm = discloseElementToWm;
module.exports.closeElementFromWm = closeElementFromWm;