

var appUsedSchemas = ["skbContexts/base"]

var arangoService = require('../arangoService').service;
var AT = arangoService.AT;
var db = arangoService.db;

const fs = require('fs');
const path = require("path");
const util = require('util');

//https://stackoverflow.com/questions/46867517/how-to-read-file-with-async-await-properly
// Convert fs.readFile into Promise version of same    
const readFile = util.promisify(fs.readFile);


async function parseSpoonComponentTypes() {
    try {
        var res = {};
        res.withoutExt = [];
        var data = JSON.parse(
            await readFile(path.join(path.dirname(''), "./data/spoon/componentTypes.json"))
        );
        //TODO AI from spoon to componenttype-component ontology 
        var res = [];
        // { "@type": "ComponentType", _id: "pcs/physicalComponent", "@context": "pcs" },
        // { "@type": "SubclassOf", _from: "pcs/physicalComponent", _to: "pcs/rootComponent", "@context": "pcs" }

        for (var d of data) {
            var l = { "@type": "ComponentType", _id: `pcs/${d.componentType}`, "@context": "pcs", shortLabel: d.label }
            if (d.parentComponentType) {
                var SubclassOf = { "@type": "SubclassOf", _from: `pcs/${d.componentType}`, _to: `pcs/${d.parentComponentType}`, "@context": "pcs" }
                res.push(SubclassOf);
            }
            else {
                res.push({ "@type": "SubclassOf", _from: `pcs/${d.componentType}`, _to: "pcs/rootComponent", "@context": "pcs" });
            }
            res.push(l);
        }

        var baseDataBlock = {
            _key: "spoonData", shortLabel: "spoon data",
            "@context": "baseLinkedData", //"componentsTopStructureApp",
            "@type": "schema",
            "@entityList": [
                {
                    _id: "spoonComponentTypes", shortLabel: "spoon component types",
                    "@context": "baseLinkedData", //"componentsTopStructureApp",
                    "@type": "schema",
                    "@entityList": res,
                    "createdBy": "spoonParser"
                }
            ],
            "createdBy": "spoonParser",
            "spoonData": {
                "@context": "spoonData",
                "@type": "componentType",
            }
        };

        var res1 = await AT.collections.baseData.import([baseDataBlock], { onDuplicate: "replace" });

        return res;
    } catch (err) {
        throw err;
    }

}
var exts = ['NOMINAL', 'MIN', 'MAX', 'PEAK', 'AVERAGE'];

async function parseSpoonAttributes() {

    try {
        var res = {
            withoutExt: [],
            withExt: [],
            endsWithUnit: [],
            endsWithoutUnit: [],
            attrs: []
        };
        var data = JSON.parse(
            await readFile(path.join(path.dirname(''), "./data/spoon/attributes.json"))
        );

        for (var line of data) {
            try {
                // var attributes = line.attributes;
                // if (Array.isArray(attributes)) {
                var attributeType = line.attributeType //NOMINALFREQUENCYBAND QUALIFIEDOPERATINGTIMESEC 
                //TRANSMITPOWER-MINW 1PPSINPUTQTY VOLTAGEINPUTMAXV VOLUMELITERS VOLUMEKG VOLUMEM3
                //POWERINPUTAVERAGEW-HR POWERINPUTAVERAGEW
                var defaultAattributeUnit = line.attributeUnit; //QTY
                var attributeValueType = line.attributeValueType; //NUMBER TEXT QTY....
                if (!defaultAattributeUnit) {
                    //mb it's a TEXT
                    if (attributeValueType == "TEXT") {

                    }
                    else {
                        console.error(line);
                        // throw (Error(line));
                    }
                }
                else {
                    // console.error(line, "2");
                }

                // const checker = value =>
                //     !['NOMINAL', '-MIN', '-MAX'].some(element => value.includes(element));
                //SPOON2RUSTIK
                var hasExtInUnit = exts.some(element => attributeType.includes(element));;
                if (!hasExtInUnit) {
                    res.withoutExt.push(line);

                    var attrname = null;
                    var attrUnit = null;
                    if (defaultAattributeUnit &&
                        line.description && attributeType) {
                        // attributeType.toLowerCase().split(defaultAattributeUnit.toLowerCase())[0] == line.description.toLowerCase()) {

                        var reg1 = new RegExp(defaultAattributeUnit.replace(/\-|\*|\^|\//g, "") + "$", "gi");
                        var realAttributeType = attributeType.replace(/\-|\*|\^|\//g, "").split(reg1)[0];
                        if (realAttributeType.toLowerCase() == line.description
                        .replace(/ /g, "") //"Watt Density" for "WATTDENSITYWCM^2"
                        .toLowerCase()) {
                            attrname = realAttributeType;
                            attrUnit = defaultAattributeUnit
                        }
                        else {
                            var endsToUnit = { "LITERS": "L" };
                            for (var end in endsToUnit) {
                                if (attributeType.endsWith(end) && defaultAattributeUnit == endsToUnit[end]) {
                                    attrname = attributeType.split(end)[0];
                                    attrUnit = endsToUnit[end];
                                    break;
                                }
                            }
                        }
                    }

                    if (!attrname) {
                        attrname = attributeType;
                        attrUnit = defaultAattributeUnit
                    }

                    var attr = {
                        attrname: attrname,
                        ext: null,
                        unit: defaultAattributeUnit,
                        spoon: line
                    }

                    res.attrs.push(attr);
                }
                else {
                    res.withExt.push(line);

                    var currentExt = null;
                    for (var ext of exts) {
                        if (attributeType.includes(ext)) {
                            if (currentExt) {
                                console.log("eroor two exts ", attributeType, currentExt, ext)
                            }
                            currentExt = ext;
                            break;
                        }
                    }

                    if (currentExt) {
                        var attrname = attributeType.split(currentExt)[0].replace(/^\-+|\-+$/g, '');
                        var attrUnit = attributeType.split(currentExt)[1].replace(/^\-+|\-+$/g, '');
                    }
                    else {
                        var attrname = attributeType;
                        var attrUnit = defaultAattributeUnit
                    }
                    // if (attributeType.endsWith(defaultAattributeUnit)) { // MIN And N is wrong because ...-MIN ends with N
                    if (attrUnit && defaultAattributeUnit && attrUnit.toLowerCase() == defaultAattributeUnit.toLowerCase()) {
                        res.endsWithUnit.push(attr);
                    }
                    else {
                        res.endsWithoutUnit.push(line);
                    }

                    var attr = {
                        attrname: attrname,
                        ext: currentExt,
                        unit: defaultAattributeUnit,
                        spoon: line
                    }

                    res.attrs.push(attr);
                }

            } catch (err) {
                throw err;
            }
        }
        // }
        var t = [];
        var grouped = res.withoutExt.reduce((acc, curr) => {
            if (!acc[curr.description]) acc[curr.description] = []; //If this type wasn't previously stored
            acc[curr.description].push(curr);
            return acc;
        }, {});
        var multi = {};
        for (var v in grouped) {
            if (grouped[v].length > 1) {
                multi[v] = grouped[v]
            }
        }
        res.grouped = grouped;
        res.multi = multi;

        var groupedAttrs = res.attrs.reduce((acc, curr) => {
            if (!acc[curr.attrname]) acc[curr.attrname] = []; //If this type wasn't previously stored
            acc[curr.attrname].push(curr);
            return acc;
        }, {});

        var attrsData = [];
        for (var dname in groupedAttrs) {
            var l = { "@type": "GeneralParams", _id: `pcs/attr${dname}`, "@context": "pcs", shortLabel: dname }
            attrsData.push(l);
        }

        var baseDataBlock = {
            _key: "spoonDataAttrs", shortLabel: "spoon data attrs",
            "@context": "baseLinkedData", //"componentsTopStructureApp",
            "@type": "schema",
            "@entityList": [
                {
                    _id: "spoonComponentAttributes", shortLabel: "spoon component attributes",
                    "@context": "baseLinkedData", //"componentsTopStructureApp",
                    "@type": "schema",
                    "@entityList": attrsData,
                    "createdBy": "spoonParser"
                }
            ],
            "createdBy": "spoonParser",
        };

        var res1 = await AT.collections.baseData.import([baseDataBlock], { onDuplicate: "replace" });

        res.attrsData = attrsData;

        return res;
    } catch (err) {
        console.error(line);
        throw err;
    }

}


async function parseSpoonComponents() {

    try {
        var data = JSON.parse(
            await readFile(path.join(path.dirname(''), "./data/spoon/components.json"))
        );
        var data = data.slice(0, 30);
        var res = []
        for (var line of data) {
            var attributes = line.attributes; //{"code":"RECT","label":"Rectangular","type":"GEOMETRY","typeLabel":"Geometry"}
            var componentId = line.componentId;
            var componentType = line.componentType;
            var name = line.name;
            //var type = //NOMINALFREQUENCYBAND //QUALIFIEDOPERATINGTIMESEC

            //create component (or componentType)
            var elId = `pcs/${componentId}`;
            var c = { "@type": "Component", _id: elId, "@context": "pcs", shortLabel: name, spoonData: line }
            res.push(c);
            //create realisationOf (or subclassOf)
            var sc = { "@type": "RealisationOf", _from: elId, _to: `pcs/${componentType}`, "@context": "pcs" }
            res.push(sc);
            //create links to params
            if (Array.isArray(attributes)) {
                for (var attr of attributes) {
                    var currentExt = null;
                    var attributeType = attr.type
                    for (var ext of exts) {
                        if (attributeType.includes(ext)) {
                            if (currentExt) {
                                console.log("eroor two exts ", attributeType, currentExt, ext)
                            }
                            currentExt = ext;
                            break;
                        }
                    }

                    var hasParamId = "randomRef" + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                    var paramName = currentExt ? attributeType.split(currentExt)[0].replace(/^\-+|\-+$/g, '') : attributeType;
                    var hp = { "@type": "HasParam", _id: hasParamId, _from: elId, _to: `pcs/attr${paramName}`, "@context": "pcs" }
                    res.push(hp);

                    var extUnitId = null;
                    if (currentExt) {
                        extUnitId = currentExt + attributeType.split(currentExt)[1];
                    }
                    else {
                        extUnitId = `pcs/extUnitAny`
                    }

                    var pm = { "@type": "ParamValues", _from: hasParamId, _to: extUnitId, "@context": "pcs", value: attr.code }
                    res.push(pm);
                }
            }
        }

        var baseDataBlock = {
            _key: "spoonData222comps", shortLabel: "spoon data22 comps",
            "@context": "baseLinkedData", //"componentsTopStructureApp",
            "@type": "schema",
            "@entityList": [
                {
                    _id: "spoonComponentComponents", shortLabel: "spoon components",
                    "@context": "baseLinkedData", //"componentsTopStructureApp",
                    "@type": "schema",
                    "@entityList": res,
                    "createdBy": "spoonParser"
                }
            ],
            "createdBy": "spoonParser",
        };

        var res1 = await AT.collections.baseData.import([baseDataBlock], { onDuplicate: "replace" });

        return res;
    } catch (err) {
        throw err;
    }

    // var name = path.join(path.dirname(''), "./data/messages.json");
    // fs.writeFile(name, [{a: 3}], (err) => {
    //     if (err) throw err;
    //     console.log('Data written to file');
    // });
}

// ["milliAmperHour", "milli", "amperHour", { shortLabel: "mAh" }],
// ["kWattHour", "kilo", "wattHour", { shortLabel: "kWh", desc: { ref: "https://en.wikipedia.org/wiki/Kilowatt_hour" } }],
// ["nominalWattHourPerLiter", "nominal", "wattHourPerLiter"],
// ["nominalLiter", "nominal", "liter", { shortLabel: "L" }],
function spoonUnitToPcsUnit(unit) {
    var data = {
        "V": "volt",
        "w": "watt",
        "A": "amper",
        "W*hr": "wattHour",
        "Hz": "hertz",
        "dBm": "decibel-milliwatt",
        "dBW": "decibel-watt",
        "°C": "celsius-degree",
        "deg": "angle-degree",
        "deg/s": "angle-degree-sec",
        "km": "kilometre",
        "ft": "foot",
        "GHz": "gigaHertz",
        // "TEXT": "stringAny",
        null: "stringAny",
        "cm^3": "centimetre-cubic"
    }

}

module.exports.parseSpoonAttributes = parseSpoonAttributes;
module.exports.parseSpoonComponentTypes = parseSpoonComponentTypes;
module.exports.parseSpoonComponents = parseSpoonComponents;