
var arangoService = require('../arangoService').service;
var AT = arangoService.AT;
var db = arangoService.db;
var request = require("request")


async function importHypotesisData() {
    var jsondata = await getHypotesisData();
    var jsondata = JSON.parse(jsondata);
    console.log("jsondata", jsondata)
    await importDataToDb(jsondata);
}

async function getHypotesisData() {
    var token = global.gConfig.hypotesis.token;
    var authVal = `Bearer ${token}`
    var url = "https://api.hypothes.is/api/search?user=acct:skolspace@hypothes.is";
    const options = {
        url: url,
        method: 'GET',
        headers: {
            "Authorization": authVal
            // 'Content-Type': 'application/json',
            //'Content-Length': Buffer.byteLength(data)
        }
    };

    // Return new promise
    return new Promise(function (resolve, reject) {
        // Do async job
        request.get(options, function (err, resp, body) {
            if (err) {
                reject(err);
            } else {
                resolve(body);
            }
        })
    })
}

async function importDataToDb(data) {
    var res = [];
    for (var row of data.rows) {
        try {
            var created = row.created;
            var text = row.text;
            var targetUrl = row.target[0].source;
            var title = row.document.title[0];
            var hypotesisLink = row.links.html;
            var _fromId = "userLink"; //TODO user link
            var _toId = await getPageIdByUrl(targetUrl);// "AAA" //TODO pageLink
            res.push({
                // _id   { "@type": "subclassOf", _from: "skSocial/skWebActivity", _to: "pcs/event", "@context": "pcs" },
                "@type": "skWebActivity",
                "@context": "skSocial",
                "_subject": [_fromId],
                "_object": [_toId],
            })
        }
        catch (ex) {
            console.error("hypotesis importDataToDb", row, ex)
        }
    }
}

async function getPageIdByUrl(url) {
    var cursor = await arangoService.dbQuery(aql`
    FOR v,e,path IN skbPages
    Filter v.url == ${url}
           
      RETURN v`);
    var data2 = await cursor.all();
    if (data2[0]){
        return data2[0]._id;
    }
    else{
        //create new page by url
    }
}

module.exports.importHypotesisData = importHypotesisData;