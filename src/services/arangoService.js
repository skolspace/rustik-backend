
var ArangoDatabase = require('arangojs').Database;
var db;// = new ArangoDatabase(global.gConfig.arango.connectionString);
var aql = require('arangojs').aqlQuery;

var cPrefix = "skb"

var appWMid = "WM333"

var AT = {
  collections: {
    baseData: null,

    ComponentTypes: null,
    Components: null,
    Res: null,
    Provide: null,
    Consume: null,
    InsteadOf: null,
    ConnectedTo: null,

    Conditions: null,
    Through: null
  },

};//

global.AT = AT;
global.aql = aql;

async function init() {
  db = new ArangoDatabase(global.gConfig.arango.connectionString);
  db.useDatabase(global.gConfig.arango.dbName);

  AT.collections = {
    baseData: db.collection(`baseData`),
    ComponentTypes: db.collection(`${cPrefix}ComponentTypes`),
    SubclassOf: db.edgeCollection(`${cPrefix}SubclassOf`),
    Components: db.collection(`${cPrefix}Components`),
    RealisationOf: db.edgeCollection(`${cPrefix}RealisationOf`),
    //
    Models: db.collection(`${cPrefix}Models`),
    ConsistsOf: db.edgeCollection(`${cPrefix}ConsistsOf`),
    //
    Units: db.collection(`${cPrefix}Units`), //Amper, Watt
    UnitsExt: db.collection(`${cPrefix}UnitExts`), //micro Max Min Avg nano
    ExtUnits: db.edgeCollection(`${cPrefix}ExtUnits`), //extended units microAmpers, MaxAmpers, etc      
    //
    // ComponentTypes : db.collection(`${cPrefix}ComponentTypes`), 
    GeneralParams: db.collection(`${cPrefix}GeneralParams`),
    HasParam: db.edgeCollection(`${cPrefix}HasParam`),
    MeasuredIn: db.edgeCollection(`${cPrefix}MeasuredIn`),
    //   
    Res: db.collection(`${cPrefix}Resources`),
    Provide: db.edgeCollection(`${cPrefix}Provide`),
    Consume: db.edgeCollection(`${cPrefix}Consume`),
    InsteadOf: db.edgeCollection(`${cPrefix}InsteadOf`),
    ConnectedTo: db.edgeCollection(`${cPrefix}ConnectedTo`),
    //To define object X is subclass of which {consume-tata}, {provide-ta} and {hasProp-asda}
    DefinedBy: db.edgeCollection(`${cPrefix}DefinedBy`),
    //
    ParamValues: db.edgeCollection(`${cPrefix}ParamValues`),
    Inputs: db.edgeCollection(`${cPrefix}Inputs`),
    //TODO mb create DependsOn with field dependType: "condition"
    Conditions: db.edgeCollection(`${cPrefix}Conditions`),
    Through: db.edgeCollection(`${cPrefix}Through`),
    //Knowledges
    Knowledges: db.collection(`${cPrefix}Knowledges`),
    //ontologies
    Ontologies: db.collection(`${cPrefix}Ontologies`),
    Includes: db.edgeCollection(`${cPrefix}Includes`) // can includes any child ontologies or any components or any another refs
  };
  AT.componentTypes = {
    solarPanel: "solarPanel",
    rechargeableBattery: "rechargeableBattery"
  }

  await createAllCollections();
}

async function createCollection(coll) {
  try {
    // if (typeof coll === "string")
    //   coll = collection(coll);
    const res1 = await coll.create();
    console.log(res1);
  } catch (err) {
    console.error(err);
  }
}

async function getEntity(id) {
  try {
    var col = await collection(id.split('/')[0]);
    const cursor = await col.lookupByKeys([id]);
    if (cursor[0]) {
      return cursor[0];
    }
    else {
      console.error(`${key} not found`);
      return null;
    }
  }
  catch (e) {
    console.error(e);
    return null;
  }
}

async function removeEntity(id) {
  try {
    var col = await collection(id.split('/')[0]);
    const cursor = await col.remove(id);
  }
  catch (e) {
    console.error(e);
    return null;
  }
}

async function modifyValue(paramValId, props, addHistory = false) {
  try {
    var collection = await db.collection(paramValId.split('/')[0]);
    var ent = await getEntity(paramValId);
    // const cursor = await collection2.lookupByKeys([paramValId]);
    // var ent = cursor[0]
    ent._modifyHistory = ent._modifyHistory || [];
    var modifications = [];
    for (var p in props) {
      modifications.push({ prevVal: ent[p], newVal: props[p] });
      ent[p] = props[p];
    }
    if (addHistory) {
      ent._modifyHistory.push({
        modifications: modifications,
        modifiedOn: new Date().toISOString()
      })
    }

    ent._id = paramValId;
    ent.viewValue = null;
    var res = collection.save(ent, { overwrite: true });
    return res;
  }
  catch (e) {
    console.log(e);
    return null;
  }
}

async function modifyValueToUnit(paramValId, newUnitId, addHistory = false) {
  try {
    var collection = await db.collection(paramValId.split('/')[0]);
    var ent = await getEntity(paramValId);
    // const cursor = await collection2.lookupByKeys([paramValId]);
    // var ent = cursor[0]
    var newExtUnit = await getEntity(newUnitId);
    ent._to = newExtUnit._id;
    var res = collection.save(ent, { overwrite: true });
    return res;
  }
  catch (e) {
    console.log(e);
    return null;
  }
}

/**
 * import edgesss
 * @param {*} targetCollection 
 * @param {*} data 
 * @param {*} collectionFrom 
 * @param {*} collectionTo 
 * @param {*} keyPrefix 
 * @param {*} firstItemIsKey 
 */
async function importEdgesOfTwoCollections(targetCollection, data, collectionFrom, collectionTo,
  keyPrefix = null, firstItemIsKey = false) {
  var dataToPut = [
    ((keyPrefix || firstItemIsKey) ? ["_key", "_from", "_to"] : ["_from", "_to"])
  ]
  var tt = _.map(data, (d, i) => {
    return keyPrefix ? [`${keyPrefix}${i}`, `${collectionFrom.name}/${d[0]}`, `${collectionTo.name}/${d[1]}`] :
      firstItemIsKey ? [`${d[0]}`, `${collectionFrom.name}/${d[1]}`, `${collectionTo.name}/${d[2]}`]
        : [`${collectionFrom.name}/${d[0]}`, `${collectionTo.name}/${d[1]}`]
  });

  dataToPut.push.apply(dataToPut, tt)

  return await targetCollection.import(dataToPut, { "type": null, onDuplicate: "replace" });
}

/**
 * Save edges of two collctions
 * @param {*} targetCollection 
 * @param {*} data 
 * @param {*} collectionFrom 
 * @param {*} collectionTo 
 * @param {*} keyPrefix 
 * @param {*} firstItemIsKey 
 * @param {as tata} hasAdditionalValues 
 */
async function saveEdgesOfTwoCollections(targetCollection, data, collectionFrom, collectionTo,
  keyPrefix = null, firstItemIsKey = false, hasAdditionalValues = false) {

  var dataToPut = _.map(data, (d, i) => {
    var obj = {};
    if (keyPrefix || firstItemIsKey) {
      obj._key = keyPrefix ? `${keyPrefix}${i}` : `${d[0]}`;
    }
    obj._from = `${collectionFrom.name}/${firstItemIsKey ? d[1] : d[0]}`;
    obj._to = `${collectionTo.name}/${firstItemIsKey ? d[2] : d[1]}`;
    if (hasAdditionalValues) {
      var additionalProps = firstItemIsKey ? d[3] : d[2];
      if (typeof additionalProps === 'object') {
        for (var pKey in additionalProps) {
          obj[pKey] = additionalProps[pKey];
        }
      }
    }
    return obj;
  });

  var res = [];
  // for (var d of dataToPut) {
  // res.push(await targetCollection.save(dataToPut, { overwrite: true }));
  await targetCollection.import(dataToPut, { onDuplicate: "replace" })
  //}
  return res;
}

async function truncateAll() {
  for (var key in AT.collections) {
    AT.collections[key].truncate()
  };
}

async function createAllCollections() {
  for (var key in AT.collections) {
    await createCollection(AT.collections[key])
  };
}

async function dbQuery(query) {
  return await db.query(query);
}

function collection(name) {
  // for (var key in AT.collections){
  //   if (AT.collections[key].name == name){
  //     return AT.collections[key];
  //   }
  // }
  // throw new Error(`Collection ${name} not found`)
  return db.collection(name);
}
function edgeCollection(name) {
  return db.edgeCollection(name);
}


function getWMidToCollection() {
  var colname = `${appWMid}idToCol`;
  return collection(colname);
}


// async function dbQueryAQL(string) {
//   // var req = aql(string);
//   // return await db.query(req);
// }

// module.exports.db = db;
// module.exports.AT = AT;
// module.exports.init = init;
// module.exports.createCollection = createCollection;
// module.exports.createAllCollections = createAllCollections;
// module.exports.truncateAll = truncateAll;
// module.exports.importEdgesOfTwoCollections = importEdgesOfTwoCollections;


/**
 * Main service
 */
var ArangoService = {
  //this.student = student;
}


ArangoService.db = db;
ArangoService.AT = AT;
ArangoService.init = init;
ArangoService.collection = collection;
ArangoService.edgeCollection = edgeCollection;
ArangoService.getEntity = getEntity;
ArangoService.removeEntity = removeEntity;
ArangoService.modifyValue = modifyValue;
ArangoService.modifyValueToUnit = modifyValueToUnit;
ArangoService.createCollection = createCollection;
ArangoService.createAllCollections = createAllCollections;
ArangoService.truncateAll = truncateAll;
ArangoService.importEdgesOfTwoCollections = importEdgesOfTwoCollections;
ArangoService.saveEdgesOfTwoCollections = saveEdgesOfTwoCollections;
ArangoService.dbQuery = dbQuery;
ArangoService.appWMid =  appWMid;
ArangoService.getWMidToCollection = getWMidToCollection;
// ArangoService.dbQueryAQL = dbQueryAQL;

// module.exports = ArangoService;

module.exports.aql = aql;
module.exports.service = ArangoService;
// module.exports =  {
//   aql: aql,
//   service: ArangoService
// }

// module.exports = (dbConnection = null, otherStatefulDeps = null) => {
//   const self = {
//     AT: {},
//     db: null,
//     // find() {
//     //   return dbConnection.table('cupcakes').find();
//     // }
//     async init() {
//       db = new ArangoDatabase(global.gConfig.arango.connectionString);
//       db.useDatabase(global.gConfig.arango.dbName);

//       AT.collections = {
//           ComponentTypes: db.collection(`${cPrefix}ComponentTypes`),
//           Components: db.collection(`${cPrefix}Components`),
//           ConsistsOf: db.edgeCollection(`${cPrefix}ConsistsOf`),
//           Models: db.collection(`${cPrefix}Models`),
//           Units : db.collection(`${cPrefix}Units`), //Amper, Watt
//           UnitsExt : db.collection(`${cPrefix}UnitExts`), //micro Max Min Avg nano
//           ExtUnits : db.edgeCollection(`${cPrefix}ExtUnits`), //extended units microAmpers, MaxAmpers, etc      
//           ComponentTypes : db.collection(`${cPrefix}ComponentTypes`), //Amper, Watt
//           GeneralParams : db.collection(`${cPrefix}GeneralParams`),
//           HasParam : db.edgeCollection(`${cPrefix}HasParam`),
//           MeasuredIn : db.edgeCollection(`${cPrefix}MeasuredIn`),     
//           SubclassOf : db.edgeCollection(`${cPrefix}SubclassOf`),
//           Res : db.collection(`${cPrefix}Resources`),
//           Provide : db.edgeCollection(`${cPrefix}Provide`),
//           Consume : db.edgeCollection(`${cPrefix}Consume`),
//     //To define object X is subclass of which {consume-tata}, {provide-ta} and {hasProp-asda}
//           DefinedBy : db.edgeCollection(`${cPrefix}DefinedBy`),
//     //
//           RealisationOf: db.edgeCollection(`${cPrefix}RealisationOf`),
//           ParamValues: db.edgeCollection(`${cPrefix}ParamValues`),
//           Through: db.edgeCollection(`${cPrefix}Through`)
//       };
//        AT.componentTypes = {
//           solarPanel: "solarPanel",
//           rechargeableBattery: "rechargeableBattery"
//         }

//       await createAllCollections();
//     },


//     async createCollection(collection) {
//       try {
//         const res1 = await collection.create();
//         console.log(res1);
//       } catch (err) {
//         console.error(err);
//       }
//     },

//     async importEdgesOfTwoCollections(targetCollection, data, collectionFrom, collectionTo, keyPrefix = null) {
//       var dataToPut = [
//         (keyPrefix ? ["_key", "_from", "_to"] : ["_from", "_to"])
//       ]
//       var tt = _.map(data, (d, i) => {
//         return keyPrefix ? [`${keyPrefix}${i}`, `${collectionFrom.name}/${d[0]}`, `${collectionTo.name}/${d[1]}`] :
//           [`${collectionFrom.name}/${d[0]}`, `${collectionTo.name}/${d[1]}`]
//       });

//       dataToPut.push.apply(dataToPut, tt)

//       return await targetCollection.import(dataToPut, { "type": null, onDuplicate: "replace" });
//     },

//     async truncateAll() {
//       for (var key in AT.collections) {
//         AT.collections[key].truncate()
//       };
//     },

//     async createAllCollections() {
//       for (var key in AT.collections) {
//         await createCollection(AT.collections[key])
//       };
//     }
//   };
//   return self;
// }
