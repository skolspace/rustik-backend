import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import satInfos from './models//satInfo';
const evaluatex = require("evaluatex");

import configF from '../config/config.js' //https://stackoverflow.com/questions/48593254/execute-a-module-immediately-after-calling-import-in-es6
//const config = configF();//  require('../config/config.js');

// import arangoService from './services/arangoService'
var arangoService = require('./services/arangoService').service;
arangoService.init()

// import satsService from './services/satsService'
var satsService = require('./services/satsService');
import solutionsService from './services/solutionsService'
// import modelsService from './services/modelsService'
var modelsService = require('./services/modelsService');
// import componentsService from './services/componentsService'
var componentsService = require('./services/componentsService');
var ontologiesService = require('./services/ontologiesService');
var shemaEditorAppService = require('./services/schemaEditorApp/shemaEditorAppService');
var spoonParser = require('./services/parsers/spoonParser');
var hypotesisParser = require('./services/parsers/hypotesisParser');
import skgEnvService from './services/test-support/skgEnvService'

const app = express();
const router = express.Router();
app.use(cors());
app.use(bodyParser.json());

mongoose.connect(global.gConfig.mongo.connectionString);
const connection = mongoose.connection;
connection.once('open', () => {
    console.log('MongoDB database connection established successfully!');
});

// var ArangoDatabase = require('arangojs').Database;
// var db = new ArangoDatabase(global.gConfig.arango.connectionString);
// var aql = require('arangojs').aqlQuery;
// // db.createDatabase('mydb').then(
// //   () => console.log('Database created'),
// //   err => console.error('Failed to create database:', err)
// // );
// db.useDatabase(global.gConfig.arango.dbName);



app.use('/', router);
router.route('/satInfos/:pattern*?').get((req, res) => {
    var searchPattern = req.params.pattern;
    if (searchPattern && searchPattern.length > 1) {
        var searchReq = { satName: new RegExp('^' + searchPattern) };
        if (Number(searchPattern)) {
            searchReq = {
                $or: [searchReq,
                    { norad_id: Number(searchPattern) }]
            }
        }
        satInfos.find(searchReq, (err, issues) => {
            // satInfos.find({ satName: new RegExp('^' + searchPattern)}, (err, issues) => {
            if (err)
                console.log(err);
            else
                res.json(issues);
        }).limit(20);
    }
    else {
        satInfos.find((err, issues) => {
            if (err)
                console.log(err);
            else
                res.json(issues);
        }).limit(50);
    }
})

router.route('/solution').post((req, res) => {
    var body = req.body;
    console.log("body", body);

    solutionsService.create(body).then((data) => {
        //TODO move to common handler
        if (data.error) {
            res.status(500).json(data);
        }
        else
            res.status(200).json(data);
    });
});

router.route('/solutions').get(async (req, res) => {
    //var userid
    var data = await solutionsService.getAvailableSolutions();
    res.status(200).json(data);
});

router.route('/solution/:solId/generateSats').post((req, res) => {
    var solId = req.params.solId;
    var body = req.body;
    console.log("body", body);

    // res.status(200).json("COMMENTED");
    // return;

    satsService.generateSats(solId, body).then((data) => {
        res.status(200).json(data);
    });
});

router.route('/solution/:solId/sats').get(async (req, res) => {
    var solId = req.params.solId;
    var data = await satsService.satsBySolId(solId);
    res.status(200).json(data);
});

router.route('/ontologyTree').get(async (req, res) => {
    var data = await ontologiesService.getOntologyTree();
    res.status(200).json(data);
});

//--------------COPONENTS
router.route('/componentTypes').post(async (req, res) => {
    //var userid
    var filterOntologyIds = req.body.ontologyIds;
    var data = await componentsService.getComponentTypes(filterOntologyIds);
    res.status(200).json(data);
});

router.route('/satInfos/add').get((req, res) => {
    let ss = "{uuid:\"TEST678\" }";
    let issue = new satInfos();// (req.body);//(ss);
    issue.save()
        .then(issue => {
            res.status(200).json({ 'issue': 'Added successfully' });
        })
        .catch(err => {
            res.status(400).send('Failed to create new record');
        });
    // res.status(400).send('asd');
});

app.listen(4000, () => console.log(`Express server running on port 4000`));



router.route('/componentTypes/:compTypeId([^/]+/[^/]+)/components').get(async (req, res) => {
    var compTypeId = req.params.compTypeId;
    var data = await componentsService.getComponentsByType(compTypeId);
    res.status(200).json(data);
});

router.route('/componentTypes/:compTypeId([^/]+/[^/]+)/definition').get(async (req, res) => {
    var compTypeId = req.params.compTypeId;
    var data = await componentsService.getComponentTypeDefinition(compTypeId);
    res.status(200).json(data);
});

// router.route('/componentTypes/:compTypeId([^/]+/[^/]+)/components').post(async (req, res) => {
router.route('/components').post(async (req, res) => {
    var compTypeId = req.body.TypeId;
    var baseComponentId = req.body.baseComponentId;
    var data = await componentsService.createComponent(compTypeId, req.body, baseComponentId);
    res.status(200).json(data);
});

// --
// --
// Knowledge base ontologies
// --

router.route('/generateSkg').get(async (req, res) => {
    var data = await skgEnvService.createAll();
    res.status(200).json(data);
});


router.route('/solution/:solId/models').get(async (req, res) => {
    var solId = req.params.solId;
    var data = await modelsService.getShort(solId);
    res.status(200).json(data);
});

router.route('/solution/:solId/models/:modelId').get(async (req, res) => {
    var solId = req.params.solId;
    var modelId = req.params.modelId;
    var data = await modelsService.get(modelId, solId);
    res.status(200).json(data);
});

router.route('/solution/:solId/models/:modelId/info').post(async (req, res) => {
    var solId = req.params.solId;
    var modelId = req.params.modelId;
    var infoReq = req.body;
    var data = await satsService.getInfo(modelId, infoReq);
    // const fn = evaluatex("27^{\\frac{1}{3}}", { latex: true }); //replace sqrt[3]{27} because not working
    // const result = fn({aa: 27}, { latex: true });
    res.status(200).json(data);
});

router.route('/models/:modelId([^/]+/[^/]+)/components').get(async (req, res) => {
    var body = req.body;
    var modelId = req.params.modelId;
    var data = await componentsService.getSubcomponents(modelId);
    res.status(200).json(data);
});

router.route('/models/:modelId/components/:compId([^/]+/[^/]+)').post(async (req, res) => {
    var body = req.body;
    var modelId = req.params.modelId;
    var compId = req.params.compId;
    var data = await componentsService.attachComponentToModel(compId, modelId);
    res.status(200).json(data);
});

router.route('/components/:parentCompId([^/]+/[^/]+)/components/:compId([^/]+/[^/]+)').post(async (req, res) => {
    var body = req.body;
    var parentCompId = req.params.parentCompId;
    var compId = req.params.compId;
    var data = await componentsService.attachComponentToParentComp(compId, parentCompId);
    res.status(200).json(data);
});

router.route('/components/:compId([^/]+/[^/]+)/family').post(async (req, res) => {
    var compId = req.params.compId;
    var request = req.body;
    var data = await componentsService.getCompFamily(compId, request);
    res.status(200).json(data);
});

router.route('/components/:compId([^/]+/[^/]+)/params').post(async (req, res) => {
    var compId = req.params.compId;
    var request = req.body;
    var data = await componentsService.addParam(compId, request);
    res.status(200).json(data);
});

router.route('/components/:compId([^/]+/[^/]+)/functions').post(async (req, res) => {
    var compId = req.params.compId;
    var functionType = req.body.functionType;
    var resourceId = req.body.resourceId;
    var data = await componentsService.addFunction(compId, functionType, resourceId);
    res.status(200).json(data);
});

router.route('/functions/:funcId([^/]+/[^/]+)').delete(async (req, res) => {
    var funcId = req.params.funcId;
    var data = await componentsService.removeFunction(funcId);
    res.status(200).json(data);
});

router.route('/functions/:funcId([^/]+/[^/]+)/throughs').post(async (req, res) => {
    var funcId = req.params.funcId;
    var toId = req.body.attachmentId;
    var data = await componentsService.addThrough(funcId, toId);
    res.status(200).json(data);
});

router.route('/throughs/:throughId([^/]+/[^/]+)').delete(async (req, res) => {
    var throughId = req.params.throughId;
    var data = await componentsService.deleteThrough(throughId);
    res.status(200).json(data);
});

router.route('/params/:hasParamId([^/]+/[^/]+)/value').post(async (req, res) => {
    var hasParamId = req.params.hasParamId;
    var request = req.body;
    var data = await componentsService.addParamValue(hasParamId, request);
    res.status(200).json(data);
});

router.route('/connection').post(async (req, res) => {
    var request = req.body;
    var data = await componentsService.createConnection(request._from, request._to, request);
    res.status(200).json(data);
});

router.route('/entity/:id([^/]+/[^/]+)').get(async (req, res) => {
    var id = req.params.id;
    var data = await arangoService.getEntity(id);
    res.status(200).json(data);
});


router.route('/paramValue/:paramValId([^/]+/[^/]+)').post(async (req, res) => {
    var paramValId = req.params.paramValId;
    var body = req.body;
    var data = await modelsService.modifyValue(paramValId, body);
    res.status(200).json(data);
});

router.route('/calculate/:modelId').post(async (req, res) => {
    var body = req.body;
    var modelId = req.params.modelId;
    var data = await modelsService.calculateParamValues(modelId);
    res.status(200).json(data);
});

router.route('/extUnits').get(async (req, res) => {
    var data = await componentsService.getExtUnits();
    res.status(200).json(data);
});

router.route('/resources').get(async (req, res) => {
    var data = await componentsService.getResources();
    res.status(200).json(data);
});

router.route('/generalParams').get(async (req, res) => {
    var data = await componentsService.getGeneralParams();
    res.status(200).json(data);
});

router.route('/bkData/available').get(async (req, res) => {
    var data = await shemaEditorAppService.getAvailableDataChunks();
    //var data = [ { _id: "bkData/mainChunk"}, { _id: "bkData/secondChunk" }]
    res.status(200).json(data);
});

router.route('/apps/schemaEditor/uploadDataToWM/:dataId([^/]+/?[^/]+)').get(async (req, res) => {
    var dataId = req.params.dataId;
    var data = await shemaEditorAppService.uploadDataToWM(dataId);
    res.status(200).json(data);
});


router.route('/apps/schemaEditor/getCurrentWM/asgraph').post(async (req, res) => {
    // var elementId = req.params.elementId;
    var body = req.body; ///list of all coontexts and collections what i want to get.
    var data = await shemaEditorAppService.getWMasGraph();
    res.status(200).json(data);
});

router.route('/apps/schemaEditor/disclose/:elementId([^/]+/[^/]+)').post(async (req, res) => {
    var elementId = req.params.elementId;
    var data = await shemaEditorAppService.discloseElementToWm(elementId);
    res.status(200).json(data);
});

router.route('/apps/schemaEditor/close/:elementId([^/]+/[^/]+)').delete(async (req, res) => {
    var elementId = req.params.elementId;
    var data = await shemaEditorAppService.closeElementFromWm(elementId);
    res.status(200).json(data);
});

router.route('/parse/spoon/test').get(async (req, res) => {
    //var data = await spoonParser.parseSpoonComponents();
    var data = await spoonParser.parseSpoonAttributes();
    // var data = await spoonParser.parseSpoonComponentTypes();
    res.status(200).json(data);
});

router.route('/parse/hypotesis/import').get(async (req, res) => {
    var data = await hypotesisParser.importHypotesisData();
    res.status(200).json(data);
});

router.route('/solution/:solId/executeRules/models/:modelId/').get(async (req, res) => {
    var modelId = req.params.modelId;
    var rules = ["rule1"];
    var data = await componentsService.checkRules(modelId, rules);
    // var data = await spoonParser.parseSpoonComponentTypes();
    res.status(200).json(data);
});

//returns al lrisks directly related to this entity
router.route('/apps/:elementId([^/]+/[^/]+)/directlyRelatedRisks').delete(async (req, res) => {
    var elementId = req.params.elementId;
    // var data = await shemaEditorAppService.closeElementFromWm(elementId);
    res.status(200).json(data);
});