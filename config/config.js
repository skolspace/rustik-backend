//const _ = require('lodash');
global._ = require('lodash');
// module variables
const defaultConfig = require('./config.json');
const environmentConfig = require(`./config.${process.env.NODE_ENV}.json`);
const finalConfig = _.merge({}, defaultConfig, environmentConfig);
global.gConfig = finalConfig; //https://codeburst.io/node-js-best-practices-smarter-ways-to-manage-config-files-and-variables-893eef56cbef
//